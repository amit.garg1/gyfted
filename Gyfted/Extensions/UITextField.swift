//
//  UITextField.swift
//  Gyfted
//
//  Created by Ben Dodson on 25/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func addDismissToolbar() {
        let toolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(resignFirstResponder))
        toolbar.items = [flexSpace, doneButton]
        inputAccessoryView = toolbar
    }
    
    func addClearButtonToToolbar() {
        guard let toolbar = inputAccessoryView as? UIToolbar else { return }
        let clearButton = UIBarButtonItem(title: NSLocalizedString("Clear", comment: ""), style: .plain, target: self, action: #selector(clearText))
        toolbar.items?.insert(clearButton, at: 0)
        inputAccessoryView = toolbar
    }
    
    @IBAction func clearText() {
        text = nil
    }
}

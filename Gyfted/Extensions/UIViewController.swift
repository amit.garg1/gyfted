//
//  UIViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 03/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

extension UIViewController {
    func add(_ child: UIViewController, to view: UIView) {
        addChild(child)
        child.view.frame = view.bounds
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func remove() {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
    
    @objc func popToRoot() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
}

//
//  UILabel.swift
//  Gyfted
//
//  Created by Ben Dodson on 11/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func kern(_ value: CGFloat) {
        guard let textColor = textColor, let font = font, let text = text else { return }
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = textAlignment
        attributedText = NSAttributedString(string: text, attributes: [.foregroundColor: textColor, .font: font, .paragraphStyle: paragraph, .kern: value])
    }
}

//
//  AppDelegate.swift
//  Gyfted
//
//  Created by Ben Dodson on 02/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import Firebase
import GooglePlaces
import OneSignal
import Branch
import FBSDKCoreKit
import StoreKit
import Mixpanel
import Swifter
import Analytics

enum DeepLinkLocation {
    case notifications
    case wishlist
    case item
    case profile
    case inviteFriends
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deepLinkLocation: DeepLinkLocation?
    var deepLinkValue: Any?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        GMSPlacesClient.provideAPIKey(Config.credentials.googlePlaces)
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        migrateDefaults()
        initiateFirebaseKeychain()
        
        Config.keepingUpAppearances()
        
        checkReviewPrompt()
        
        DataManager.shared.downloadCachedKeywords()
        
        OneSignal.initWithLaunchOptions(launchOptions, appId: "422759b6-2da6-4861-88d3-b611656a888c", handleNotificationAction: { (_) in
            self.performDeepLink(.notifications)
        }, settings: [kOSSettingsKeyAutoPrompt: false])
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        
        Mixpanel.initialize(token: "8a2974ccaa44287b77b7ab8b209912af")
        
        let segmentConfiguration = SEGAnalyticsConfiguration.init(writeKey: "3KPp1Z6B09n2bNpTgP5g5nySyTHFvHte")
        segmentConfiguration.trackApplicationLifecycleEvents = true
        segmentConfiguration.recordScreenViews = true
        segmentConfiguration.trackPushNotifications = true
        segmentConfiguration.trackDeepLinks = true
        SEGAnalytics.setup(with: segmentConfiguration)
        
        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, _ in
            if let params = params {
                if let id = params["wishlist"] as? Int {
                    self.performDeepLink(.wishlist, value: id)
                }
                if let id = params["item"] as? Int {
                    self.performDeepLink(.item, value: id)
                }
                if let id = params["profile"] as? String {
                    self.performDeepLink(.profile, value: id)
                }
            }
        })        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = DataManager.shared.unreadNotificationCount
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = DataManager.shared.unreadNotificationCount
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        checkReviewPrompt()
    }
    
    func checkReviewPrompt() {
        let defaults = UserDefaults.standard
        let opens = defaults.integer(forKey: Config.userDefaults.appOpens) + 1
        defaults.set(opens, forKey: Config.userDefaults.appOpens)
        if opens >= 5 && defaults.bool(forKey: Config.userDefaults.shownReviewPrompt) == false {
            Timer.scheduledTimer(withTimeInterval: 10, repeats: false) { (_) in
                DispatchQueue.main.async {
                    SKStoreReviewController.requestReview()
                    defaults.set(true, forKey: Config.userDefaults.shownReviewPrompt)
                }
            }
        }
    }
    
    func migrateDefaults() {
        if UserDefaults.standard.object(forKey: Config.userDefaults.profile) != nil {
            NSLog("MIGRATING USER DEFAULTS")
            let keys = [Config.userDefaults.profile, Config.userDefaults.wishlists, Config.userDefaults.followedTopics]
            for key in keys {
                Config.sharedDefaults().set(UserDefaults.standard.object(forKey: key), forKey: key)
                UserDefaults.standard.removeObject(forKey: key)
            }
        }
    }
    
    func initiateFirebaseKeychain() {
        if let user = Auth.auth().currentUser {
            var tempUser: User?
            do {
                try tempUser = Auth.auth().getStoredUser(forAccessGroup: Config.credentials.firebaseKeychain)
            } catch let error as NSError {
                print("Error getting stored user: %@", error)
            }
            if tempUser == nil {
                do {
                    try Auth.auth().useUserAccessGroup(Config.credentials.firebaseKeychain)
                    Auth.auth().updateCurrentUser(user) { error in
                        if let error = error {
                            print("ERROR: \(error)")
                        } else {
                            print("USER HAS BEEN MIGRATED")
                        }
                    }
                } catch let error as NSError {
                    print("Error changing user access group: %@", error)
                }
            } else {
                print("USER ALREADY MIGRATED")
            }
        } else {
            do {
                try Auth.auth().useUserAccessGroup(Config.credentials.firebaseKeychain)
            } catch let error as NSError {
                print("Error changing user access group: %@", error)
            }
        }
    }
    
    // MARK: Branch Deep Linking
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let branchHandled = Branch.getInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        if !branchHandled {
            return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        if url.absoluteString.contains("oauth_token"), let callbackUrl = URL(string: "gyfted://") {
            Swifter.handleOpenURL(url, callbackURL: callbackUrl)
            return true
        }
        return ApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        Branch.getInstance().continue(userActivity)
        return true
    }
    
    // MARK: Deep Linking
    
    func performDeepLink(_ location: DeepLinkLocation, value: Any? = nil) {
        self.deepLinkLocation = location
        self.deepLinkValue = value
        
        if let controller = UIApplication.shared.keyWindow?.rootViewController as? MainViewController {
            DispatchQueue.main.async {
                if let presenting = controller.presentedViewController {
                    presenting.dismiss(animated: false, completion: nil)
                }
                controller.performDeepLinkRouting()
            }
        }
    }

}

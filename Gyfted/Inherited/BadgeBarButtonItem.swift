//
//  BadgeBarButtonItem.swift
//  Gyfted
//
//  Created by Ben Dodson on 15/11/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

public class BadgeBarButtonItem: UIBarButtonItem {
    @IBInspectable
    public var badgeNumber: Int = 0 {
        didSet {
            self.updateBadge()
        }
    }
    
    private let label: UILabel
    
    required public init?(coder aDecoder: NSCoder) {
        let label = UILabel()
        label.backgroundColor = .red
        label.layer.cornerRadius = 7
        label.clipsToBounds = true
        label.isUserInteractionEnabled = false
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .white
        label.layer.zPosition = 1
        label.font = UIFont.systemFont(ofSize: 13)
        self.label = label
        
        super.init(coder: aDecoder)
        self.imageInsets = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 0)
        
        self.addObserver(self, forKeyPath: "view", options: [], context: nil)
    }
    
    // swiftlint:disable block_based_kvo
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        self.updateBadge()
    }
    
    private func updateBadge() {
        guard let view = self.value(forKey: "view") as? UIView else { return }
        
        self.label.text = "\(badgeNumber)"
        
        if self.badgeNumber > 0 && self.label.superview == nil {
            view.addSubview(self.label)
            
            self.label.heightAnchor.constraint(equalToConstant: 15).isActive = true
            self.label.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 24).isActive = true
            self.label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -8).isActive = true
        } else if self.badgeNumber == 0 && self.label.superview != nil {
            self.label.removeFromSuperview()
        }
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: "view")
    }
}

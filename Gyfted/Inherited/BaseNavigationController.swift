//
//  BaseNavigationController.swift
//  Gyfted
//
//  Created by Ben Dodson on 03/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return viewControllers.last?.preferredStatusBarStyle ?? .default
    }

    func applyAppearances() {
        navigationBar.shadowImage = UIImage()
        navigationBar.titleTextAttributes = [.foregroundColor: Config.colors.dark, .font: UIFont.systemFont(ofSize: 18, weight: .bold)]
        navigationBar.barTintColor = .white
        navigationBar.tintColor = Config.colors.tint
        navigationBar.backIndicatorImage = UIImage()
        navigationBar.backIndicatorTransitionMaskImage = UIImage()
        
        let states: [UIControl.State] = [.normal, .selected, .highlighted]
        for state in states {
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([.foregroundColor: Config.colors.tint, .font: Config.compactFont(ofSize: 16)], for: state)
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([.foregroundColor: Config.colors.tint, .font: Config.compactFont(ofSize: 16)], for: state)
        }
    }
}

// Used for UIAppearance darkening of navigation bars
class RegisterNavigationController: BaseNavigationController {
}

//
//  DynamicCollectionView.swift
//  Gyfted
//
//  Created by Ben Dodson on 26/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

public class DynamicCollectionView: UICollectionView {
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        if !bounds.size.equalTo(intrinsicContentSize) {
            invalidateIntrinsicContentSize()
        }
    }
    
    override public var intrinsicContentSize: CGSize {
        let intrinsicContentSize: CGSize = contentSize
        return intrinsicContentSize
    }
}

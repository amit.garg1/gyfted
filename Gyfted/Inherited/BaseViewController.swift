//
//  BaseViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 03/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    var fields = [UITextField]()
    var context: NSExtensionContext?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", comment: ""), style: .plain, target: nil, action: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resignResponders()
    }
    
    @IBAction func dismissView() {
        #if TARGET_IS_EXTENSION
            dismiss(animated: true) {
                self.context?.completeRequest(returningItems: self.context?.inputItems, completionHandler: nil)
            }
        #else
            dismiss(animated: true, completion: nil)
        #endif
        
    }

    func present(_ error: String) {
        let controller = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @objc func presentShare() {
        #if !TARGET_IS_EXTENSION
        guard let main = navigationController?.parent as? MainViewController else { return }
        main.presentShare()
        #endif
    }
    
}

// MARK: Keyboard Tricks
extension BaseViewController {
    
    @IBAction func resignResponders() {
        _ = fields.map { $0.resignFirstResponder() }
    }
    
    func setupKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIControl.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIControl.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size ?? .zero
        
        let tabbarHeight = tabBarController?.tabBar.frame.size.height ?? 0
        let toolbarHeight = navigationController?.toolbar.frame.size.height ?? 0
        let bottomInset = keyboardSize.height - tabbarHeight - toolbarHeight
        
        scrollView.contentInset.bottom = bottomInset
        scrollView.scrollIndicatorInsets.bottom = bottomInset
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
    }
}

//
//  BaseRegisterViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 10/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class BaseRegisterViewController: BaseViewController {

    var registrationProfile: RegistrationProfile?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? BaseRegisterViewController {
            controller.registrationProfile = registrationProfile
        }
    }
    
}

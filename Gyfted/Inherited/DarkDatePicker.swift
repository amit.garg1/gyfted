//
//  DarkDatePicker.swift
//  Gyfted
//
//  Created by Ben Dodson on 10/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class DarkDatePicker: UIDatePicker {
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setValue(false, forKey: "highlightsToday")
        setValue(UIColor.white, forKey: "textColor")
    }
}

//
//  SearchProfileTableViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/08/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class SearchProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var imageStackView: UIStackView!
    
    var profile: SearchProfile?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        for case let button as UIButton in subviews {
            let image = button.backgroundImage(for: .normal)?.withRenderingMode(.alwaysTemplate)
            button.setBackgroundImage(image, for: .normal)
        }
        avatarView.layer.borderWidth = 1
        avatarView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func use(_ profile: SearchProfile) {
        self.profile = profile
        avatarView.sd_setImage(with: profile.avatarURL, placeholderImage: nil, options: .retryFailed, completed: nil)
        
        nameLabel.text = profile.name
        if profile.isCreator {
            let string = NSMutableAttributedString(string: profile.name, attributes: [.font: UIFont.systemFont(ofSize: 14, weight: .semibold), .foregroundColor: Config.colors.text])
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "CreatorTick")?.withRenderingMode(.alwaysTemplate)
            attachment.bounds = CGRect(x: 0, y: -2, width: 14, height: 14)
            let attachmentString = NSMutableAttributedString(attachment: attachment)
            attachmentString.insert(NSAttributedString(string: " ", attributes: [.font: UIFont.systemFont(ofSize: 12)]), at: 0)
            attachmentString.addAttribute(.foregroundColor, value: Config.colors.tint, range: NSMakeRange(0, attachmentString.length))
            string.append(attachmentString)
            nameLabel.attributedText = string
        }
        
        contentLabel.text = profile.wishlistsCount == 1 ? "1 wishlist" : "\(profile.wishlistsCount) wishlists"
        
        imageStackView.superview?.isHidden = profile.imageUrls.count == 0
        var index = 0
        for case let imageView as UIImageView in imageStackView.arrangedSubviews {
            imageView.image = nil
            imageView.layer.borderWidth = 0
            imageView.layer.borderColor = UIColor.clear.cgColor
            imageView.layer.cornerRadius = 4
            if index < profile.imageUrls.count {
                imageView.sd_setImage(with: profile.imageUrls[index], placeholderImage: nil, options: .retryFailed, completed: nil)
                imageView.layer.borderWidth = 1
                imageView.layer.borderColor = Config.colors.light.cgColor
            }
            index += 1
        }
    }
    
}

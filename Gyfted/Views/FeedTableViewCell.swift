//
//  FeedTableViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 22/07/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

protocol FeedTableViewCellDelegate: class {
    func presentProfile(with id: String)
}

class FeedTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var imageStackView: UIStackView!
    
    var post: FeedPost?
    weak var delegate: FeedTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        for case let button as UIButton in subviews {
            let image = button.backgroundImage(for: .normal)?.withRenderingMode(.alwaysTemplate)
            button.setBackgroundImage(image, for: .normal)
        }
        avatarView.layer.borderWidth = 0.5
        avatarView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func use(_ post: FeedPost) {
        self.post = post
        avatarView.sd_setImage(with: post.avatarUrl, placeholderImage: nil, options: .retryFailed, completed: nil)
        dateLabel.text = post.postDate.timeAgo(numericDates: false)
        
        var highlights: [String] = [post.userName, post.wishlistName]
        var text = ""
        switch post.type {
        case .addedWishlist:
            text = "\(post.userName) created a new wishlist with \(post.metadata) items: \(post.wishlistName)."
        case .addedItem:
            let plural = post.metadata == "1" ? "item" : "items"
            text = "\(post.userName) added \(post.metadata) new \(plural) to \(post.wishlistName)."
        case .purchasedItem:
            text = "\(post.userName) purchased \(post.metadata) from \(post.wishlistName)."
            highlights.append(post.metadata)
        }
        
        let attributed = NSMutableAttributedString(string: text, attributes: [.foregroundColor: Config.colors.text, .font: UIFont.systemFont(ofSize: 15), .kern: 0.52])
        for highlight in highlights {
            attributed.addAttribute(.font, value: UIFont.systemFont(ofSize: 15, weight: .semibold), range: (text as NSString).range(of: highlight))
        }
        contentLabel.attributedText = attributed
        
        imageStackView.superview?.isHidden = post.imageUrls.count == 0
        var index = 0
        for case let imageView as UIImageView in imageStackView.arrangedSubviews {
            imageView.image = nil
            imageView.layer.borderWidth = 0
            imageView.layer.borderColor = UIColor.clear.cgColor
            imageView.layer.cornerRadius = 4
            if index < post.imageUrls.count {
                imageView.sd_setImage(with: post.imageUrls[index], placeholderImage: nil, options: .retryFailed, completed: nil)
                imageView.layer.borderWidth = 1
                imageView.layer.borderColor = Config.colors.light.cgColor
            }
            index += 1
        }
    }
    
    @IBAction func presentProfile(_ sender: Any) {
        guard let post = post else { return }
        delegate?.presentProfile(with: post.userId)
    }
    
}

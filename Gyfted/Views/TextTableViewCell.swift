//
//  TextTableViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 18/11/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class TextTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    func use(_ text: String) {
        label.text = text
    }
    
}

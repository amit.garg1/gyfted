//
//  WishlistCollectionViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 26/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class WishlistCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backingView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        backingView.layer.applySketchShadow(color: .black, alpha: 0.3, x: 0, y: 1, blur: 4, spread: 0)
    }
    
    func use(_ wishlist: Wishlist) {
        nameLabel.text = wishlist.name
        nameLabel.kern(0.59)
        imageView.sd_setImage(with: wishlist.imageUrl, completed: nil)
    }
    
    func use(_ item: Item, showPrice: Bool = false, showPurchaseStatus: Bool = false) {
        let string = showPrice ? "\(item.price) • \(item.name)" : item.name
        let attributed = NSMutableAttributedString(string: string, attributes: [.foregroundColor: Config.colors.dark, .font: UIFont.systemFont(ofSize: 14)])
        attributed.addAttributes([.foregroundColor: Config.colors.blue, .font: UIFont.systemFont(ofSize: 14, weight: .semibold)], range: (string as NSString).range(of: item.price))
        
        imageView.alpha = 1
        
        if showPurchaseStatus && item.purchasedBy != nil {
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "PurchasedTick")
            attachment.bounds = CGRect(x: 0, y: -1, width: 12, height: 12)
            let attachmentString = NSMutableAttributedString(attachment: attachment)
            attachmentString.append(NSAttributedString(string: " ", attributes: [.font: UIFont.systemFont(ofSize: 14)]))
            attributed.insert(attachmentString, at: 0)
            imageView.alpha = 0.5
        }
        
        nameLabel.attributedText = attributed
        imageView.sd_setImage(with: item.imageUrls.first, completed: nil)
    }

}

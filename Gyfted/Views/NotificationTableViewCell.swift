//
//  NotificationTableViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 02/08/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

protocol NotificationTableViewCellDelegate: class {
    func requestAccepted(_ notification: GyftedNotification)
    func requestRejected(_ notification: GyftedNotification)
    func presentProfile(with id: String)
}

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var requestButtonsContainer: UIView!
    
    var notification: GyftedNotification?
    weak var delegate: NotificationTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarView.layer.borderWidth = 0.5
        avatarView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func use(_ notification: GyftedNotification) {
        self.notification = notification
        
        avatarView.sd_setImage(with: notification.imageUrl, placeholderImage: nil, options: .retryFailed, completed: nil)
        dateLabel.text = notification.sentDate.timeAgo(numericDates: false)
       
        requestButtonsContainer.isHidden = true
        
        var text = notification.text
        switch notification.notificationType {
        case .addressRequest:
            if notification.notificationRequestStatus == .accepted {
                text = "You accepted this address request from \(notification.highlight)."
            } else {
                requestButtonsContainer.isHidden = false
            }
        case .friendRequest:
            if notification.notificationRequestStatus == .accepted {
                text = "You accepted this friend request from \(notification.highlight)."
            } else {
                requestButtonsContainer.isHidden = false
            }
        default:
            break
        }
        let attributed = NSMutableAttributedString(string: text, attributes: [.foregroundColor: Config.colors.text, .font: UIFont.systemFont(ofSize: 15), .kern: 0.52])
        if notification.highlight.isEmpty == false {
            attributed.addAttribute(.underlineStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: (text as NSString).range(of: notification.highlight))
        }
        contentLabel.attributedText = attributed
        backgroundColor = notification.isRead ? .white : Config.colors.tint.withAlphaComponent(0.1)
    }
    
    @IBAction func acceptRequest(_ sender: Any) {
        guard let notification = notification else { return }
        delegate?.requestAccepted(notification)
    }
    
    @IBAction func rejectRequest(_ sender: Any) {
        guard let notification = notification else { return }
        delegate?.requestRejected(notification)
    }
    
    @IBAction func presentProfile(_ sender: Any) {
        guard let id = notification?.profileId else { return }
        delegate?.presentProfile(with: id)
    }
}

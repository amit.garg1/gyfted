//
//  HomeCollectionCarouselView.swift
//  Gyfted
//
//  Created by Ben Dodson on 21/08/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit

protocol HomeCollectionCarouselViewDelegate: class {
    func carouselViewPresentItem(_ item: Item)
}

class HomeCollectionCarouselView: UIView {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate: HomeCollectionCarouselViewDelegate?
    
    var collection: Collection?
    
    // swiftlint:disable force_cast
    class func instance() -> HomeCollectionCarouselView {
        let nib = UINib(nibName: "HomeCollectionCarouselView", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! HomeCollectionCarouselView
    }
    
    func use(_ collection: Collection) {
        self.collection = collection
        label.text = collection.name
        collectionView.register(UINib(nibName: "HomeItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeItemCollectionViewCell")
        collectionView.reloadData()
    }
}

extension HomeCollectionCarouselView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collection?.items.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeItemCollectionViewCell", for: indexPath) as! HomeItemCollectionViewCell
        if let item = collection?.items[indexPath.row] {
            cell.use(item)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let item = collection?.items[indexPath.row] else { return }
        delegate?.carouselViewPresentItem(item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 95, height: 140)
    }
}


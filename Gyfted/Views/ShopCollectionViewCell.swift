//
//  ShopCollectionViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 12/05/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit

class ShopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var logo: UIImageView!

    func use(_ shop: Shop) {
        logo.sd_setImage(with: shop.imageUrl, placeholderImage: nil, options: .retryFailed, completed: nil)
    }

}

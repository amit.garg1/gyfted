//
//  ItemDetailView.swift
//  Gyfted
//
//  Created by Ben Dodson on 09/05/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Analytics

protocol ItemDetailViewDelegate: class {
    func presentPurchaseItemView(for item: Item, showPurchaseStatus: Bool)
    func presentAddItemView(for item: Item)
    func presentItems(with tag: String)
    func presentProfile(with id: String)
    func presentAlert(_ controller: UIAlertController)
}

class ItemDetailView: UIView {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var friendButton: UIButton!
    @IBOutlet weak var friendRequestLabel: UILabel!
    
    weak var delegate: ItemDetailViewDelegate?
    weak var itemViewController: ItemViewController?
    
    var sizingCell: KeywordCollectionViewCell?
    
    var item: Item?
    var profile: Profile?
    var keywords = [String]()
    var showPurchaseStatus = true
    
    // swiftlint:disable force_cast
    class func instance() -> ItemDetailView {
        let nib = UINib(nibName: "ItemDetailView", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! ItemDetailView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: "KeywordCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "KeywordCollectionViewCell")
        NotificationCenter.default.addObserver(self, selector: #selector(updatePurchaseButton), name: .itemPurchaseStatusDidChange, object: nil)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(presentPurchaseItem(_:)))
        scrollView.addGestureRecognizer(tapRecognizer)
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(toggleLike))
        doubleTapRecognizer.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTapRecognizer)
        tapRecognizer.require(toFail: doubleTapRecognizer)
        
        let profileTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(presentProfile))
        avatarView.superview?.addGestureRecognizer(profileTapRecognizer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePurchaseButton()
    }
    
    // swiftlint:disable function_body_length
    func use(_ item: Item, and profile: Profile) {
        self.item = item
        self.keywords = item.keywords
        self.profile = profile
        
        usernameLabel.text = profile.name
        if profile.isCreator {
            let string = NSMutableAttributedString(string: profile.name, attributes: [.font: UIFont.systemFont(ofSize: 14, weight: .bold), .foregroundColor: Config.colors.text])
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "CreatorTick")?.withRenderingMode(.alwaysTemplate)
            attachment.bounds = CGRect(x: 0, y: -2, width: 14, height: 14)
            let attachmentString = NSMutableAttributedString(attachment: attachment)
            attachmentString.insert(NSAttributedString(string: " ", attributes: [.font: UIFont.systemFont(ofSize: 12)]), at: 0)
            attachmentString.addAttribute(.foregroundColor, value: Config.colors.tint, range: NSMakeRange(0, attachmentString.length))
            string.append(attachmentString)
            nameLabel.attributedText = string
        }
        avatarView.sd_setImage(with: profile.avatarURL, placeholderImage: nil, options: .retryFailed, progress: nil, completed: nil)
        avatarView.layer.borderColor = Config.colors.light.cgColor
        avatarView.layer.borderWidth = 1
        
        friendButton.isHidden = true
        friendRequestLabel.isHidden = true
        
        if profile.id != Profile.userId && profile.isFriend == false {
            friendButton.isHidden = profile.isRequestSent
            friendRequestLabel.isHidden = !friendButton.isHidden
        }
               
        pager.numberOfPages = item.imageUrls.count
        pager.isHidden = pager.numberOfPages <= 1
        
        _ = scrollView.subviews.map { $0.removeFromSuperview() }
        var x: CGFloat = 0
        for imageUrl in item.imageUrls {
            let imageView = UIImageView(frame: CGRect(x: x, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height))
            imageView.clipsToBounds = true
            imageView.contentMode = .scaleAspectFill
            imageView.backgroundColor = Config.colors.light
            imageView.sd_setImage(with: imageUrl, completed: nil)
            scrollView.addSubview(imageView)
            x += scrollView.frame.size.width
        }
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width * CGFloat(item.imageUrls.count), height: scrollView.frame.size.height)
        
        updatePurchaseButton()
        
        var metadata = [String]()
        metadata.append(item.name)
        if item.size.isEmpty == false {
            metadata.append(item.size)
        }
        if item.color.isEmpty == false {
            metadata.append(item.color)
        }
        nameLabel.text = metadata.joined(separator: " • ")
    
        descriptionLabel.text = item.text
        descriptionLabel.kern(0.59)
        descriptionLabel.superview?.isHidden = item.text.isEmpty
        
        if keywords.count == 0 {
            collectionView.superview?.isHidden = true
        } else {
            let nib = Bundle.main.loadNibNamed("KeywordCollectionViewCell", owner: KeywordCollectionViewCell.self, options: nil)
            sizingCell = nib?.first as? KeywordCollectionViewCell
            collectionView.reloadData()
            collectionView.layoutSubviews()
            collectionViewHeightConstraint.constant = collectionView.contentSize.height
            layoutSubviews()
        }
        
        updateLikeButton()
        
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "item/\(item.id)/liked"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                if let data = data, let response = try? JSONDecoder().decode(ItemLikedResponse.self, from: data) {
                    Config.sharedDefaults().set(response.isLiked, forKey: item.likeKey)
                }
            case .failure:
                break
            }
            self.updateLikeButton()
        }
    }
    
    func updateLikeButton() {
        guard let item = item else { return }
        likeButton.setImage(UIImage(named: item.isLiked ? "ItemIconLiked" : "ItemIconLike"), for: .normal)
    }
    
    @objc func updatePurchaseButton() {
        guard let item = item else { return }
        
        //purchaseStatusView.isHidden = !showPurchaseStatus
        var state = NSLocalizedString("Not purchased", comment: "")
        var showPurchaseTick = false
        purchaseButton.isHidden = false
        
        if let purchasedBy = item.purchasedBy {
            showPurchaseTick = true
            if purchasedBy == Profile.userId {
                state = NSLocalizedString("Purchased by you", comment: "")
            } else {
                state = NSLocalizedString("Purchased", comment: "")
                purchaseButton.isHidden = true
            }
        }
        
        var string = ""
        if item.price.isEmpty == false {
            string += "\(item.price) • "
        }
        if showPurchaseTick {
            string += "TICK "
        }
        string += state
        
        var attributes = [NSAttributedString.Key: Any]()
        attributes[.font] = priceLabel.font
        attributes[.foregroundColor] = priceLabel.textColor
        let attributed = NSMutableAttributedString(string: string, attributes: attributes)
        
        let range = (string as NSString).range(of: "TICK")
        if range.location != NSNotFound {
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "PurchasedTick")
            attachment.bounds = CGRect(x: 0, y: -1, width: 17, height: 16)
            let attachmentString = NSMutableAttributedString(attachment: attachment)
            attributed.replaceCharacters(in: range, with: attachmentString)
        }
        priceLabel.attributedText = attributed
    }
    
    @IBAction func toggleLike(_ sender: Any) {
        guard let item = item else { return }
        let isLiked = !item.isLiked
        if isLiked {
            Config.sharedDefaults().set(true, forKey: item.likeKey)
            var data = [String: Any]()
            data["user_id"] = Profile.userId
            data["item_id"] = item.id
        } else {
            Config.sharedDefaults().removeObject(forKey: item.likeKey)
        }
        
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "item/\(item.id)/like"), parameters: ["like": isLiked ? 1 : 0], onCompletion: nil)
        updateLikeButton()
    }
    
    @IBAction func presentAddItem(_ sender: UIButton) {
        guard let item = item else { return }
        delegate?.presentAddItemView(for: item)
    }
    
    @IBAction func presentShareItem(_ sender: UIButton) {
        itemViewController?.presentShareItem()
    }
    
    @IBAction func presentPurchaseItem(_ sender: Any) {
        guard let item = item else { return }
        delegate?.presentPurchaseItemView(for: item, showPurchaseStatus: showPurchaseStatus)
    }
    
    @IBAction func presentProfile() {
        guard let profile = profile else { return }
        delegate?.presentProfile(with: profile.id)
    }
    
    @IBAction func sendFriendRequest(_ sender: Any) {
        let controller = UIAlertController(title: "Send Friend Request", message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "Friend", style: .default, handler: { (_) in
            self.performSendFriendRequest(isClose: false)
        }))
        controller.addAction(UIAlertAction(title: "Close Friend", style: .default, handler: { (_) in
            self.performSendFriendRequest(isClose: true)
        }))
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        delegate?.presentAlert(controller)
    }
    
    private func performSendFriendRequest(isClose: Bool) {
        guard let profile = profile else { return }

        let parameters: [String: Any] = ["close": isClose ? 1 : 0]
        if let notification = DataManager.shared.notifications.filter({$0.profileId == profile.id && $0.requestStatus == NotificationRequestStatus.pending.rawValue }).first {
            _ = DataManager.shared.notifications.filter({$0.id == notification.id}).map({
                $0.requestStatus = NotificationRequestStatus.accepted.rawValue
                $0.isRead = true
            })
            NotificationCenter.default.post(name: .notificationsDidChange, object: nil)
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "request/\(notification.id)/accept"), parameters: parameters) { (_) in
                Analytics.logEvent("accepted_friend_request", parameters: [
                    "user_id": Profile.userId as NSObject,
                    "friend_id": profile.id as NSObject
                ])
            }
            profile.isFriend = true
            friendButton.isHidden = true
        } else {
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/\(profile.id)/sendFriendRequest"), parameters: parameters, onCompletion: nil)
            profile.isRequestSent = true
            friendButton.isHidden = true
            friendRequestLabel.isHidden = false
            Analytics.logEvent("send_friend_request", parameters: [
                "user_id": Profile.userId as NSObject,
                "friend_id": profile.id as NSObject
            ])
        }
        SEGAnalytics.shared()?.track("Added Friend", properties: ["id": "\(profile.id)"])
    }
    
}

extension ItemDetailView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pager.currentPage = Int((scrollView.contentOffset.x / scrollView.bounds.size.width).rounded())
    }
    
}

extension ItemDetailView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return keywords.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KeywordCollectionViewCell", for: indexPath) as! KeywordCollectionViewCell
        cell.use(keywords[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let keyword = keywords[indexPath.row]
        guard let sizingCell = sizingCell else {
            return .zero
        }
        
        sizingCell.use(keyword)
        sizingCell.setNeedsLayout()
        sizingCell.layoutIfNeeded()
        
        let size = sizingCell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        return CGSize(width: size.width, height: 24)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.presentItems(with: keywords[indexPath.row])
    }
    
}

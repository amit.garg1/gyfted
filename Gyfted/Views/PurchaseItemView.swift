//
//  PurchaseItemView.swift
//  Gyfted
//
//  Created by Ben Dodson on 09/05/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import MapKit
import Analytics

protocol PurchaseItemViewDelegate: class {
    func dismissPurchaseItemSheet()
    func openPurchaseWebsite(item: Item)
}

class PurchaseItemView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webComponent: UIView!
    @IBOutlet weak var mapComponent: UIView!
    @IBOutlet weak var storeAddressComponent: UIView!
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var storeLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var disclosureArrow: UIImageView!
    @IBOutlet weak var purchaseLabel: UILabel!
    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var purchaseStackView: UIStackView!
    
    weak var delegate: PurchaseItemViewDelegate?
    
    var item: Item?
    var showPurchaseStatus = true
    
    // swiftlint:disable force_cast
    class func instance() -> PurchaseItemView {
        let nib = UINib(nibName: "PurchaseItemView", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! PurchaseItemView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.kern(0.68)
    }
    
    func use(_ item: Item, showPurchaseStatus: Bool) {
        
        self.item = item
        self.showPurchaseStatus = showPurchaseStatus
        purchaseStackView.isHidden = !showPurchaseStatus
        
        webComponent.isHidden = true
        mapComponent.isHidden = true
        storeAddressComponent.isHidden = true
        
        if item.url.isEmpty == false {
            webComponent.isHidden = false
            webLabel.attributedText = NSAttributedString(string: item.url, attributes: [.font: UIFont.systemFont(ofSize: 16), .foregroundColor: Config.colors.text, .underlineStyle: NSNumber(value: NSUnderlineStyle.single.rawValue)])
        }
        
        if item.storeName.isEmpty == false {
            storeAddressComponent.isHidden = false
            storeLabel.text = item.formattedStoreAddress
            disclosureArrow.isHidden = item.storeAddress.isEmpty
        }
        
        if let lat = item.storeLat, let lng = item.storeLng {
            mapComponent.isHidden = false
            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            mapView.setRegion(MKCoordinateRegion(center: coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000), animated: false)
            let annotation = MKPointAnnotation()
            annotation.title = item.storeName
            annotation.coordinate = coordinate
            mapView.addAnnotation(annotation)
        }
        
        if item.purchasedBy == nil {
            purchaseLabel.text = NSLocalizedString("Let others know if you have already purchased this item.", comment: "")
            purchaseButton.setTitle(NSLocalizedString("Yes, this item is purchased", comment: "").uppercased(), for: .normal)
        } else {
            purchaseLabel.text = NSLocalizedString("If you changed your mind, you can mark this item as available again.", comment: "")
            purchaseButton.setTitle(NSLocalizedString("Mark this item as unpurchased", comment: "").uppercased(), for: .normal)
        }
    }
    
    @IBAction func presentWebsite(_ sender: Any) {
        guard let item = item else { return }
        delegate?.openPurchaseWebsite(item: item)
    }
    
    @IBAction func presentStoreLocation(_ sender: Any) {
        guard let item = item, let lat = item.storeLat, let lng = item.storeLng else { return }
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let span = MKCoordinateRegion(center: coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = item.storeName
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: span.center)])
    }
    
    @IBAction func dismissSheet(_ sender: Any) {
        delegate?.dismissPurchaseItemSheet()
    }
    
    @IBAction func purchase(_ sender: UIButton) {
        guard let item = item else { return }
        
        if item.purchasedBy == nil {
            item.purchasedBy = Profile.userId
            
            var properties = [String: String]()
            properties["id"] = "\(item.id)"
            SEGAnalytics.shared()?.track("Item Purchased", properties: properties)
            
        } else {
            item.purchasedBy = nil
        }
        use(item, showPurchaseStatus: showPurchaseStatus)
        
        var data = [String: Any]()
        data["id"] = item.id
        data["purchasedBy"] = item.purchasedBy ?? NSNull()
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "item/save"), parameters: data) { (handler) in
            switch handler {
            case .success:
                break
            case .failure(let error):
                NSLog("ERROR: \(error)")
            }
        }
    
        NotificationCenter.default.post(name: .itemPurchaseStatusDidChange, object: nil)
    }
}

//
//  HomeShopCollectionViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 21/08/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit

class HomeShopCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    func use(_ shop: Shop) {
        label.text = shop.name
        imageView.sd_setImage(with: shop.iconUrl, placeholderImage: nil, options: .retryFailed, completed: nil)
    }
    
}

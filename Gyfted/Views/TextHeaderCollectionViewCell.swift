//
//  TextHeaderCollectionViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/05/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit

class TextHeaderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func use(_ string: String, padding: CGFloat = 10) {
        label.text = string
        topConstraint.constant = padding
        bottomConstraint.constant = padding
    }

}

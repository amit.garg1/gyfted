//
//  PaymentLinksView.swift
//  Gyfted
//
//  Created by Ben Dodson on 04/02/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit

protocol PaymentLinksViewDelegate: class {
    func dismissPaymentLinksSheet()
    func openPaymentsLink(url: URL)
}

class PaymentLinksView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var paypalLabel: UILabel!
    @IBOutlet weak var venmoLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    weak var delegate: PaymentLinksViewDelegate?
    var profile: Profile?
    
    // swiftlint:disable force_cast
    class func instance() -> PaymentLinksView {
        let nib = UINib(nibName: "PaymentLinksView", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! PaymentLinksView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.kern(0.68)
    }
    
    func use(_ profile: Profile) {
        self.profile = profile
        
        paypalLabel.superview?.isHidden = true
        venmoLabel.superview?.isHidden = true
        let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 16, weight: .medium), .foregroundColor: Config.colors.text, .underlineStyle: NSNumber(value: NSUnderlineStyle.single.rawValue)]
        if profile.paypal.isEmpty == false {
            paypalLabel.attributedText = NSAttributedString(string: "paypal.me/" + profile.paypal, attributes: attributes)
            paypalLabel.superview?.isHidden = false
        }
        if profile.venmo.isEmpty == false {
            venmoLabel.attributedText = NSAttributedString(string: "@" + profile.venmo, attributes: attributes)
            venmoLabel.superview?.isHidden = false
        }
    }
    
    @IBAction func presentPaypal(_ sender: Any) {
        guard let profile = profile, let url = URL(string: "https://paypal.me/" + profile.paypal) else { return }
        delegate?.openPaymentsLink(url: url)
    }
    
    @IBAction func presentVenmo(_ sender: Any) {
        guard let profile = profile, let url = URL(string: "https://venmo.com/" + profile.venmo) else { return }
        delegate?.openPaymentsLink(url: url)
    }
    
    @IBAction func dismissSheet(_ sender: Any) {
        delegate?.dismissPaymentLinksSheet()
    }
}

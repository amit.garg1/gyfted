//
//  ImageCollectionViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 25/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var indexLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.layer.cornerRadius = 4
        imageView.layer.borderColor = Config.colors.tint.cgColor
    }
    
    func use(_ url: URL?, selectedIndex: Int? = nil) {
        imageView.sd_setImage(with: url, placeholderImage: nil, options: .retryFailed, context: nil)
        
        if let selectedIndex = selectedIndex {
            imageView.layer.borderWidth = 2
            indexLabel.text = "\(selectedIndex + 1)"
            indexLabel.isHidden = false
        } else {
            imageView.layer.borderWidth = 0
            indexLabel.isHidden = true
        }
        
    }
    
    func use(_ image: UIImage?) {
        imageView.image = image
        indexLabel.isHidden = true
    }
}

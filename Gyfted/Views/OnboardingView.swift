//
//  OnboardingView.swift
//  Gyfted
//
//  Created by Ben Dodson on 11/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class OnboardingView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    
    // swiftlint:disable force_cast
    class func instance() -> OnboardingView {
        let nib = UINib(nibName: "OnboardingView", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! OnboardingView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIScreen.main.bounds.size.width <= 320 {
            imageViewHeightConstraint.constant = 200
            imageView.contentMode = .scaleAspectFit
        }
    }
    
    func use(_ page: Int) {
        imageView.image = UIImage(named: "Onboarding-\(page)")
        switch page {
        case 1:
            textLabel.text = NSLocalizedString("Create wishlists for any reason of event!", comment: "")
        case 2:
            textLabel.text = NSLocalizedString("Follow trending categories to discover gifts for your wishlists", comment: "")
        case 3:
            textLabel.text = NSLocalizedString("Add gifts you love to your wishlists by tapping on the + button", comment: "")
        case 4:
            textLabel.text = NSLocalizedString("Share your wishlists with family and friends!", comment: "")
        default:
            break
        }
    }
}

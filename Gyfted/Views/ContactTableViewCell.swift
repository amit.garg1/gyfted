//
//  ContactTableViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 18/11/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

protocol ContactTableViewCellDelegate: class {
    func contactCellButtonWasTapped(indexPath: IndexPath)
}

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var metaLabel: UILabel!
    @IBOutlet weak var buttonLabel: UILabel!
    
    var indexPath: IndexPath?
    weak var delegate: ContactTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarView.layer.borderWidth = 1
        avatarView.layer.borderColor = UIColor.lightGray.cgColor
        avatarView.backgroundColor = Config.colors.light
        buttonLabel?.superview?.layer.cornerRadius = 15
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(buttonTapped))
        buttonLabel?.superview?.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func buttonTapped() {
        guard let indexPath = indexPath else { return }
        delegate?.contactCellButtonWasTapped(indexPath: indexPath)
    }
    
    func use(_ contact: Contact, indexPath: IndexPath) {
        
        self.indexPath = indexPath
        avatarView.image = nil
        
        if let profile = contact.profile {
            avatarView.sd_setImage(with: profile.avatarURL, completed: nil)
            nameLabel.text = profile.name
            metaLabel.text = "@" + profile.username
            
            if profile.isFriend {
                buttonLabel?.text = "Friend"
                buttonLabel?.superview?.backgroundColor = .clear
                buttonLabel?.textColor = .lightGray
            } else if profile.isRequestSent {
                buttonLabel?.text = "Request Sent"
                buttonLabel?.superview?.backgroundColor = .clear
                buttonLabel?.textColor = .lightGray
            } else {
                buttonLabel?.text = "Add Friend"
                buttonLabel?.superview?.backgroundColor = Config.colors.tint
                buttonLabel?.textColor = .white
            }
            
        } else {
            if let data = contact.imageData {
                avatarView.image = UIImage(data: data)
            } else {
                var identifier = contact.emailAddresses.first
                if identifier == nil {
                    identifier = contact.phoneNumbers.first
                }
                let string = "https://www.gravatar.com/avatar/\(identifier?.hashed(.md5) ?? "")?s=200&default=identicon"
                avatarView.sd_setImage(with: URL(string: string), completed: nil)
            }
            
            nameLabel.text = contact.name
            metaLabel.text = contact.metaText
            
            buttonLabel?.text = "Invite Friend"
            buttonLabel?.superview?.backgroundColor = Config.colors.tint
            buttonLabel?.textColor = .white
        }
        
    }
}

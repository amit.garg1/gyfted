//
//  TopicTableViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 17/07/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

protocol TopicTableViewCellDelegate: class {
    func toggleFollow(at indexPath: IndexPath)
    func didSelect(item: Item, in topic: Topic)
}

class TopicTableViewCell: UITableViewCell {

    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var avatarView: UIImageView!
    
    var topic: Topic?
    var indexPath: IndexPath?
    
    weak var delegate: TopicTableViewCellDelegate?
    
    lazy var imageViews: [UIImageView] = {
        guard let stackViews = stackView.arrangedSubviews as? [UIStackView] else { return [] }
        return stackViews.flatMap({$0.arrangedSubviews}) as? [UIImageView] ?? []
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarView.layer.borderWidth = 0.5
        avatarView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func use(_ topic: Topic, indexPath: IndexPath) {
        self.topic = topic
        self.indexPath = indexPath
        topicLabel.text = topic.name
        followButton.setTitle(topic.isFollowed ? NSLocalizedString("Following", comment: "").uppercased() : NSLocalizedString("Follow", comment: "").uppercased(), for: .normal)
        
        avatarView.sd_setImage(with: URL(string: topic.imageUrlString), placeholderImage: nil, options: .retryFailed, progress: nil, completed: nil)

        for index in 0..<imageViews.count {
            let imageView = imageViews[index]
            imageView.image = nil
            imageView.layer.borderColor = UIColor.clear.cgColor
            imageView.layer.borderWidth = 1
            imageView.layer.cornerRadius = 5
            if index < topic.items.count {
                let item = topic.items[index]
                imageView.layer.borderColor = Config.colors.light.cgColor
                imageView.sd_setImage(with: item.imageUrls.first, placeholderImage: nil, options: .retryFailed, completed: nil)
            }
        }
        
    }
    
    @IBAction func toggleFollow(_ sender: Any) {
        guard let indexPath = indexPath, let delegate = delegate else { return }
        delegate.toggleFollow(at: indexPath)
    }
    
    @IBAction func didTapItem(_ button: UIButton) {
        guard let topic = topic, button.tag < topic.items.count else { return }
        let item = topic.items[button.tag]
        delegate?.didSelect(item: item, in: topic)
    }
}

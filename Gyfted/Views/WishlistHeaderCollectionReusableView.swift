//
//  WishlistHeaderCollectionReusableView.swift
//  Gyfted
//
//  Created by Ben Dodson on 23/11/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Analytics

protocol WishlistHeaderDelegate: class {
    func presentProfile(with id: String)
    func presentAlert(_ controller: UIAlertController)
}

class WishlistHeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var friendButton: UIButton!
    @IBOutlet weak var friendRequestLabel: UILabel!
    
    var profile: Profile?
    weak var delegate: WishlistHeaderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let profileTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(presentProfile))
        avatarView.superview?.addGestureRecognizer(profileTapRecognizer)
    }
    
    func use(_ wishlist: Wishlist, and profile: Profile?) {
        
        guard let profile = profile else { return }
        
        self.profile = profile
        
        usernameLabel.text = profile.name
        if profile.isCreator {
            let string = NSMutableAttributedString(string: profile.name, attributes: [.font: UIFont.systemFont(ofSize: 14, weight: .bold), .foregroundColor: Config.colors.text])
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "CreatorTick")?.withRenderingMode(.alwaysTemplate)
            attachment.bounds = CGRect(x: 0, y: -2, width: 14, height: 14)
            let attachmentString = NSMutableAttributedString(attachment: attachment)
            attachmentString.insert(NSAttributedString(string: " ", attributes: [.font: UIFont.systemFont(ofSize: 12)]), at: 0)
            attachmentString.addAttribute(.foregroundColor, value: Config.colors.tint, range: NSMakeRange(0, attachmentString.length))
            string.append(attachmentString)
            nameLabel.attributedText = string
        }
        avatarView.sd_setImage(with: profile.avatarURL, placeholderImage: nil, options: .retryFailed, progress: nil, completed: nil)
        avatarView.layer.borderColor = Config.colors.light.cgColor
        avatarView.layer.borderWidth = 1
        
        friendButton.isHidden = true
        friendRequestLabel.isHidden = true
        
        if profile.id != Profile.userId && profile.isFriend == false {
            friendButton.isHidden = profile.isRequestSent
            friendRequestLabel.isHidden = !friendButton.isHidden
        }
        
        nameLabel.text = wishlist.name.uppercased()
        dateLabel.text = wishlist.date
        dateLabel.isHidden = (wishlist.date ?? "").isEmpty
    }
    
    @IBAction func presentProfile() {
        guard let profile = profile else { return }
        delegate?.presentProfile(with: profile.id)
    }
    
    @IBAction func sendFriendRequest(_ sender: Any) {
        let controller = UIAlertController(title: "Send Friend Request", message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "Friend", style: .default, handler: { (_) in
            self.performSendFriendRequest(isClose: false)
        }))
        controller.addAction(UIAlertAction(title: "Close Friend", style: .default, handler: { (_) in
            self.performSendFriendRequest(isClose: true)
        }))
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        delegate?.presentAlert(controller)
    }
    
    private func performSendFriendRequest(isClose: Bool) {
        guard let profile = profile else { return }

        let parameters: [String: Any] = ["close": isClose ? 1 : 0]
        if let notification = DataManager.shared.notifications.filter({$0.profileId == profile.id && $0.requestStatus == NotificationRequestStatus.pending.rawValue }).first {
            _ = DataManager.shared.notifications.filter({$0.id == notification.id}).map({
                $0.requestStatus = NotificationRequestStatus.accepted.rawValue
                $0.isRead = true
            })
            NotificationCenter.default.post(name: .notificationsDidChange, object: nil)
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "request/\(notification.id)/accept"), parameters: parameters) { (_) in
                Analytics.logEvent("accepted_friend_request", parameters: [
                    "user_id": Profile.userId as NSObject,
                    "friend_id": profile.id as NSObject
                ])
            }
            profile.isFriend = true
            friendButton.isHidden = true
        } else {
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/\(profile.id)/sendFriendRequest"), parameters: parameters, onCompletion: nil)
            profile.isRequestSent = true
            friendButton.isHidden = true
            friendRequestLabel.isHidden = false
            Analytics.logEvent("send_friend_request", parameters: [
                "user_id": Profile.userId as NSObject,
                "friend_id": profile.id as NSObject
            ])
        }
        SEGAnalytics.shared()?.track("Added Friend", properties: ["id": "\(profile.id)"])
    }
    
}

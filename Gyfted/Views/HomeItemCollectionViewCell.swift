//
//  HomeItemCollectionViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 21/08/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit

class HomeItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    func use(_ item: Item) {
        label.text = item.name
        imageView.sd_setImage(with: item.imageUrls.first, placeholderImage: nil, options: .retryFailed, completed: nil)
    }
    
}

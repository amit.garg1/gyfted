//
//  AddButtonCollectionViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 09/05/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class AddButtonCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    var heightPercentage: CGFloat = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func use(_ title: String, heightPercentage: CGFloat) {
        label.text = title
        label.kern(0.68)
        self.heightPercentage = heightPercentage
        updateHeightConstraint()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateHeightConstraint()
    }
    
    func updateHeightConstraint() {
        heightConstraint.constant = (frame.size.height * heightPercentage).rounded()
    }

}

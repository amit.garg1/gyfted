//
//  ItemAddedView.swift
//  Gyfted
//
//  Created by Ben Dodson on 15/09/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import SDWebImage

protocol ItemAddedViewDelegate: class {
    func itemAddedViewPresentShare()
    func itemAddedViewDismiss()
}

class ItemAddedView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    weak var delegate: ItemAddedViewDelegate?
    
    // swiftlint:disable force_cast
    class func instance() -> ItemAddedView {
        let nib = UINib(nibName: "ItemAddedView", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! ItemAddedView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.layer.borderColor = Config.colors.light.cgColor
        imageView.layer.borderWidth = 2
    }
    
    func use(_ name: String, imageURL: URL?) {
        nameLabel.text = name
        imageView.sd_setImage(with: imageURL, completed: nil)
    }
    
    @IBAction func presentShare(_ sender: Any) {
        delegate?.itemAddedViewPresentShare()
    }
    
    @IBAction func dismissView(_ sender: Any) {
        delegate?.itemAddedViewDismiss()
    }
    
}

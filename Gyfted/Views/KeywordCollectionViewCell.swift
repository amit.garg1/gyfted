//
//  KeywordCollectionViewCell.swift
//  Gyfted
//
//  Created by Ben Dodson on 09/05/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class KeywordCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func use(_ keyword: String) {
        label.text = keyword
    }

}

//
//  AddItemImagesViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/05/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit

class AddItemImagesViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var wishlist: Wishlist?
    var product: PotentialProduct?
    var isFirstItem = false
    var isFromInternalBrowser = false
    
    var selectedIndexes = [Int]()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? AddItemDetailsViewController {
            controller.wishlist = wishlist
            controller.isFirstItem = isFirstItem
            #if TARGET_IS_EXTENSION
                controller.context = context
            #endif
            controller.isFromInternalBrowser = isFromInternalBrowser
            
            if let originalProduct = product {
                var images = [String]()
                for index in selectedIndexes {
                    images.append(originalProduct.images[index])
                }
                controller.product = PotentialProduct(title: originalProduct.title, description: originalProduct.description, url: originalProduct.url, images: images)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Item Details", comment: "").uppercased()
        collectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        collectionView.register(UINib(nibName: "TextHeaderCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "TextHeaderCollectionViewCell")
        
        #if TARGET_IS_EXTENSION
            navigationController?.viewControllers = [self]
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
        #endif
    }

}

extension AddItemImagesViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product?.images.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let product = product, let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell else { fatalError("Could not load ImageCollectionViewCell") }
        
        cell.use(URL(string: product.images[indexPath.row]), selectedIndex: selectedIndexes.firstIndex(of: indexPath.row))
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((UIScreen.main.bounds.size.width - 50 - 25) / 2.0).rounded(.down)
        let height = (width * (248.0/151.0)).rounded()
        return CGSize(width: width, height: height)
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let index = selectedIndexes.firstIndex(of: indexPath.row) {
            selectedIndexes.remove(at: index)
        } else {
            if selectedIndexes.count < 6 {
                selectedIndexes.append(indexPath.row)
            }
        }
        collectionView.reloadData()
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TextHeaderCollectionViewCell", for: indexPath) as? TextHeaderCollectionViewCell else {
            fatalError("Could not load TextHeaderCollectionViewCell")
        }
        header.use("Choose up to 6 of the following images:", padding: 40)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let indexPath = IndexPath(row: 0, section: section)
        let headerView = self.collectionView(collectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: indexPath)
        return headerView.systemLayoutSizeFitting(CGSize(width: UIScreen.main.bounds.size.width, height: UIView.layoutFittingExpandedSize.height), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
    }
    
}

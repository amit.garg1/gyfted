//
//  AddItemViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 24/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import PhotosUI

class AddItemViewController: BaseViewController {

    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var exploreLabel: UILabel!
    
    var tapGestureRecognizer: UITapGestureRecognizer!
    
    var wishlist: Wishlist?
    var items = [Item]()
    var type: WishlistType?
    var isFirstItem = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
        
        wishlist = wishlist ?? DataManager.shared.wishlists.first
        updateTitle()
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.enableInterface(true)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigationBarTapped))
        navigationController?.navigationBar.addGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer.cancelsTouchesInView = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.removeGestureRecognizer(tapGestureRecognizer)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? AddItemDetailsViewController {
            if let item = sender as? Item {
                controller.item = item
                controller.isDuplicating = true
                controller.wishlist = wishlist
            } else {
                controller.image = sender as? UIImage
                controller.wishlist = wishlist
            }
            controller.isFirstItem = isFirstItem
        }
        
        if let controller = segue.destination as? AddItemImportViewController {
            controller.wishlist = wishlist
            controller.isFirstItem = isFirstItem
        }
    }
    
    // swiftlint:disable function_body_length
    func reloadData() {
        guard let wishlist = wishlist else { return }
        if wishlist.type == type { return }
        type = wishlist.type
        _ = stackView.arrangedSubviews.map { $0.removeFromSuperview() }
        (stackView.superview as? UIScrollView)?.setContentOffset(.zero, animated: false)
        exploreLabel.text = wishlist.type == .general ? "Explore other gift ideas" : "Explore other \(wishlist.type.keyword.lowercased()) gift ideas"
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "wishlist/suggested", with: ["type": wishlist.typeSetting]), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                if let data = data, let items = try? JSONDecoder().decode([Item].self, from: data) {
                    self.items = items
                    for item in items {
                        
                        let view = UIView()
                        view.widthAnchor.constraint(equalToConstant: 140).isActive = true
                        self.stackView.addArrangedSubview(view)
                        
                        let imageView = UIImageView()
                        imageView.contentMode = .scaleAspectFill
                        imageView.clipsToBounds = true
                        imageView.layer.borderColor = Config.colors.light.cgColor
                        imageView.layer.borderWidth = 1
                        imageView.layer.cornerRadius = 5
                        view.addSubview(imageView)
                        imageView.translatesAutoresizingMaskIntoConstraints = false
                        imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
                        imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
                        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
                        imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
                        
                        let button = UIButton()
                        button.addTarget(self, action: #selector(self.didTapItem), for: .touchUpInside)
                        button.tag = item.id
                        view.addSubview(button)
                        button.translatesAutoresizingMaskIntoConstraints = false
                        button.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
                        button.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
                        button.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
                        button.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
                        
                        imageView.sd_setImage(with: item.imageUrls.first, placeholderImage: nil, options: .retryFailed, completed: nil)
                    }
                    
                    let view = UIView()
                    view.widthAnchor.constraint(equalToConstant: 140).isActive = true
                    self.stackView.addArrangedSubview(view)
                    
                    let imageView = UIImageView()
                    imageView.contentMode = .scaleAspectFill
                    imageView.clipsToBounds = true
                    imageView.layer.borderColor = Config.colors.light.cgColor
                    imageView.layer.borderWidth = 1
                    imageView.layer.cornerRadius = 5
                    imageView.image = UIImage(named: "ViewMore")
                    view.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
                    imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
                    imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
                    imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
                    
                    let button = UIButton()
                    button.addTarget(self, action: #selector(self.presentExplore), for: .touchUpInside)
                    view.addSubview(button)
                    button.translatesAutoresizingMaskIntoConstraints = false
                    button.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
                    button.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
                    button.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
                    button.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
                }
            case .failure(let error):
                NSLog("Error: \(error)")
            }
        }
    }
    
    @IBAction func didTapItem(_ button: UIButton) {
        guard let item = items.filter({ return $0.id == button.tag }).first else { return }
        performSegue(withIdentifier: "PresentAddDetails", sender: item)
    }
    
    // Buttons
    
    @IBAction func presentPhotoOptions(_ sender: Any) {
        resignResponders()
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: NSLocalizedString("Choose Photo", comment: ""), style: .default, handler: { (_) -> Void in
            self.presentPhotoPicker(.photoLibrary)
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Take Photo", comment: ""), style: .default, handler: { (_) -> Void in
            self.presentPhotoPicker(.camera)
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    private func presentPhotoPicker(_ sourceType: UIImagePickerController.SourceType) {
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = sourceType
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func presentExplore(_ sender: Any) {
        if let main = navigationController?.presentingViewController as? MainViewController {
            main.select(tab: .discover)
        }
        dismissView()
    }

    // MARK: Nav Bar Tapping
    
    @objc func navigationBarTapped(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: self.navigationController?.navigationBar)
        let hitView = self.navigationController?.navigationBar.hitTest(location, with: nil)
        guard !(hitView is UIControl) else { return }
        presentWishlistSelector()
    }
    
    @IBAction func presentWishlistSelector() {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for wishlist in DataManager.shared.wishlists {
            controller.addAction(UIAlertAction(title: wishlist.name, style: .default, handler: { (_) in
                self.wishlist = wishlist
                self.updateTitle()
                self.reloadData()
            }))
        }
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    func updateTitle() {
        guard let wishlist = wishlist else { return }
        title = "\(wishlist.name) ▼"
    }

    // MARK: Interface Disabling
    
    func enableInterface(_ isEnabled: Bool) {
        photoButton.isUserInteractionEnabled = isEnabled
    }
    
}

extension AddItemViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[.originalImage] as? UIImage {
            self.performSegue(withIdentifier: "PresentAddDetailsWithoutAnimation", sender: image)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

//
//  AddItemImportViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 09/05/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import WebKit

class AddItemImportViewController: BaseViewController {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    lazy var webView: WKWebView = {
        let webView = WKWebView()
        webView.uiDelegate = self
        webView.navigationDelegate = self
        return webView
    }()
    
    private var observation: NSKeyValueObservation?

    var wishlist: Wishlist?
    var isFirstItem = false
    
    deinit {
        observation = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.layer.borderColor = Config.colors.dark.cgColor
        textView.layer.borderWidth = 1.0
        
        title = NSLocalizedString("Add Item", comment: "").uppercased()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateLinkFromPasteboard), name: UIApplication.willEnterForegroundNotification, object: nil)
        updateLinkFromPasteboard()
        
        observation = webView.observe(\WKWebView.estimatedProgress, options: .new) { [weak self] _, change in
            let progress = Float(change.newValue ?? 0.0) * 0.8
            self?.progressView.setProgress(progress, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.enableInterface(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        webView.stopLoading()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? AddItemImagesViewController {
            controller.product = sender as? PotentialProduct
            controller.wishlist = wishlist
            controller.isFirstItem = isFirstItem
        }
        if let controller = segue.destination as? AddItemDetailsViewController {
            controller.product = sender as? PotentialProduct
            controller.wishlist = wishlist
            controller.isFirstItem = isFirstItem
        }
    }
    
    // MARK: Buttons
    @objc func updateLinkFromPasteboard() {
        if let url = UIPasteboard.general.url {
            textView.text = url.absoluteString
        }
        updateClearButton()
    }
    
    func updateClearButton() {
        clearButton.isHidden = textView.text.lengthOfBytes(using: .utf8) == 0
    }
    
    @IBAction func clearTextView() {
        textView.text = nil
        updateClearButton()
    }
    
    @IBAction func importItemFromLink(_ sender: Any) {
        guard let url = URL(string: textView.text.trimmingCharacters(in: .whitespacesAndNewlines)) else {
            let controller = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("This does not appear to be a valid URL.", comment: ""), preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(controller, animated: true, completion: nil)
            return
        }
        
        enableInterface(false)
        resignResponders()
        
        loadingLabel.text = "Fetching...only take a few seconds"
        progressView.setProgress(0, animated: false)
        let request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 15)
        webView.load(request)
    }
    
    func parseHTML() {
        progressView.setProgress(0.8, animated: true)
        
        fetchTitle { (title) in
            self.progressView.setProgress(0.85, animated: true)
            self.fetchDescription { (description) in
                self.progressView.setProgress(0.9, animated: true)
                self.fetchImages { (images) in
                    self.progressView.setProgress(1.0, animated: true)
                    let product = PotentialProduct(title: title, description: description, url: self.webView.url, images: images)
                    
                    if let url = self.webView.url {
                        var parameters = [String: Any]()
                        parameters["url"] = url.absoluteString
                        parameters["title"] = title
                        parameters["description"] = description
                        parameters["images"] = images
                        if title.isEmpty || description.isEmpty || images.count == 0 {
                            let url = APIClient.url(action: "reportLink")
                            APIClient.shared.performRequest(method: .post, url: url, parameters: parameters, onCompletion: nil)
                        }
                    }

                    if images.count > 1 {
                        self.performSegue(withIdentifier: "PresentImages", sender: product)
                    } else {
                        if title.isEmpty && description.isEmpty && images.count == 0 {
                            let controller = UIAlertController(title: "Automatic Fetch Failed", message: "We were unable to download any information from the link you supplied. We've made a note of this to improve the app in the future.", preferredStyle: .alert)
                            controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                self.performSegue(withIdentifier: "PresentItemDetails", sender: product)
                            }))
                            self.present(controller, animated: true, completion: nil)
                        } else {
                            self.performSegue(withIdentifier: "PresentItemDetails", sender: product)
                        }
                    }
                    
                }
            }
        }
    }
    
    fileprivate func fetchTitle(onCompletion completionHandler: @escaping ((String) -> Void)) {
        webView.evaluateJavaScript("document.querySelectorAll('meta[property=\"og:title\"]')[0].content") { (response, _) in
            guard let string = response as? String, string.isEmpty == false, string != "undefined" else {
                self.webView.evaluateJavaScript("document.querySelectorAll('title')[0].text") { (response, _) in
                    completionHandler(response as? String ?? "")
                }
                return
            }
            completionHandler(string)
        }
    }
    
    fileprivate func fetchDescription(onCompletion completionHandler: @escaping ((String) -> Void)) {
        webView.evaluateJavaScript("document.querySelectorAll('meta[property=\"og:description\"]')[0].content") { (response, _) in
            guard let string = response as? String, string.isEmpty == false, string != "undefined" else {
                self.webView.evaluateJavaScript("document.querySelectorAll('meta[name=\"description\"]')[0].content") { (response, _) in
                    completionHandler(response as? String ?? "")
                }
                return
            }
            completionHandler(string)
        }
    }
    
    fileprivate func fetchImages(onCompletion completionHandler: @escaping (([String]) -> Void)) {
        var images = [String]()
        webView.evaluateJavaScript("document.querySelectorAll('meta[property=\"og:image:secure_url\"]')[0].content") { (response, _) in
            if let string = response as? String, string.isEmpty == false, string != "undefined", string.contains("https") {
                images.append(string)
            }
            self.webView.evaluateJavaScript("document.querySelectorAll('meta[property=\"og:image\"]')[0].content") { (response, _) in
                if let string = response as? String, string.isEmpty == false, string != "undefined", string.contains("https"), images.count == 0 {
                    images.append(string)
                }
                self.webView.evaluateJavaScript("Array.prototype.filter.call(document.images, function(image) { return image.src.substring(0,4) == 'http' && image.width >= 250 && image.height >= 250 && image.complete }).map(function(image) { return image.src })") { (response, _) in
                    if let array = response as? [String] {
                        for image in array {
                            if images.contains(image) == false && image.contains(".svg") == false {
                                images.append(image)
                            }
                        }
                    }
                    completionHandler(images)
                }
            }
        }
    }
    
    // MARK: Interface Disabling
    
    func enableInterface(_ isEnabled: Bool) {
        loadingView.isHidden = isEnabled
        textView.isUserInteractionEnabled = isEnabled
        clearButton.isUserInteractionEnabled = isEnabled
    }
    
}

extension AddItemImportViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        updateClearButton()
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            resignResponders()
            return false
        }
        return true
    }
}

extension AddItemImportViewController: WKUIDelegate, WKNavigationDelegate {
            
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        parseHTML()
    }
}

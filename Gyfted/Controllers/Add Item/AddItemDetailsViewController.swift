//
//  AddItemDetailsViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 24/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import PhotosUI
import GooglePlaces
import SDWebImage
import FirebaseStorage
import WSTagsField
import FirebaseAnalytics
import LUAutocompleteView
import Branch
import Swifter
import Analytics

// swiftlint:disable file_length type_body_length
class AddItemDetailsViewController: BaseViewController {
    
    var item: Item?
    
    var wishlist: Wishlist?
    var product: PotentialProduct?
    var image: UIImage?
    var storeLat: Double?
    var storeLng: Double?
    
    var isDuplicating = false
    var isFirstItem = false
    var isAutoTweeting = false
    var isFromInternalBrowser = false
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var addImageHeaderButton: UIButton!
    @IBOutlet weak var addImageLargeButton: UIButton!
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var sizeField: UITextField!
    @IBOutlet weak var colorField: UITextField!
    @IBOutlet weak var storeNameField: UITextField!
    @IBOutlet weak var storeAddressField: UITextField!
    @IBOutlet weak var urlField: UITextField!
    @IBOutlet weak var keywordsContainer: UIView!
    @IBOutlet weak var keywordsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var wishlistField: UITextField!
    @IBOutlet weak var fauxKeywordTextField: UITextField!
    @IBOutlet weak var autoTweetToggle: UISwitch!
    
    var invitePromptBackgroundView: UIView?
    
    #if !TARGET_IS_EXTENSION
    var invitePromptView: ItemAddedView?
    #endif
    
    lazy var keywordsField: WSTagsField = {
        let field = WSTagsField()
        field.clipsToBounds = false
        field.autocapitalizationType = .allCharacters
        field.layoutMargins = UIEdgeInsets(top: 4, left: 10, bottom: 4, right: 10)
        field.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        field.spaceBetweenLines = 4.0
        field.spaceBetweenTags = 4.0
        field.font = .systemFont(ofSize: 12.0)
        field.backgroundColor = .white
        field.tintColor = Config.colors.tint
        field.textColor = .white
        field.fieldTextColor = Config.colors.tint
        field.selectedColor = .red
        field.selectedTextColor = .white
        field.isDelimiterVisible = true
        field.returnKeyType = .done
        field.acceptTagOption = .comma
        field.placeholder = NSLocalizedString("Use a comma to separate keywords", comment: "")
        field.placeholderColor = UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
        field.onDidChangeHeightTo = { _, height in
            print("HeightTo", height)
            self.keywordsHeightConstraint.constant = max(30, height + 5)
        }
        field.onDidChangeText = { _, text in
            self.fauxKeywordTextField.text = text
            if text == "" {
                self.fauxKeywordTextField.sendActions(for: .editingDidEnd)
            } else {
                self.fauxKeywordTextField.sendActions(for: .editingChanged)
            }
        }
        return field
    }()
    
    lazy var autocompleteView: LUAutocompleteView = {
        let autocompleteView = LUAutocompleteView()
        autocompleteView.dataSource = self
        autocompleteView.delegate = self
        autocompleteView.throttleTime = 0
        autocompleteView.rowHeight = 30
        autocompleteView.maximumHeight = 150
        autocompleteView.autocompleteCell = CustomAutocompleteTableViewCell.self
        for subview in autocompleteView.subviews {
            if let tableView = subview as? UITableView {
                tableView.separatorInset = .zero
                tableView.layer.borderColor = Config.colors.tint.cgColor
                tableView.layer.borderWidth = 2
                tableView.layer.cornerRadius = 8
                tableView.separatorStyle = .none
                tableView.estimatedRowHeight = 30
            }
        }
        return autocompleteView
    }()
    
    var images = [Any]()
    
    // swiftlint:disable function_body_length cyclomatic_complexity
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("Item Details", comment: "").uppercased()
        if item != nil {
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
        }

        wishlistField?.superview?.isHidden = true
        #if TARGET_IS_EXTENSION
            if let navigationController = navigationController {
                if navigationController.viewControllers.first as? LoadingViewController != nil {
                    var viewControllers = navigationController.viewControllers
                    viewControllers.remove(at: 0)
                    navigationController.viewControllers = viewControllers
                }
                
                if navigationController.viewControllers.count == 1 {
                    navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
                }
            }
            wishlistField?.superview?.isHidden = false
        #endif
        
        if isFromInternalBrowser {
            wishlistField?.superview?.isHidden = false
        }
        
        DataManager.shared.downloadCachedKeywords()
        
        fields = [nameField, priceField, descriptionField, sizeField, colorField, storeNameField, storeAddressField, urlField]
        _ = fields.map {
            $0.addDismissToolbar()
            $0.delegate = self
        }
        prefillForm()
        setupKeyboardNotifications()
        
        imagesCollectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        let imageDragGesture = UILongPressGestureRecognizer(target: self, action: #selector(imageLongPressDetected))
        imagesCollectionView.addGestureRecognizer(imageDragGesture)
        
        updateImageInterface()
        
        if item == nil {
            for field in fields where (field.text?.isEmpty ?? true) {
                field.becomeFirstResponder()
                break
            }
        }
        
        keywordsField.frame = keywordsContainer.bounds
        keywordsContainer.addSubview(keywordsField)
        keywordsField.textDelegate = self
        
        fauxKeywordTextField.superview?.addSubview(autocompleteView)
        autocompleteView.textField = fauxKeywordTextField
        
        if item != nil && isDuplicating == false {
            addButton.setTitle(NSLocalizedString("Update Item", comment: "").uppercased(), for: .normal)
        }
        
        autoTweetToggle?.superview?.isHidden = true
        #if TARGET_IS_EXTENSION
            if Config.isTwitterEnabled {
                autoTweetToggle?.superview?.isHidden = false
            }
        #else
            autoTweetToggle?.superview?.isHidden = false
        #endif
        if item != nil && isDuplicating == false {
            autoTweetToggle?.superview?.isHidden = true
        }
        
        if (autoTweetToggle?.superview?.isHidden ?? true) == false {
            isAutoTweeting = Config.sharedDefaults().bool(forKey: Config.userDefaults.autoTweetEnabled)
            if Config.isTwitterEnabled == false {
                isAutoTweeting = false
                Config.sharedDefaults().set(false, forKey: Config.userDefaults.autoTweetEnabled)
            }
            updateAutoTweetField()
        }
    }
    
    @objc func updateImageInterface() {
        addImageHeaderButton.isHidden = images.count == 0
        addImageLargeButton.isHidden = !addImageHeaderButton.isHidden
    }
    
    func updateAutoTweetField() {
        autoTweetToggle.isOn = isAutoTweeting
    }
    
    func prefillForm() {
        if let product = product {
            images = product.imageUrls
            nameField.text = product.title
            descriptionField.text = product.description
            urlField.text = product.url?.absoluteString
            storeNameField.superview?.isHidden = true
            storeAddressField.superview?.isHidden = true
        }
        if let image = image {
            images.append(image)
            urlField.superview?.isHidden = true
        }
        if let item = item {
            images = item.imageUrls
            nameField.text = item.name
            priceField.text = item.price
            descriptionField.text = item.text
            sizeField.text = item.size
            colorField.text = item.color
            storeNameField.text = item.storeName
            storeLat = item.storeLat
            storeLng = item.storeLng
            storeAddressField.text = item.storeAddress
            urlField.text = item.url
            if item.url.isEmpty == false {
                storeNameField.superview?.isHidden = true
                storeAddressField.superview?.isHidden = true
            } else {
                urlField.superview?.isHidden = true
            }
            keywordsField.addTags(item.keywords)
        }
        
        #if TARGET_IS_EXTENSION
            wishlistField.text = wishlist?.name
        #endif
        if isFromInternalBrowser {
            wishlistField.text = wishlist?.name
        }
        
        updateAutoTweetField()
        
        priceField.placeholder = Locale.current.currencySymbol
    }
    
    @IBAction func addItem(_ sender: Any) {
        resignResponders()
        
        if images.count == 0 {
            present(NSLocalizedString("Your item must have at least one image.", comment: ""))
            return
        }
        
        var requiredFields: [UITextField] = [nameField, priceField]
        if urlField.superview?.isHidden == false {
            requiredFields += [urlField]
        }
        if storeNameField.superview?.isHidden == false {
            requiredFields += [storeNameField]
        }
        
        var showError = false
        for field in requiredFields where field.text?.isEmpty ?? true {
            updateHighlight(for: field, color: .red)
            showError = true
        }
        if showError {
            present(NSLocalizedString("Please fill out all required fields.", comment: ""))
            return
        }

        fetchImages()
    }
    
    private func fetchImages() {
        addButton.isHidden = true
        _ = fields.map { $0.isUserInteractionEnabled = false }
        
        for index in 0..<images.count {
            if let url = images[index] as? URL {
                SDWebImageManager.shared.loadImage(with: url, options: .retryFailed, progress: nil) { (image, _, _, _, _, _) in
                    if let image = image {
                        self.images[index] = image
                    } else {
                        self.images.remove(at: index)
                    }
                    self.fetchImages()
                }
                return
            }
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.uploadImages()
        }
    }
    
    private func uploadImages() {
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        var urls = [URL?]()
        var completed = 0
        
        if images.count == 0 {
            DispatchQueue.main.async {
                self.saveItem(with: [])
            }
            return
        }
        
        for index in 0..<images.count {
            if let url = images[index] as? URL {
                urls.append(url)
                completed += 1
                if completed == self.images.count {
                    DispatchQueue.main.async {
                        self.saveItem(with: urls.compactMap({$0}))
                    }
                }
            } else {
                urls.append(nil)
                guard let image = images[index] as? UIImage, let resizedImage = resize(image, maxDimension: 1000), let data = resizedImage.jpegData(compressionQuality: 0.7) else {
                    continue
                }
                let filename = Profile.userId + "-" + NSUUID().uuidString + ".jpg"
                let imageRef = storageRef.child("images/" + filename)
                _ = imageRef.putData(data, metadata: nil) { (_, error) in
                    if let error = error {
                        print("UPLOAD ERROR: \(error)")
                    }
                    imageRef.downloadURL(completion: { (url, error) in
                        if let error = error {
                            print("DOWNLOAD ERROR: \(error)")
                        }
                        urls[index] = url
                        completed += 1
                        if completed == self.images.count {
                            DispatchQueue.main.async {
                                self.saveItem(with: urls.compactMap({$0}))
                            }
                        }
                    })
                }
            }
        }
    }
    
    private func saveItem(with imageUrls: [URL]) {
        guard let wishlist = wishlist else { return }
        
        if imageUrls.count == 0 {
            present(NSLocalizedString("An error occurred whilst uploading your images. Please try again ensuring you have a good network connection.", comment: ""))
            addButton.isHidden = false
            _ = fields.map { $0.isUserInteractionEnabled = true }
            return
        }
        
        var data = [String: Any]()
        if let item = item, isDuplicating == false {
            data["id"] = item.id
        }
        data["name"] = nameField.text ?? ""
        data["price"] = priceField.text ?? ""
        data["text"] = descriptionField.text ?? ""
        data["size"] = sizeField.text ?? ""
        data["color"] = colorField.text ?? ""
        data["url"] = urlField.text ?? ""
        data["storeName"] = storeNameField.text ?? ""
        data["storeAddress"] = storeAddressField.text ?? ""
        if let lat = storeLat, let lng = storeLng {
            data["storeLat"] = lat
            data["storeLng"] = lng
        }
        data["imageUrlsString"] = imageUrls.map({ $0.absoluteString }).joined(separator: ",")
        data["wishlistId"] = wishlist.id
        data["keywordString"] = ""
        if keywordsField.tags.count > 0 {
            data["keywordString"] = "," + keywordsField.tags.map({$0.text.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()}).joined(separator: ",") + ","
        }
        
        if item == nil || isDuplicating {
            data["autoShared"] = isAutoTweeting && Config.isTwitterEnabled ? 1 : 0
        }

        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "item/save"), parameters: data) { (handler) in
            
            var id = self.item?.id ?? 0
            
            switch handler {
            case .success(let data):
                if let data = data, let dictionary = try? JSONSerialization.jsonObject(with: data, options: .init(rawValue: 0)) as? NSDictionary, let itemId = dictionary["id"] as? Int {
                    id = itemId
                    
                    if self.item == nil || self.isDuplicating {
                        var properties = [String: String]()
                        properties["id"] = "\(itemId)"
                        properties["name"] = self.nameField.text ?? ""
                        properties["price"] = self.priceField.text ?? ""
                        properties["text"] = self.descriptionField.text ?? ""
                        properties["size"] = self.sizeField.text ?? ""
                        properties["color"] = self.colorField.text ?? ""
                        properties["url"] = self.urlField.text ?? ""
                        properties["storeName"] = self.storeNameField.text ?? ""
                        properties["storeAddress"] = self.storeAddressField.text ?? ""
                        if let lat = self.storeLat, let lng = self.storeLng {
                            properties["storeLat"] = "\(lat)"
                            properties["storeLng"] = "\(lng)"
                        }
                        properties["wishlistId"] = "\(self.wishlist?.id ?? 0)"
                        properties["keywords"] = ""
                        if self.keywordsField.tags.count > 0 {
                            properties["keywordString"] = self.keywordsField.tags.map({$0.text.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()}).joined(separator: ",")
                        }
                        properties["autoShared"] = self.isAutoTweeting && Config.isTwitterEnabled ? "true" : "false"
                        SEGAnalytics.shared()?.track("Item Added", properties: properties)
                    }
                }
            case .failure(let error):
                NSLog("ERROR: \(error)")
            }
            
            self.sendAutoTweet(itemId: id, imageUrl: imageUrls.first) {
                self.transactionComplete(handler, imageUrls: imageUrls)
            }
        }
    }
    
    private func transactionComplete(_ handler: Result<Data?, APIError>, imageUrls: [URL]) {
        switch handler {
        case .success:
            DataManager.shared.cacheWishlists {
                if let item = self.item, self.isDuplicating == false {
                    APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "item/\(item.id)"), parameters: nil, onCompletion: { (handler) in
                        #if !TARGET_IS_EXTENSION
                            switch handler {
                            case .success(let data):
                                if let data = data, let item = try? JSONDecoder().decode(Item.self, from: data) {
                                    NSLog("ANNOUNCING ITEM")
                                    NotificationCenter.default.post(name: .itemWasUpdated, object: nil, userInfo: ["item": item])
                                }
                            case .failure:
                                NSLog("FAILED TO FIND ITEM")
                            }
                        #endif
                        self.dismissView()
                    })
                } else {
                    #if !TARGET_IS_EXTENSION
                        if let controller = self.navigationController?.presentingViewController as? MainViewController {
                            controller.select(tab: .wishlists)
                        }
                        Analytics.logEvent("add_item", parameters: [
                            "name": (self.nameField.text ?? "") as NSObject,
                            "user_id": Profile.userId as NSObject,
                            "wishlist_id": "\(self.wishlist?.id ?? 0)" as NSObject
                        ])
                    
                        if self.isFirstItem {
                            (UIApplication.shared.delegate as? AppDelegate)?.deepLinkLocation = .inviteFriends
                            self.dismissView()
                        } else {
                            self.presentInvitePrompt(with: self.nameField.text ?? "", imageURL: imageUrls.first)
                        }
                    #else
                        self.dismissView()
                    #endif
                }
            }
        case .failure(let error):
            self.present(NSLocalizedString("An error occurred whilst saving your item. Please try again ensuring you have a good network connection.", comment: ""))
            NSLog("ERROR: \(error)")
            addButton.isHidden = false
            _ = fields.map { $0.isUserInteractionEnabled = true }
            return
        }
    }
    
    private func updateHighlight(for field: UITextField, color: UIColor) {
        guard let subviews = field.superview?.subviews else { return }
        for subview in subviews where subview is UILabel {
            (subview as? UILabel)?.textColor = color
        }
    }
    
    private func resize(_ image: UIImage, maxDimension: CGFloat) -> UIImage? {
        let rect = AVMakeRect(aspectRatio: image.size, insideRect: CGRect(x: 0, y: 0, width: maxDimension, height: maxDimension))
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 1.0)
        image.draw(in: CGRect(origin: .zero, size: rect.size))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage
    }
    
    func presentInvitePrompt(with name: String, imageURL: URL?) {
        #if !TARGET_IS_EXTENSION
        guard let navigationController = navigationController else { return }
        
        invitePromptBackgroundView = UIView(frame: UIScreen.main.bounds)
        invitePromptBackgroundView?.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        invitePromptBackgroundView?.alpha = 0
        if let invitePromptBackgroundView = invitePromptBackgroundView {
            navigationController.view.addSubview(invitePromptBackgroundView)
            invitePromptBackgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissView)))
            UIView.animate(withDuration: 0.2) {
                invitePromptBackgroundView.alpha = 1
            }
        }
        
        invitePromptView = ItemAddedView.instance()
        if let invitePromptView = invitePromptView {
            invitePromptView.layer.cornerRadius = 12
            invitePromptView.delegate = self
            invitePromptView.use(name, imageURL: imageURL)
            invitePromptView.translatesAutoresizingMaskIntoConstraints = false
            navigationController.view.addSubview(invitePromptView)
            
            invitePromptView.leadingAnchor.constraint(equalTo: navigationController.view.leadingAnchor, constant: 20).isActive = true
            invitePromptView.trailingAnchor.constraint(equalTo: navigationController.view.trailingAnchor, constant: -20).isActive = true
            
            let centerConstraint = invitePromptView.centerYAnchor.constraint(equalTo: navigationController.view.centerYAnchor)
            let bottomConstraint = invitePromptView.topAnchor.constraint(equalTo: navigationController.view.bottomAnchor, constant: 0)
            bottomConstraint.isActive = true
            navigationController.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .init(rawValue: 0), animations: {
                bottomConstraint.isActive = false
                centerConstraint.isActive = true
                navigationController.view.layoutIfNeeded()
            }, completion: nil)
        }
        #endif
    }
    
    func sendAutoTweet(itemId: Int, imageUrl: URL?, onCompletion completionHandler: @escaping () -> Void) {
        
        if isAutoTweeting == false || itemId == 0 {
            completionHandler()
            return
        }
        
        guard let token = Config.sharedDefaults().string(forKey: Config.userDefaults.twitterToken), let secret = Config.sharedDefaults().string(forKey: Config.userDefaults.twitterSecret) else {
            completionHandler()
            return
        }
        
        #if TARGET_IS_EXTENSION
            let branch: Branch = Branch.getInstance()
            branch.initSession(launchOptions: nil)
        #endif
        
        let text = "Check out what I just added to my wishlist on @Gyftedapp! Feel free to spend some $$ & buy this for me :)"
        
        let buo = BranchUniversalObject(canonicalIdentifier: "item/\(itemId)")
        buo.title = nameField.text ?? ""
        buo.contentDescription = text
        buo.contentMetadata.customMetadata["item"] = itemId
        buo.publiclyIndex = true
        buo.imageUrl = imageUrl?.absoluteString
        buo.canonicalIdentifier = "item-\(itemId)"
        
        let lp = BranchLinkProperties()
        buo.getShortUrl(with: lp) { (url, _) in
            DispatchQueue.main.async {
                guard let url = url else {
                    completionHandler()
                    return
                }
                let swifter = Swifter(consumerKey: Config.credentials.twitterConsumerKey, consumerSecret: Config.credentials.twitterConsumerSecret, oauthToken: token, oauthTokenSecret: secret)
                swifter.postTweet(status: "\(text) \(url)")
                completionHandler()
            }
        }
    }
}

extension AddItemDetailsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: NSLocalizedString("Remove Image", comment: ""), style: .destructive, handler: { (_) in
            self.images.remove(at: indexPath.row)
            self.imagesCollectionView.deleteItems(at: [indexPath])
            self.updateImageInterface()
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
}

extension AddItemDetailsViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // swiftlint:disable force_cast
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        if let image = images[indexPath.row] as? UIImage {
            cell.use(image)
        }
        if let url = images[indexPath.row] as? URL {
            cell.use(url)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        images.swapAt(sourceIndexPath.row, destinationIndexPath.row)
    }
    
    @IBAction func imageLongPressDetected(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            guard let selectedIndexPath = imagesCollectionView.indexPathForItem(at: gesture.location(in: imagesCollectionView)) else {
                break
            }
            imagesCollectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            imagesCollectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            imagesCollectionView.endInteractiveMovement()
        default:
            imagesCollectionView.cancelInteractiveMovement()
        }
    }
    
    @IBAction func autoTweetToggleDidChange() {
        if Config.isTwitterEnabled {
            isAutoTweeting = !isAutoTweeting
            Config.sharedDefaults().set(isAutoTweeting, forKey: Config.userDefaults.autoTweetEnabled)
            updateAutoTweetField()
        } else {
            autoTweetToggle.setOn(false, animated: true)
            #if !TARGET_IS_EXTENSION
                guard let callbackUrl = URL(string: "gyfted://") else { return }
                let swifter = Swifter(consumerKey: Config.credentials.twitterConsumerKey, consumerSecret: Config.credentials.twitterConsumerSecret)
                swifter.authorize(withCallback: callbackUrl, presentingFrom: self, success: { (token, _) in
                    guard let token = token else { return }
                    DispatchQueue.main.async {
                        Config.sharedDefaults().set(token.key, forKey: Config.userDefaults.twitterToken)
                        Config.sharedDefaults().set(token.secret, forKey: Config.userDefaults.twitterSecret)
                        Config.sharedDefaults().set(true, forKey: Config.userDefaults.autoTweetEnabled)
                        Config.sharedDefaults().synchronize()
                        self.isAutoTweeting = true
                        self.updateAutoTweetField()
                    }
                })
            #endif
        }
    }
}

extension AddItemDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBAction func presentPhotoOptions(_ sender: Any) {
        resignResponders()
        
        #if TARGET_IS_EXTENSION
            presentPhotoPicker(.photoLibrary)
        #else
            let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            controller.addAction(UIAlertAction(title: NSLocalizedString("Choose Photo", comment: ""), style: .default, handler: { (_) -> Void in
                self.presentPhotoPicker(.photoLibrary)
            }))
            controller.addAction(UIAlertAction(title: NSLocalizedString("Take Photo", comment: ""), style: .default, handler: { (_) -> Void in
                self.presentPhotoPicker(.camera)
            }))
            controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            present(controller, animated: true, completion: nil)
        #endif
    }
    
    private func presentPhotoPicker(_ sourceType: UIImagePickerController.SourceType) {
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = sourceType
        present(controller, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[.originalImage] as? UIImage {
            images.append(image)
            let indexPath = IndexPath(row: images.count - 1, section: 0)
            imagesCollectionView.insertItems(at: [indexPath])
            imagesCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
            updateImageInterface()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension AddItemDetailsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case priceField:
            if textField.text?.isEmpty ?? true {
                textField.text = (textField.placeholder ?? "") + string
                return false
            }
        default:
            break
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        updateHighlight(for: textField, color: Config.colors.dark)
        
        switch textField {
        case priceField:
            if textField.text?.isEmpty ?? true {
                textField.text = textField.placeholder
            }
        case storeAddressField:
            let controller = GMSAutocompleteViewController()
            controller.delegate = self
            let filter = GMSAutocompleteFilter()
            filter.type = .establishment
            controller.autocompleteFilter = filter
            present(controller, animated: true, completion: nil)
            return false
        case wishlistField:
            let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            for wishlist in DataManager.shared.wishlists {
                controller.addAction(UIAlertAction(title: wishlist.name, style: .default, handler: { (_) in
                    self.wishlist = wishlist
                    self.wishlistField.text = wishlist.name
                }))
            }
            controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            present(controller, animated: true, completion: nil)
            return false
        default:
            break
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case priceField:
            if (textField.text?.count ?? 0) == 1 {
                textField.text = nil
            }
        default:
            break
        }
    }
}

extension AddItemDetailsViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        storeAddressField.text = place.formattedAddress
        if storeNameField.text?.isEmpty ?? true {
            storeNameField.text = place.name
        }
        storeLat = place.coordinate.latitude
        storeLng = place.coordinate.longitude
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        NSLog("ERROR: \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}

extension AddItemDetailsViewController: LUAutocompleteViewDataSource {
    func autocompleteView(_ autocompleteView: LUAutocompleteView, elementsFor text: String, completion: @escaping ([String]) -> Void) {
        completion(DataManager.shared.fetchKeywords(for: text))
    }
}

extension AddItemDetailsViewController: LUAutocompleteViewDelegate {
    func autocompleteView(_ autocompleteView: LUAutocompleteView, didSelect text: String) {
        keywordsField.addTag(text)
    }
}

#if !TARGET_IS_EXTENSION
extension AddItemDetailsViewController: ItemAddedViewDelegate {
    func itemAddedViewPresentShare() {
        guard let wishlist = wishlist else { return }
        let text = "Check out my wishlist on @Gyftedapp! Start here if you’re going to buy me gifts anytime soon :)"
        
        let buo = BranchUniversalObject(canonicalIdentifier: "wishlist/\(wishlist.id)")
        buo.title = wishlist.name
        buo.contentDescription = text
        buo.contentMetadata.customMetadata["wishlist"] = wishlist.id
        buo.publiclyIndex = true
        buo.imageUrl = wishlist.imageUrl?.absoluteString
        buo.canonicalIdentifier = "wishlist-\(wishlist.id)"
        
        let lp = BranchLinkProperties()
        buo.getShortUrl(with: lp) { (url, _) in
            DispatchQueue.main.async {
                guard let url = url else { return }
                if let url = URL(string: url) {
                    var items = [Any]()
                    items.append(text)
                    items.append(url)
                    let controller = UIActivityViewController(activityItems: items, applicationActivities: nil)
                    self.present(controller, animated: true, completion: nil)
                    
                    Analytics.logEvent(AnalyticsEventShare, parameters: [
                        AnalyticsParameterContentType: "wishlist",
                        AnalyticsParameterItemID: "\(wishlist.id)",
                        AnalyticsParameterItemName: "\(wishlist.name)"
                        ])
                    
                    SEGAnalytics.shared()?.track("Shared Wishlist", properties: ["id": "\(wishlist.id)"])
                    /*
                    controller.completionWithItemsHandler = { (_, _, _, _) in
                        self.dismissView()
                    }
                    */
                }
            }
        }
    }
    
    func itemAddedViewDismiss() {
        dismissView()
    }
}
#endif

//
//  AddItemBrowserViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 13/05/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit
import WebKit
import PercentEncoder

class AddItemBrowserViewController: BaseViewController {

    @IBOutlet weak var urlField: UITextField!
    @IBOutlet weak var loadingBar: UIView!
    @IBOutlet weak var loadingBarWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    
    private var loadingObservation: NSKeyValueObservation?
    private var backObservation: NSKeyValueObservation?
    private var forwardObservation: NSKeyValueObservation?
    
    var shop: Shop?
    
    deinit {
        loadingObservation = nil
        backObservation = nil
        forwardObservation = nil
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? AddItemImagesViewController {
            controller.product = sender as? PotentialProduct
            controller.wishlist = DataManager.shared.wishlists.first
            controller.isFirstItem = false
            controller.isFromInternalBrowser = true
        }
        if let controller = segue.destination as? AddItemDetailsViewController {
            controller.product = sender as? PotentialProduct
            controller.wishlist = DataManager.shared.wishlists.first
            controller.isFirstItem = false
            controller.isFromInternalBrowser = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingBarWidthConstraint.constant = 0
        
        urlField.tintColor = Config.colors.tint
        webView.navigationDelegate = self
        
        loadingObservation = webView.observe(\WKWebView.estimatedProgress, options: .new) { [weak self] _, change in
            let progress = CGFloat(change.newValue ?? 0.0)
            let width = (self?.loadingBar.superview?.bounds.size.width ?? 0) * progress
            self?.loadingBar.alpha = 1
            UIView.animate(withDuration: 0.2, animations: {
                self?.loadingBarWidthConstraint.constant = width
            }) { (completed) in
                guard completed else { return }
                if progress == 1 {
                    UIView.animate(withDuration: 0.2, delay: 0.5, options: .curveLinear, animations: {
                        self?.loadingBar.alpha = 0
                    }) { (_) in
                        self?.loadingBarWidthConstraint.constant = 0
                        self?.loadingBar.alpha = 1
                    }
                }
            }
        }
        
        backObservation = webView.observe(\WKWebView.canGoBack, options: .new) { [weak self] _, _ in
            self?.backButton.tintColor = (self?.webView.canGoBack ?? false) ? Config.colors.tint : Config.colors.light
        }
        
        forwardObservation = webView.observe(\WKWebView.canGoForward, options: .new) { [weak self] _, _ in
            self?.forwardButton.tintColor = (self?.webView.canGoForward ?? false) ? Config.colors.tint : Config.colors.light
        }
        
        if let shop = shop {
            urlField.text = shop.url
            if let url = URL(string: shop.url) {
                webView.load(URLRequest(url: url))
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func goBack(_ sender: Any) {
        guard webView.canGoBack else { return }
        webView.goBack()
    }
    
    @IBAction func goForward(_ sender: Any) {
        guard webView.canGoForward else { return }
        webView.goForward()
    }
    
    @IBAction func presentShare(_ sender: Any) {
        guard let url = webView.url else { return }
        let controller = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        present(controller, animated: true, completion: nil)
    }
}

extension AddItemBrowserViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        guard let text = textField.text, text.isEmpty == false else { return true }
        if let url = URL(string: text) {
            if text.contains("http") == false {
                if let url = URL(string: "https://" + text) {
                    webView.load(URLRequest(url: url))
                }
            } else {
                webView.load(URLRequest(url: url))
            }
        } else if let url = URL(string: "https://www.google.com/search?q=" + PercentEncoding.encodeURIComponent.evaluate(string: text)) {
            webView.load(URLRequest(url: url))
        }
        return true
    }
}

extension AddItemBrowserViewController {
    @IBAction func addItem(_ sender: Any) {
        parseHTML()
    }
    
    func parseHTML() {
        fetchTitle { (title) in
            self.fetchDescription { (description) in
                self.fetchImages { (images) in
                    let product = PotentialProduct(title: title, description: description, url: self.webView.url, images: images)
                    
                    if let url = self.webView.url {
                        var parameters = [String: Any]()
                        parameters["url"] = url.absoluteString
                        parameters["title"] = title
                        parameters["description"] = description
                        parameters["images"] = images
                        if title.isEmpty || description.isEmpty || images.count == 0 {
                            let url = APIClient.url(action: "reportLink")
                            APIClient.shared.performRequest(method: .post, url: url, parameters: parameters, onCompletion: nil)
                        }
                    }

                    if images.count > 1 {
                        self.performSegue(withIdentifier: "PresentImages", sender: product)
                    } else {
                        if title.isEmpty && description.isEmpty && images.count == 0 {
                            let controller = UIAlertController(title: "Automatic Fetch Failed", message: "We were unable to download any information from this website. We've made a note of this to improve the app in the future.", preferredStyle: .alert)
                            controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                self.performSegue(withIdentifier: "PresentItemDetails", sender: product)
                            }))
                            self.present(controller, animated: true, completion: nil)
                        } else {
                            self.performSegue(withIdentifier: "PresentItemDetails", sender: product)
                        }
                    }
                    
                }
            }
        }
    }
    
    fileprivate func fetchTitle(onCompletion completionHandler: @escaping ((String) -> Void)) {
        webView.evaluateJavaScript("document.querySelectorAll('meta[property=\"og:title\"]')[0].content") { (response, _) in
            guard let string = response as? String, string.isEmpty == false, string != "undefined" else {
                self.webView.evaluateJavaScript("document.querySelectorAll('title')[0].text") { (response, _) in
                    completionHandler(response as? String ?? "")
                }
                return
            }
            completionHandler(string)
        }
    }
    
    fileprivate func fetchDescription(onCompletion completionHandler: @escaping ((String) -> Void)) {
        webView.evaluateJavaScript("document.querySelectorAll('meta[property=\"og:description\"]')[0].content") { (response, _) in
            guard let string = response as? String, string.isEmpty == false, string != "undefined" else {
                self.webView.evaluateJavaScript("document.querySelectorAll('meta[name=\"description\"]')[0].content") { (response, _) in
                    completionHandler(response as? String ?? "")
                }
                return
            }
            completionHandler(string)
        }
    }
    
    fileprivate func fetchImages(onCompletion completionHandler: @escaping (([String]) -> Void)) {
        var images = [String]()
        webView.evaluateJavaScript("document.querySelectorAll('meta[property=\"og:image:secure_url\"]')[0].content") { (response, _) in
            if let string = response as? String, string.isEmpty == false, string != "undefined", string.contains("https") {
                images.append(string)
            }
            self.webView.evaluateJavaScript("document.querySelectorAll('meta[property=\"og:image\"]')[0].content") { (response, _) in
                if let string = response as? String, string.isEmpty == false, string != "undefined", string.contains("https"), images.count == 0 {
                    images.append(string)
                }
                self.webView.evaluateJavaScript("Array.prototype.filter.call(document.images, function(image) { return image.src.substring(0,4) == 'http' && image.width >= 250 && image.height >= 250 && image.complete }).map(function(image) { return image.src })") { (response, _) in
                    if let array = response as? [String] {
                        for image in array {
                            if images.contains(image) == false && image.contains(".svg") == false {
                                images.append(image)
                            }
                        }
                    }
                    completionHandler(images)
                }
            }
        }
    }
}

extension AddItemBrowserViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        guard let url = webView.url else { return }
        urlField.text = url.absoluteString
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error)
    }
    
}

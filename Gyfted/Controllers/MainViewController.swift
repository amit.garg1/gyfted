//
//  MainViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 02/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import SafariServices
import OneSignal
import Analytics

extension Notification.Name {
    static let authenticatedUserDidChange = Notification.Name(rawValue: "AuthenticatedUserDidChange")
    static let cachedWishlistsDidChange = Notification.Name(rawValue: "CachedWishlistsDidChange")
    static let itemPurchaseStatusDidChange = Notification.Name(rawValue: "ItemPurchaseStatusDidChange")
    static let notificationsDidChange = Notification.Name(rawValue: "NotificationsDidChange")
}

enum ReengagementNotification: Int, CaseIterable {
    case day1 = 1
    case day3 = 3
    
    var title: String {
        switch self {
        case .day1:
            return NSLocalizedString("Create your wishlists!", comment: "")
        case .day3:
            return NSLocalizedString("Only get gifts you’ll love!", comment: "")
        }
    }
    
    var message: String {
        switch self {
        case .day1:
            return NSLocalizedString("Are your wishlists up to date? Hop back into the app & start adding away..", comment: "")
        case .day3:
            return NSLocalizedString("Invite your friends and family to follow your wishlist. You’ll be happy you did.", comment: "")
        }
    }
}

// swiftlint:disable file_length type_body_length
class MainViewController: BaseViewController {
    
    enum Tab: Int {
        case home
        case discover
        case add
        case friends
        case wishlists
        
        var name: String {
            switch self {
            case .home:
                return "Home"
            case .discover:
                return "Discover"
            case .add:
                return "Add"
            case .friends:
                return "Friends"
            case .wishlists:
                return "Wishlists"
            }
        }
    }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var indicatorLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabBarStackView: UIStackView!
    @IBOutlet weak var dimmedOverlay: UIView!
    @IBOutlet weak var actionSheetButtonOne: UIButton!
    @IBOutlet weak var actionSheetButtonOneBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var actionSheetButtonTwo: UIButton!
    @IBOutlet weak var actionSheetButtonTwoBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var invitePromptCenterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var referButton: UIButton!
    @IBOutlet weak var laterButton: UIButton!
    
    // swiftlint:disable comma
    private var controllers: [UIViewController?] = [nil, nil, nil ,nil, nil]
    var selectedTab = Tab.home
    var previousTab = Tab.home
    
    var shadowView: UIView?
    var overlay: UIView?
    var overlayConstraint: NSLayoutConstraint?
    var presentedFromOnboarding = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        actionSheetButtonOneBottomConstraint.constant = -50
        actionSheetButtonTwoBottomConstraint.constant = -50
        
        select(tab: .home)
        
        updateBadge(UIApplication.shared.applicationIconBadgeNumber)
        updateCaches()
        NotificationCenter.default.addObserver(self, selector: #selector(updateCaches), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBadgeFromNotification), name: .notificationsDidChange, object: nil)
        
        invitePromptCenterConstraint.constant = UIScreen.main.bounds.size.height
        
        if let app = UIApplication.shared.delegate as? AppDelegate, app.deepLinkLocation == nil {
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (_) in
                DispatchQueue.main.async {
                    self.presentInvitePrompt()
                }
            }
        }
        
        if let analytics = SEGAnalytics.shared(), let data = Config.sharedDefaults().object(forKey: Config.userDefaults.profile) as? Data, let profile = try? JSONDecoder().decode(Profile.self, from: data) {
            analytics.identify(profile.id, traits: profile.analyticsTraits)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        OneSignal.promptForPushNotifications { (approved) in
            if approved {
                OneSignal.sendTag("user", value: Profile.userId)

                if UserDefaults.standard.bool(forKey: Config.userDefaults.engagementNotificationsScheduled) == false {
                    for notification in ReengagementNotification.allCases {
                        let content = UNMutableNotificationContent()
                        content.title = notification.title
                        content.body = notification.message
                        let date = Date().adjust(.day, offset: notification.rawValue)
                        let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
                        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
                        let request = UNNotificationRequest(identifier: "Reengagement-\(NSUUID().uuidString)", content: content, trigger: trigger)
                        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                    }
                    UserDefaults.standard.set(true, forKey: Config.userDefaults.engagementNotificationsScheduled)
                }
            }
        }
        
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { (_) in
            self.performDeepLinkRouting()
        }
        
    }
    
    @objc func updateCaches() {
        DataManager.shared.cacheWishlists {
        }
        DataManager.shared.cacheProfile {
        }
        DataManager.shared.cacheNotifications {
        }
    }
    
    @objc func updateBadgeFromNotification() {
        updateBadge()
    }
    
    func updateBadge(_ number: Int? = nil) {
        let count = number ?? DataManager.shared.unreadNotificationCount
        UIApplication.shared.applicationIconBadgeNumber = count
    }
    
    override func presentShare() {
        performSegue(withIdentifier: "PresentShare", sender: nil)
    }
    
    // MARK: Tab Selection
    
    @IBAction func tabSelected(_ sender: UITapGestureRecognizer) {
        guard let tag = sender.view?.tag, let tab = Tab(rawValue: tag) else { return }
        select(tab: tab)
    }
    
    func select(tab: Tab) {
        
        var tab = tab
        if tab == .add && selectedTab == .add {
            tab = previousTab
        }
        
        switch tab {
        case .home,.discover,.friends,.wishlists:
            if controllers[tab.rawValue] == nil {
                controllers[tab.rawValue] = UIStoryboard(name: tab.name, bundle: nil).instantiateInitialViewController()
            }
            controllers[selectedTab.rawValue]?.remove()
            
            if let controller = controllers[tab.rawValue] {
                add(controller, to: containerView)
                if tab == selectedTab, let navigationController = controller as? UINavigationController {
                    Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { (_) in
                        _ = navigationController.popToRootViewController(animated: true)
                    }
                }
            }
            dismissActionSheet()
        case .add:
            actionSheetButtonOne.setTitle(NSLocalizedString("Create New Wishlist", comment: "").uppercased(), for: .normal)
            _ = actionSheetButtonOne.allTargets.map { actionSheetButtonOne.removeTarget($0, action: nil, for: .touchUpInside) }
            actionSheetButtonOne.addTarget(self, action: #selector(presentCreateWishlistFromActionSheet), for: .touchUpInside)
            actionSheetButtonTwo.setTitle(NSLocalizedString("Add New Item", comment: "").uppercased(), for: .normal)
            _ = actionSheetButtonTwo.allTargets.map { actionSheetButtonTwo.removeTarget($0, action: nil, for: .touchUpInside) }
            actionSheetButtonTwo.addTarget(self, action: #selector(presentAddNewItemFromActionSheet), for: .touchUpInside)
            presentActionSheet()
        }

        previousTab = selectedTab
        selectedTab = tab
        updateTabIndicator()
        updateIcons()
    }
    
    // MARK: Tab Indicator
    
    private func updateTabIndicator() {
        let width = UIScreen.main.bounds.size.width / 5.0
        let x = CGFloat(selectedTab.rawValue) * width
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveLinear, animations: {
            self.indicatorLeadingConstraint.constant = x
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK: Icons
    
    private func updateIcons() {
        for view in tabBarStackView.arrangedSubviews {
            guard let imageView = view as? UIImageView, let tab = Tab(rawValue: imageView.tag) else { return }
            imageView.image = UIImage(named: "TabBar" + tab.name + (tab == selectedTab ? "Selected" : ""))
            if tab == .add {
                imageView.image = UIImage(named: "TabBarAdd")
                UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveLinear, animations: {
                    imageView.transform = CGAffineTransform(rotationAngle: self.deg2rad(tab == self.selectedTab ? 45 : 0))
                }, completion: nil)
            }
        }
    }
    
    private func deg2rad(_ number: CGFloat) -> CGFloat {
        return number * .pi / 180
    }
    
    // MARK: Add Sheet
    
    private func presentActionSheet() {
        UIView.animate(withDuration: 0.15) {
            self.dimmedOverlay.alpha = 1
        }
        presentActionButtons()
    }
    
    private func dismissActionSheet() {
        UIView.animate(withDuration: 0.15) {
            self.dimmedOverlay.alpha = 0
        }
        dismissActionButtons()
    }
    
    @IBAction func dimmedViewTapped() {
        if selectedTab == .add {
            select(tab: previousTab)
        }
    }
    
    func presentActionButtons() {
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveLinear, animations: {
            self.actionSheetButtonOneBottomConstraint.constant = 68
            self.actionSheetButtonTwoBottomConstraint.constant = 16
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // swiftlint:disable multiple_closures_with_trailing_closure
    func dismissActionButtons(with completionHandler: (() -> Void)? = nil) {
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.15, animations: {
            self.actionSheetButtonOneBottomConstraint.constant = -50
            self.actionSheetButtonTwoBottomConstraint.constant = -50
            self.view.layoutIfNeeded()
        }) { (_) in
            completionHandler?()
        }
    }
    
    @IBAction func presentCreateWishlistFromActionSheet() {
        select(tab: previousTab)
        presentCreateWishlist()
    }
    
    @IBAction func presentCreateWishlist() {
        guard let controller = UIStoryboard(name: "AddWishlist", bundle: nil).instantiateInitialViewController() as? WishlistTypeViewController else { return }
        let navController = BaseNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        present(navController, animated: true, completion: nil)
    }
    
    @IBAction func presentAddNewItemFromActionSheet() {
        select(tab: previousTab)
        presentAddNewItem()
    }
    
    func presentAddNewItem(with wishlist: Wishlist? = nil, item: Item? = nil) {
        if DataManager.shared.wishlists.count == 0 {
            presentCreateWishlist()
            return
        }
        
        if let item = item {
            guard let controller = UIStoryboard(name: "AddItem", bundle: nil).instantiateViewController(withIdentifier: "AddItemDetailsViewController") as? AddItemDetailsViewController else { return }
            controller.wishlist = wishlist
            controller.item = item
            controller.isDuplicating = true
            let navController = BaseNavigationController(rootViewController: controller)
            navController.modalPresentationStyle = .fullScreen
            present(navController, animated: true, completion: nil)
        } else {
            guard let controller = UIStoryboard(name: "AddItem", bundle: nil).instantiateInitialViewController() as? AddItemViewController else { return }
            controller.wishlist = wishlist
            let navController = BaseNavigationController(rootViewController: controller)
            navController.modalPresentationStyle = .fullScreen
            present(navController, animated: true, completion: nil)
        }
    }
    
    // MARK: Invite Friends Popover
    
    @IBAction func presentInvite(_ sender: Any) {
        presentShare()
        dismissInvitePrompt()
    }
    
    func presentInvitePrompt() {
        let appOpens = UserDefaults.standard.integer(forKey: Config.userDefaults.appOpens)
        guard appOpens == 2 || appOpens == 5 else { return }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveLinear, animations: {
            self.invitePromptCenterConstraint.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func dismissInvitePrompt() {
        UserDefaults.standard.set(true, forKey: Config.userDefaults.inviteFriendsPromptDisplayed)
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2) {
            self.invitePromptCenterConstraint.constant = UIScreen.main.bounds.size.height
            self.view.layoutIfNeeded()
        }
    }
}

extension MainViewController: ItemDetailViewDelegate, PurchaseItemViewDelegate, WishlistHeaderDelegate {
    
    func presentAddItemView(for item: Item) {
        if DataManager.shared.wishlists.count == 0 {
            presentCreateWishlist()
            return
        }
        
        let controller = UIAlertController(title: "Choose a Wishlist", message: nil, preferredStyle: .actionSheet)
        for wishlist in DataManager.shared.wishlists {
            controller.addAction(UIAlertAction(title: wishlist.name, style: .default, handler: { (_) in
                self.presentAddNewItem(with: wishlist, item: item)
            }))
        }
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    func presentPurchaseItemView(for item: Item, showPurchaseStatus: Bool) {
        
        shadowView = UIView(frame: view.bounds)
        if let shadowView = shadowView {
            shadowView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            shadowView.alpha = 0
            view.addSubview(shadowView)
            shadowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissPurchaseItemSheet)))
        }
        
        UIView.animate(withDuration: 0.2) {
            self.shadowView?.alpha = 1
        }
        
        let sheet = PurchaseItemView.instance()
        sheet.use(item, showPurchaseStatus: showPurchaseStatus)
        sheet.delegate = self
        view.addSubview(sheet)
        sheet.translatesAutoresizingMaskIntoConstraints = false
        overlayConstraint = sheet.topAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        overlayConstraint?.isActive = true
        sheet.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        sheet.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        
        view.layoutIfNeeded()
        
        let height = sheet.stackView.bounds.size.height + view.safeAreaInsets.bottom
        UIView.animate(withDuration: 0.45, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveLinear, animations: {
            self.overlayConstraint?.constant = -height
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func dismissPurchaseItemSheet() {
        UIView.animate(withDuration: 0.15, animations: {
            self.shadowView?.alpha = 0
            self.overlayConstraint?.constant = 0
            self.view.layoutIfNeeded()
        }) { (_) in
            self.shadowView?.removeFromSuperview()
            self.shadowView = nil
            self.overlay?.removeFromSuperview()
            self.overlay = nil
            self.overlayConstraint = nil
        }
    }
    
    func openPurchaseWebsite(item: Item) {
        guard item.url.isEmpty == false, let url = item.skimlinksUrl else { return }
        let controller = SFSafariViewController(url: url)
        present(controller, animated: true, completion: nil)
    }
    
    func presentItems(with tag: String) {
        guard let navigationController = controllers[selectedTab.rawValue] as? UINavigationController, let controller = UIStoryboard(name: "Wishlists", bundle: nil).instantiateViewController(withIdentifier: "ItemsViewController") as? ItemsViewController else {
            return
        }
        
        controller.mode = .tag
        controller.tag = tag
        navigationController.pushViewController(controller, animated: true)
    }
    
    func presentProfile(with id: String) {
        guard let navigationController = controllers[selectedTab.rawValue] as? UINavigationController, let controller = UIStoryboard(name: "Wishlists", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? WishlistsViewController else {
            return
        }
        
        controller.id = id
        navigationController.pushViewController(controller, animated: true)
    }
    
    func presentAlert(_ controller: UIAlertController) {
        present(controller, animated: true, completion: nil)
    }
}

extension MainViewController: PaymentLinksViewDelegate {
    
    func presentPaymentLinksView(_ profile: Profile) {
        
        shadowView = UIView(frame: view.bounds)
        if let shadowView = shadowView {
            shadowView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            shadowView.alpha = 0
            view.addSubview(shadowView)
            shadowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissPaymentLinksSheet)))
        }
        
        UIView.animate(withDuration: 0.2) {
            self.shadowView?.alpha = 1
        }
        
        let sheet = PaymentLinksView.instance()
        sheet.use(profile)
        sheet.delegate = self
        view.addSubview(sheet)
        sheet.translatesAutoresizingMaskIntoConstraints = false
        overlayConstraint = sheet.topAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        overlayConstraint?.isActive = true
        sheet.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        sheet.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        
        view.layoutIfNeeded()
        
        let height = sheet.stackView.bounds.size.height + view.safeAreaInsets.bottom
        UIView.animate(withDuration: 0.45, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveLinear, animations: {
            self.overlayConstraint?.constant = -height
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func dismissPaymentLinksSheet() {
        UIView.animate(withDuration: 0.15, animations: {
            self.shadowView?.alpha = 0
            self.overlayConstraint?.constant = 0
            self.view.layoutIfNeeded()
        }) { (_) in
            self.shadowView?.removeFromSuperview()
            self.shadowView = nil
            self.overlay?.removeFromSuperview()
            self.overlay = nil
            self.overlayConstraint = nil
        }
    }
    
    func openPaymentsLink(url: URL) {
        let controller = SFSafariViewController(url: url)
        present(controller, animated: true, completion: nil)
    }
}

// MARK: Deep linking
extension MainViewController {
    
    // swiftlint:disable cyclomatic_complexity
    func performDeepLinkRouting() {
        guard let app = UIApplication.shared.delegate as? AppDelegate, let location = app.deepLinkLocation else { return }
        let value = app.deepLinkValue
        
        app.deepLinkLocation = nil
        app.deepLinkValue = nil
        
        switch location {
        case .notifications:
            if selectedTab != .home {
                select(tab: .home)
            }
            guard let navigationController = controllers[selectedTab.rawValue] as? UINavigationController, let first = navigationController.viewControllers.first else { return }
            if let controller = UIStoryboard(name: "Notifications", bundle: nil).instantiateInitialViewController() {
                navigationController.viewControllers = [first, controller]
            }
        case .profile:
            if selectedTab != .home {
                select(tab: .home)
            }
            guard let navigationController = controllers[selectedTab.rawValue] as? UINavigationController, let first = navigationController.viewControllers.first else { return }
            if let controller = UIStoryboard(name: "Wishlists", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? WishlistsViewController, let id = value as? String {
                controller.id = id
                navigationController.viewControllers = [first, controller]
            }
        case .wishlist:
            if selectedTab != .home {
                select(tab: .home)
            }
            guard let navigationController = controllers[selectedTab.rawValue] as? UINavigationController, let first = navigationController.viewControllers.first else { return }
            if let controller = UIStoryboard(name: "Wishlists", bundle: nil).instantiateViewController(withIdentifier: "WishlistViewController") as? WishlistViewController, let id = value as? Int {
                controller.id = id
                navigationController.viewControllers = [first, controller]
            }
        case .item:
            if selectedTab != .home {
                select(tab: .home)
            }
            guard let navigationController = controllers[selectedTab.rawValue] as? UINavigationController, let first = navigationController.viewControllers.first else { return }
            if let controller = UIStoryboard(name: "Wishlists", bundle: nil).instantiateViewController(withIdentifier: "ItemViewController") as? ItemViewController, let id = value as? Int {
                controller.id = id
                navigationController.viewControllers = [first, controller]
            }
        case .inviteFriends:
            presentShare()
        }
    }
}

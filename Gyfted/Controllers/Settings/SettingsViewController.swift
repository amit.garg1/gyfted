//
//  SettingsViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/08/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import SafariServices
import FirebaseAuth
import OneSignal
import MessageUI

class SettingsViewController: UITableViewController {
    
    enum SettingsSection: Int {
        case account
        case help
        case legal
        case signOut
    }
        
    enum AccountRow: Int {
        case email
        case username
        case password
        case twitter
    }
    
    enum HelpRow: Int {
        case faqs
        case help
        case feedback
    }
    
    enum LegalRow: Int {
        case privacyPolicy
        case termsAndConditions
        case deleteAccount
    }
    
    var profile: Profile?
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var twitterAccountCell: UITableViewCell!
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerToPresent.modalPresentationStyle = .fullScreen
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Settings", comment: "").uppercased()
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissView))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let profile = profile else { return }
        emailLabel.text = profile.email
        usernameLabel.text = profile.username
        passwordLabel.text = "•••••••••"
        if let provider = Auth.auth().currentUser?.providerData.first?.providerID, provider == FacebookAuthProviderID {
            passwordLabel.text = NSLocalizedString("Facebook Login", comment: "")
        }
        if let provider = Auth.auth().currentUser?.providerData.first?.providerID, provider == "apple.com" {
            passwordLabel.text = NSLocalizedString("Signed in with Apple", comment: "")
        }
    }
    
    @objc func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let section = SettingsSection(rawValue: indexPath.section) else { return }
        switch section {
        case .account:
            guard let row = AccountRow(rawValue: indexPath.row) else { return }
            editAccount(row)
        case .help:
            guard let row = HelpRow(rawValue: indexPath.row) else { return }
            presentHelp(row)
        case .legal:
            guard let row = LegalRow(rawValue: indexPath.row) else { return }
            presentLegal(row)
        case .signOut:
            signOut()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 3 && Config.isTwitterEnabled == false {
            return 0
        }
        return 44
    }
    
    // swiftlint:disable function_body_length cyclomatic_complexity
    func editAccount(_ type: AccountRow) {
        guard let profile = profile else { return }
        switch type {
        case .email:
            guard let controller = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "RegisterEmailViewController") as? RegisterEmailViewController else { return }
            controller.profile = profile
            controller.mode = .edit
            present(RegisterNavigationController(rootViewController: controller), animated: true, completion: nil)
        case .username:
            guard let controller = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "RegisterUsernameViewController") as? RegisterUsernameViewController else { return }
            controller.profile = profile
            controller.mode = .edit
            present(RegisterNavigationController(rootViewController: controller), animated: true, completion: nil)
        case .password:
            if let provider = Auth.auth().currentUser?.providerData.first?.providerID, provider == FacebookAuthProviderID {
                let controller = UIAlertController(title: NSLocalizedString("Facebook Login", comment: ""), message: NSLocalizedString("You cannot change your password as your account uses Facebook Login.", comment: ""), preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(controller, animated: true, completion: nil)
                return
            }
            
            if let provider = Auth.auth().currentUser?.providerData.first?.providerID, provider == "apple.com" {
                let controller = UIAlertController(title: NSLocalizedString("Signed in with Apple", comment: ""), message: NSLocalizedString("You cannot change your password as your account uses Sign in with Apple.", comment: ""), preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(controller, animated: true, completion: nil)
                return
            }

            let controller = UIAlertController(title: NSLocalizedString("Verification", comment: ""), message: NSLocalizedString("Please enter your current password.", comment: ""), preferredStyle: .alert)
            controller.addTextField { (textField) in
                textField.isSecureTextEntry = true
                textField.placeholder = NSLocalizedString("Current Password", comment: "")
            }
            controller.addAction(UIAlertAction(title: NSLocalizedString("Sign In", comment: ""), style: .default, handler: { (_) in
                let credential = EmailAuthProvider.credential(withEmail: profile.email, password: controller.textFields?.first?.text ?? "")
                Auth.auth().signIn(with: credential) { (_, error) in
                    if let error = error {
                        let controller = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
                        controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(controller, animated: true, completion: nil)
                    } else {
                        guard let controller = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "RegisterPasswordViewController") as? RegisterPasswordViewController else { return }
                        controller.profile = profile
                        controller.mode = .edit
                        controller.credential = credential
                        self.present(RegisterNavigationController(rootViewController: controller), animated: true, completion: nil)
                    }
                }
            }))
            controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            self.present(controller, animated: true, completion: nil)
        case .twitter:
            let controller = UIAlertController(title: "Remove Twitter Account", message: "Are you sure you want to remove your Twitter account? You'll need to sign back in again if you want to post automatic tweets or share your wishlists on Twitter.", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Remove", style: .destructive, handler: { (_) in
                Config.sharedDefaults().removeObject(forKey: Config.userDefaults.twitterToken)
                Config.sharedDefaults().removeObject(forKey: Config.userDefaults.twitterSecret)
                Config.sharedDefaults().removeObject(forKey: Config.userDefaults.autoTweetEnabled)
                Config.sharedDefaults().synchronize()
                self.tableView.reloadData()
            }))
            controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func presentHelp(_ type: HelpRow) {
        switch type {
        case .faqs:
            present(Config.websites.faqs)
        case .help:
            present(Config.websites.help)
        case .feedback:
            presentFeedbackEmail()
        }
    }
    
    func presentLegal(_ type: LegalRow) {
        switch type {
        case .privacyPolicy:
            present(Config.websites.privacy)
        case .termsAndConditions:
            present(Config.websites.terms)
        case .deleteAccount:
            let controller = UIAlertController(title: "Delete Account?", message: "Are you sure you want to delete your account? All of your wishlists and items will be deleted - this action cannot be undone!", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Delete Account", style: .destructive, handler: { (_) in
                self.performDeleteAccount()
            }))
            controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(controller, animated: true, completion: nil)
        }
    }
    
    func signOut() {
        Profile.signOut()
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController(), let window = UIApplication.shared.keyWindow else { return }
        window.rootViewController = controller
    }
    
    func present(_ link: String) {
        guard let url = URL(string: link) else { return }
        let controller = SFSafariViewController(url: url)
        present(controller, animated: true, completion: nil)
    }
    
    func presentFeedbackEmail() {
        if MFMailComposeViewController.canSendMail() == false {
            let controller = UIAlertController(title: "Send Feedback", message: "You can email feedback to us at \(Config.emails.feedback)", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(controller, animated: true, completion: nil)
            return
        }
        let controller = MFMailComposeViewController()
        controller.mailComposeDelegate = self
        controller.setToRecipients([Config.emails.feedback])
        controller.setSubject("Gyfted Feedback")
        present(controller, animated: true, completion: nil)
    }
    
    func performDeleteAccount() {
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/delete"), parameters: nil) { (_) in
            Auth.auth().currentUser?.delete(completion: { (_) in
                self.signOut()
            })
        }
    }
}

extension SettingsViewController: MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}

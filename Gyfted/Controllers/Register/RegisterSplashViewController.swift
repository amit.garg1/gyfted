//
//  RegisterSplashViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 03/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import FirebaseAuth
import FBSDKCoreKit
import FirebaseAnalytics
import AuthenticationServices
import CryptoKit

class RegisterSplashViewController: BaseRegisterViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBOutlet weak var signInWithAppleContainer: UIView!
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    
    fileprivate var currentNonce: String?
    
    var isFirstRun = true
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let profile = sender as? RegistrationProfile, let controller = segue.destination as? BaseRegisterViewController {
            controller.registrationProfile = profile
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            signInWithAppleContainer.isHidden = false
            let signInWithAppleButton = ASAuthorizationAppleIDButton()
            signInWithAppleButton.frame = signInWithAppleContainer.bounds
            signInWithAppleButton.addTarget(self, action: #selector(signInWithApple), for: .touchUpInside)
            signInWithAppleContainer.addSubview(signInWithAppleButton)
        }
        
        for i in 1...4 {
            view.viewWithTag(i)?.alpha = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isFirstRun {
            isFirstRun = false
            startAnimations()
        }
    }
    
    private func startAnimations() {
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.logoTopConstraint.constant = -100
            self.view.layoutIfNeeded()
        }) { (_) in
            self.addSignInButton()
        }
        
        for i in 1...4 {
            self.view.viewWithTag(i)?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            UIView.animate(withDuration: 0.2, delay: 0.2 + (0.15 * Double(i)), options: .curveLinear, animations: {
                self.view.viewWithTag(i)?.alpha = 1
                self.view.viewWithTag(i)?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: nil)
        }
    }
    
    private func addSignInButton() {
        let button = UIBarButtonItem(title: NSLocalizedString("Sign In", comment: ""), style: .plain, target: self, action: #selector(self.presentLogIn))
        navigationItem.rightBarButtonItem = button
        for state in [UIControl.State.normal, UIControl.State.highlighted, UIControl.State.selected] {
            button.setTitleTextAttributes([.foregroundColor: UIColor.white, .font: UIFont.systemFont(ofSize: 16, weight: .bold)], for: state)
        }
    }
    
    @objc func presentLogIn() {
        performSegue(withIdentifier: "PresentLogIn", sender: nil)
    }
    
    // swiftlint:disable function_body_length
    @IBAction func signInWithFacebook(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [ .publicProfile, .email ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                self.present(error.localizedDescription)
            case .cancelled:
                break
            case .success(let grantedPermissions, _, let accessToken):
                
                let loadingController = UIAlertController(title: NSLocalizedString("Loading...", comment: ""), message: nil, preferredStyle: .alert)
                self.present(loadingController, animated: false, completion: nil)
                let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
                Auth.auth().signIn(with: credential) { (_, error) in
                    if let error = error {
                        loadingController.dismiss(animated: false, completion: {
                            self.present(error.localizedDescription)
                        })
                        return
                    }
                    
                    APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/\(Profile.userId)/"), parameters: nil) { (handler) in
                        switch handler {
                        case .success(let data):
                            Config.sharedDefaults().set(data, forKey: Config.userDefaults.profile)
                            loadingController.dismiss(animated: false, completion: {
                                Analytics.logEvent(AnalyticsEventLogin, parameters: [
                                    AnalyticsParameterMethod: "facebook"
                                ])
                                self.dismissView()
                            })
                        case .failure:
                            let profile = RegistrationProfile()
                            profile.avatarUrlString = "https://graph.facebook.com/\(accessToken.userID)/picture?type=large"
                            
                            var parameters = [String: Any]()
                            parameters["fields"] = grantedPermissions.contains(.email) ? "name,email" : "name"
                            let connection = GraphRequestConnection()
                            connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "name,email"]), completionHandler: { (_, response, _) in
                                if let response = response as? [String: Any], let name = response["name"] as? String {
                                    loadingController.dismiss(animated: true, completion: {
                                        profile.name = name
                                        if let email = response["email"] as? String {
                                            profile.email = email
                                            self.performSegue(withIdentifier: "PresentUsername", sender: profile)
                                        } else {
                                            self.performSegue(withIdentifier: "PresentEmail", sender: profile)
                                        }
                                    })
                                } else {
                                    loadingController.dismiss(animated: false, completion: {
                                        self.present(NSLocalizedString("We were unable to connect to your Facebook account. Please try again ensuring you have a good network connection.", comment: ""))
                                    })
                                }
                            })
                            connection.start()
                        }
                    }
                }
            }
        }
    }
    
    @available(iOS 13.0, *)
    @objc func signInWithApple() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let request = ASAuthorizationAppleIDProvider().createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        let appleAuthController = ASAuthorizationController(authorizationRequests: [request])
        appleAuthController.delegate = self
        appleAuthController.presentationContextProvider = self
        appleAuthController.performRequests()
    }
    
    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
      let inputData = Data(input.utf8)
      let hashedData = SHA256.hash(data: inputData)
      let hashString = hashedData.compactMap {
        return String(format: "%02x", $0)
      }.joined()

      return hashString
    }
    
    private func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: [Character] =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }

        randoms.forEach { random in
          if length == 0 {
            return
          }

          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }

      return result
    }
   
}

@available(iOS 13.0, *)
extension RegisterSplashViewController: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        guard let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential else { return }
        guard let nonce = currentNonce else { return }
        guard let appleIDToken = appleIDCredential.identityToken else { return }
        guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else { return }
        
        let loadingController = UIAlertController(title: NSLocalizedString("Loading...", comment: ""), message: nil, preferredStyle: .alert)
        self.present(loadingController, animated: false, completion: nil)

        let credential = OAuthProvider.credential(withProviderID: "apple.com", idToken: idTokenString, rawNonce: nonce)
        Auth.auth().signIn(with: credential) { (_, error) in
            if let error = error {
                loadingController.dismiss(animated: false, completion: {
                    self.present(error.localizedDescription)
                })
                return
            }
            
            APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/\(Profile.userId)/"), parameters: nil) { (handler) in
                switch handler {
                case .success(let data):
                    Config.sharedDefaults().set(data, forKey: Config.userDefaults.profile)
                    loadingController.dismiss(animated: false, completion: {
                        Analytics.logEvent(AnalyticsEventLogin, parameters: [
                            AnalyticsParameterMethod: "apple"
                        ])
                        self.dismissView()
                    })
                case .failure:
                    loadingController.dismiss(animated: true, completion: {
                        let profile = RegistrationProfile()
                        var nameComponents = [String?]()
                        nameComponents.append(appleIDCredential.fullName?.givenName)
                        nameComponents.append(appleIDCredential.fullName?.familyName)
                        profile.name = nameComponents.compactMap({$0}).joined(separator: " ")
                        if let email = appleIDCredential.email {
                            profile.email = email
                            self.performSegue(withIdentifier: "PresentUsername", sender: profile)
                        } else {
                            self.performSegue(withIdentifier: "PresentEmail", sender: profile)
                        }
                    })
                }
            }
        }
    }
}

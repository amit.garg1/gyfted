//
//  RegisterPasswordViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseAuth

enum RegisterPasswordMode {
    case register
    case edit
}

class RegisterPasswordViewController: BaseRegisterViewController {

    @IBOutlet weak var validationTick: UIImageView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    var mode: RegisterPasswordMode = .register
    var profile: Profile?
    var credential: AuthCredential?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch mode {
        case .register:
            title = NSLocalizedString("Sign Up", comment: "").uppercased()
        case .edit:
            title = NSLocalizedString("Update Password", comment: "").uppercased()
            nextButton.setTitle(NSLocalizedString("Save", comment: "").uppercased(), for: .normal)
        }
        validationTick.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        passwordField.becomeFirstResponder()
    }

    @IBAction func presentNextPage(_ sender: Any) {
        if isValidPassword == false {
            let controller = UIAlertController(title: NSLocalizedString("Invalid Password", comment: ""), message: NSLocalizedString("Your password must contain at least 9 characters, a number, an uppercase letter, a lowercase letter, and a special character.", comment: ""), preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(controller, animated: true, completion: nil)
            return
        }
        
        switch mode {
        case .register:
            registrationProfile?.password = passwordField.text ?? ""
            performSegue(withIdentifier: "PresentUsername", sender: nil)
        case .edit:
            guard let credential = credential else { return }
            let password = passwordField.text ?? ""
            Auth.auth().currentUser?.reauthenticate(with: credential, completion: { (_, _) in
                Auth.auth().currentUser?.updatePassword(to: password, completion: { (_) in
                    self.dismiss(animated: true, completion: nil)
                })
            })
        }
    }

    var isValidPassword: Bool {
        
        guard let password = passwordField.text else {
            return false
        }
        
        if password.count < 9 {
            return false
        }
        let nonUpperCase = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
        let letters = password.components(separatedBy: nonUpperCase)
        let strUpper = letters.joined()
        
        let smallLetterRegEx  = ".*[a-z]+.*"
        let smalltest = NSPredicate(format: "SELF MATCHES %@", smallLetterRegEx)
        let smallresult = smalltest.evaluate(with: password)
        
        let numberRegEx  = ".*[0-9]+.*"
        let numbertest = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        let numberresult = numbertest.evaluate(with: password)
        
        let regex = try? NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
        var isSpecial: Bool = false
        if regex?.firstMatch(in: password, options: NSRegularExpression.MatchingOptions(), range: NSMakeRange(0, password.count)) != nil {
            isSpecial = true
        } else {
            isSpecial = false
        }
        return (strUpper.count >= 1) && smallresult && numberresult && isSpecial
    }
}

extension RegisterPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        presentNextPage(textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        Timer.scheduledTimer(withTimeInterval: 0.005, repeats: false) { (_) in
            self.validationTick.isHidden = !self.isValidPassword
        }
        return true
    }
}

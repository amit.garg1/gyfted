//
//  RegisterEmailViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseAuth

class RegisterEmailViewController: BaseRegisterViewController {
    
    enum RegisterEmailError: Error {
        case invalidEmail
        case emailExists
    }
    
    enum RegisterEmailMode {
        case register
        case edit
    }
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var nextButton: UIButton!
    
    var mode: RegisterEmailMode = .register
    var profile: Profile?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        switch mode {
        case .register:
            navigationItem.title = NSLocalizedString("Sign Up", comment: "").uppercased()
            if registrationProfile == nil {
                registrationProfile = RegistrationProfile()
            }
            nextButton.setTitle(NSLocalizedString("Next", comment: "").uppercased(), for: .normal)
        case .edit:
            navigationItem.title = NSLocalizedString("Update Email", comment: "").uppercased()
            nextButton.setTitle(NSLocalizedString("Save", comment: "").uppercased(), for: .normal)
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
            emailField.text = profile?.email
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emailField.becomeFirstResponder()
        spinner.stopAnimating()
        nextButton.isEnabled = true
    }

    // swiftlint:disable function_body_length cyclomatic_complexity
    @IBAction func presentNextPage(_ sender: Any) {
        guard let email = emailField.text?.trimmingCharacters(in: .whitespacesAndNewlines), email.isEmpty == false else { return }
        
        guard let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else { return }
        let range = NSMakeRange(0, NSString(string: email).length)
        let allMatches = dataDetector.matches(in: email, options: [], range: range)
        guard allMatches.count == 1, allMatches.first?.url?.absoluteString.contains("mailto:") ?? false else {
            present(error: .invalidEmail)
            return
        }

        spinner.startAnimating()
        nextButton.isEnabled = false
        nextButton.setTitle(nil, for: .normal)
        
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/registrationCheck", with: ["email": email]), parameters: nil) { (handler) in
            switch handler {
            case .success:
                switch self.mode {
                case .register:
                    self.registrationProfile?.email = email
                    let provider = Auth.auth().currentUser?.providerData.first?.providerID
                    switch provider {
                    case FacebookAuthProviderID, "apple.com":
                        self.performSegue(withIdentifier: "PresentUsername", sender: nil)
                    default:
                        self.performSegue(withIdentifier: "PresentPassword", sender: nil)
                    }
                case .edit:
                    guard let profile = self.profile else { return }
                    var data = [String: Any]()
                    data["id"] = profile.id
                    data["email"] = email
                    if profile.avatarUrlString.contains("gravatar.com") {
                        data["avatarUrlString"] = "https://www.gravatar.com/avatar/\(email.hashed(.md5) ?? "")?s=200&default=identicon"
                    }
                    APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/save"), parameters: data) { (_) in
                        self.profile?.email = email
                        self.dismissView()
                    }
                }
            case .failure:
                self.present(error: .emailExists)
                self.spinner.stopAnimating()
                self.nextButton.isEnabled = true
                switch self.mode {
                case .register:
                    self.nextButton.setTitle(NSLocalizedString("Next", comment: "").uppercased(), for: .normal)
                case .edit:
                    self.nextButton.setTitle(NSLocalizedString("Save", comment: "").uppercased(), for: .normal)
                }
                
            }
        }
    }
    
    private func present(error: RegisterEmailError) {
        
        var title: String
        var message: String
        
        switch error {
        case .emailExists:
            title = NSLocalizedString("Email In Use", comment: "")
            message = NSLocalizedString("This email address already has a Gyfted account associated with it. Please log in to your existing account.", comment: "")
        case .invalidEmail:
            title = NSLocalizedString("Invalid Email", comment: "")
            message = NSLocalizedString("The entered email address doesn't appear to be valid. Please try again.", comment: "")
        }
        
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
}

extension RegisterEmailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        presentNextPage(textField)
        return true
    }
}

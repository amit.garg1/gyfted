//
//  RegisterUsernameViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 22/05/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseAuth

class RegisterUsernameViewController: BaseRegisterViewController {

    enum RegisterUsernameError: Error {
        case invalidUsername
        case usernameExists
    }
    
    enum RegisterUsernameMode {
        case register
        case edit
    }
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var nextButton: UIButton!
    
    var mode: RegisterUsernameMode = .register
    var profile: Profile?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch mode {
        case .register:
            navigationItem.title = NSLocalizedString("Sign Up", comment: "").uppercased()
            NotificationCenter.default.addObserver(self, selector: #selector(forceLowercase), name: UITextField.textDidChangeNotification, object: usernameField)
            nextButton.setTitle(NSLocalizedString("Next", comment: "").uppercased(), for: .normal)
        case .edit:
            navigationItem.title = NSLocalizedString("Update Username", comment: "").uppercased()
            nextButton.setTitle(NSLocalizedString("Save", comment: "").uppercased(), for: .normal)
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
            usernameField.text = profile?.username
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        usernameField.becomeFirstResponder()
        spinner.stopAnimating()
        nextButton.isEnabled = true
    }
    
    @IBAction func presentNextPage(_ sender: Any) {
        guard let username = usernameField.text?.trimmingCharacters(in: .whitespacesAndNewlines), username.isEmpty == false else { return }
        
        if username.count < 3 || username.isAlphanumeric != true {
            present(error: .invalidUsername)
            return
        }

        spinner.startAnimating()
        nextButton.isEnabled = false
        nextButton.setTitle(nil, for: .normal)
        
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/registrationCheck", with: ["username": username]), parameters: nil) { (handler) in
            switch handler {
            case .success:
                switch self.mode {
                case .register:
                    self.registrationProfile?.username = username
                    let provider = Auth.auth().currentUser?.providerData.first?.providerID
                    switch provider {
                    case FacebookAuthProviderID, "apple.com":
                        self.performSegue(withIdentifier: "PresentBirthday", sender: nil)
                    default:
                        self.performSegue(withIdentifier: "PresentName", sender: nil)
                    }
                    
                case .edit:
                    guard let profile = self.profile else { return }
                    var data = [String: Any]()
                    data["id"] = profile.id
                    data["username"] = username
                    APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/save"), parameters: data) { (_) in
                        self.profile?.username = username
                        self.dismissView()
                    }
                }
            case .failure:
                self.present(error: .usernameExists)
                self.spinner.stopAnimating()
                self.nextButton.isEnabled = true
                self.nextButton.setTitle(NSLocalizedString("Next", comment: "").uppercased(), for: .normal)
            }
        }
    }
    
    private func present(error: RegisterUsernameError) {
        
        var title: String
        var message: String
        
        switch error {
        case .usernameExists:
            title = NSLocalizedString("Username In Use", comment: "")
            message = NSLocalizedString("This username has already been claimed by another Gyfted user. Please enter a different username.", comment: "")
        case .invalidUsername:
            title = NSLocalizedString("Invalid Username", comment: "")
            message = NSLocalizedString("The entered username is not valid. Please ensure you are not using any special characters or spaces and that the name is at least 3 characters long.", comment: "")
        }
        
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
}

extension RegisterUsernameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        presentNextPage(textField)
        return true
    }
    
    @objc func forceLowercase() {
        usernameField.text = usernameField.text?.lowercased()
    }
}

//
//  LogInViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 11/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAnalytics

class LogInViewController: BaseViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Sign In", comment: "").uppercased()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emailField.becomeFirstResponder()
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        guard let email = emailField.text, email.isEmpty == false, let password = passwordField.text, password.isEmpty == false else { return }
        
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
        
        spinner.startAnimating()
        button.isEnabled = false
        button.setTitle(nil, for: .normal)
        
        Auth.auth().signIn(withEmail: email, password: password) { (_, error) in
            if let error = error {
                let controller = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(controller, animated: true, completion: nil)
                
                self.spinner.stopAnimating()
                self.button.isEnabled = true
                self.button.setTitle(NSLocalizedString("Sign In", comment: "").uppercased(), for: .normal)
                return
            } else {
                Analytics.logEvent(AnalyticsEventLogin, parameters: [
                    AnalyticsParameterMethod: "email"
                ])
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func requestNewPassword(_ sender: UIButton) {
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
        
        let controller = UIAlertController(title: NSLocalizedString("Request New Password", comment: ""), message: NSLocalizedString("Please enter your email address in order to request a new password.", comment: ""), preferredStyle: .alert)
        controller.addTextField { (textField) in
            textField.keyboardType = .emailAddress
            textField.textContentType = .emailAddress
            textField.keyboardAppearance = .dark
            textField.placeholder = NSLocalizedString("Email Address", comment: "")
            textField.textAlignment = .center
        }
        controller.addAction(UIAlertAction(title: NSLocalizedString("Reset Password", comment: ""), style: .default, handler: { (_) in
            guard let email = controller.textFields?.first?.text, email.isEmpty == false else { return }
            Auth.auth().sendPasswordReset(withEmail: email, completion: { (error) in
                if let error = error {
                    let controller = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
                    controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    self.present(controller, animated: true, completion: nil)
                } else {
                    let controller = UIAlertController(title: NSLocalizedString("Password Reset", comment: ""), message: "Instructions for resetting your password have been sent to your email address.", preferredStyle: .alert)
                    controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    self.present(controller, animated: true, completion: nil)
                }
            })
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
}

extension LogInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        } else if textField == passwordField {
            passwordField.resignFirstResponder()
        }
        return true
    }
    
}

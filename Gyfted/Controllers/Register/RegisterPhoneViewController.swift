//
//  RegisterPhoneViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 10/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import PhoneNumberKit
import FirebaseAuth
import FirebaseAnalytics
import Analytics

enum RegisterPhoneMode {
    case register
    case edit
}

class RegisterPhoneViewController: BaseRegisterViewController {

    @IBOutlet weak var phoneField: PhoneNumberTextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var nextButton: UIButton!
    
    var profile: Profile?
    var mode: RegisterPhoneMode = .register
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch mode {
        case .register:
            title = NSLocalizedString("Information", comment: "").uppercased()
            let button = UIBarButtonItem(title: NSLocalizedString("Skip", comment: ""), style: .plain, target: self, action: #selector(self.skip(_:)))
            navigationItem.rightBarButtonItem = button
            for state in [UIControl.State.normal, UIControl.State.highlighted, UIControl.State.selected] {
                button.setTitleTextAttributes([.foregroundColor: UIColor.white, .font: UIFont.systemFont(ofSize: 16, weight: .bold)], for: state)
            }
        case .edit:
            title = NSLocalizedString("Phone", comment: "").uppercased()
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
            nextButton.setTitle(NSLocalizedString("Save".uppercased(), comment: ""), for: .normal)
            phoneField.text = profile?.phone
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        activateControls()
    }
    
    @IBAction func presentNextPage(_ sender: Any) {
        guard let phone = phoneField.text, phone.isEmpty == false else {
            registrationProfile?.phone = ""
            performRegistration()
            return
        }
        
        let phoneNumberKit = PhoneNumberKit()
        guard let phoneNumber = try? phoneNumberKit.parse(phone) else {
            let controller = UIAlertController(title: NSLocalizedString("Invalid Phone Number", comment: ""), message: NSLocalizedString("This phone number does not appear to be valid. Please try again or skip this step.", comment: ""), preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(controller, animated: true, completion: nil)
            return
        }
        
        registrationProfile?.phone = phoneNumberKit.format(phoneNumber, toType: .international)
        performRegistration()
    }
    
    @IBAction func skip(_ sender: Any) {
        registrationProfile?.phone = ""
        performRegistration()
    }

    private func performRegistration() {
        
        if mode == .edit {
            guard let profile = profile else { return }
            var data = [String: Any]()
            data["id"] = profile.id
            data["phone"] = ""
            let phoneNumberKit = PhoneNumberKit()
            if let phone = phoneField.text, let phoneNumber = try? phoneNumberKit.parse(phone) {
                data["phone"] = phoneNumberKit.format(phoneNumber, toType: .international)
            }
            
            var method = "email"
            if let provider = Auth.auth().currentUser?.providerData.first?.providerID, provider == FacebookAuthProviderID {
                method = "facebook"
            }
            if let provider = Auth.auth().currentUser?.providerData.first?.providerID, provider == "apple.com" {
                method = "apple"
            }
            Analytics.logEvent(AnalyticsEventSignUp, parameters: [
                AnalyticsParameterMethod: method
            ])
            
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/save"), parameters: data) { (_) in
                self.dismissView()
            }
            return
        }
        
        guard let registrationProfile = registrationProfile else { return }
        
        deactivateControls()
        
        if Profile.isLoggedIn {
            saveProfile() // can happen if create user triggers but save profile fails i.e. due to network failure.
            return
        }
        
        Auth.auth().createUser(withEmail: registrationProfile.email, password: registrationProfile.password) { _, error in
            if let error = error {
                let controller = UIAlertController(title: NSLocalizedString("Account Creation Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(controller, animated: true, completion: nil)
                return
            }
            self.saveProfile()
        }
    }
    
    private func saveProfile() {
        guard let registrationProfile = registrationProfile else { return }
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/save"), parameters: registrationProfile.data) { (handler) in
            switch handler {
            case .success:
                SEGAnalytics.shared()?.identify(Profile.userId)
                
                var method = "Email"
                if let provider = Auth.auth().currentUser?.providerData.first?.providerID, provider == FacebookAuthProviderID {
                    method = "Facebook"
                }
                if let provider = Auth.auth().currentUser?.providerData.first?.providerID, provider == "apple.com" {
                    method = "Apple"
                }
                SEGAnalytics.shared()?.track("User Registered", properties: ["accountType": method])
                
                self.performSegue(withIdentifier: "PresentOnboarding", sender: nil)
            case .failure:
                let controller = UIAlertController(title: NSLocalizedString("Profile Creation Error", comment: ""), message: NSLocalizedString("We were unable to save your profile. Please try again ensuring you have a good internet connection.", comment: ""), preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (_) in
                    self.activateControls()
                }))
                self.present(controller, animated: true, completion: nil)
                return
            }
        }
    }
    
    private func activateControls() {
        spinner.stopAnimating()
        nextButton.isEnabled = true
        switch mode {
        case .register:
            nextButton.setTitle(NSLocalizedString("Next", comment: "").uppercased(), for: .normal)
        case .edit:
            nextButton.setTitle(NSLocalizedString("Save", comment: "").uppercased(), for: .normal)
        }
        navigationItem.rightBarButtonItem?.isEnabled = true
        phoneField.becomeFirstResponder()
    }
    
    private func deactivateControls() {
        spinner.startAnimating()
        nextButton.isEnabled = false
        nextButton.setTitle(nil, for: .normal)
        navigationItem.rightBarButtonItem?.isEnabled = false
        phoneField.resignFirstResponder()
    }
    
}

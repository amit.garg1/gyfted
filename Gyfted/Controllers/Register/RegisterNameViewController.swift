//
//  RegisterNameViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 10/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class RegisterNameViewController: BaseRegisterViewController {

    @IBOutlet weak var nameField: UITextField!
  
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Information", comment: "").uppercased()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nameField.becomeFirstResponder()
    }
    
    @IBAction func presentNextPage(_ sender: Any) {
        guard let name = nameField.text, name.isEmpty == false else {
            let controller = UIAlertController(title: NSLocalizedString("Full Name", comment: ""), message: NSLocalizedString("Please enter your full name in order to create your profile.", comment: ""), preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(controller, animated: true, completion: nil)
            return
        }
        registrationProfile?.name = name
        performSegue(withIdentifier: "PresentBirthday", sender: nil)
    }
    
}

extension RegisterNameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        presentNextPage(textField)
        return false
    }
}

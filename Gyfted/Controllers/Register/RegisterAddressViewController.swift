//
//  RegisterAddressViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import GooglePlaces

enum RegisterAddressMode {
    case register
    case edit
}

class RegisterAddressViewController: BaseRegisterViewController {

    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var googleLabel: UILabel!
    
    var mode: RegisterAddressMode = .register
    var profile: Profile?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch mode {
        case .register:
            title = NSLocalizedString("Information", comment: "").uppercased()
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Skip", comment: ""), style: .plain, target: self, action: #selector(self.skip(_:)))
            for state in [UIControl.State.normal, UIControl.State.highlighted, UIControl.State.selected] {
                navigationItem.rightBarButtonItem?.setTitleTextAttributes([.foregroundColor: UIColor.white, .font: UIFont.systemFont(ofSize: 16, weight: .bold)], for: state)
            }
        case .edit:
            title = NSLocalizedString("Address", comment: "").uppercased()
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
            nextButton.setTitle(NSLocalizedString("Save".uppercased(), comment: ""), for: .normal)
            addressField.text = profile?.address
        }
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        googleLabel.attributedText = NSAttributedString(string: NSLocalizedString("Search with Google Places", comment: ""), attributes: [.foregroundColor: Config.colors.light, .paragraphStyle: paragraph, .font: UIFont.italicSystemFont(ofSize: 12), .underlineStyle: NSUnderlineStyle.single.rawValue])

    }
    
    @IBAction func presentNextPage(_ sender: Any) {
        switch mode {
        case .register:
            registrationProfile?.address = ""
            if let address = addressField.text, address.isEmpty == false {
                registrationProfile?.address = address
            }
            performSegue(withIdentifier: "PresentPhone", sender: nil)
        case .edit:
            guard let profile = profile else { return }
            var data = [String: Any]()
            data["id"] = profile.id
            data["address"] = addressField.text ?? ""
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/save"), parameters: data) { (_) in
                self.dismissView()
            }
        }
    }
    
    @IBAction func skip(_ sender: Any) {
        registrationProfile?.address = ""
        performSegue(withIdentifier: "PresentPhone", sender: nil)
    }
    
    @IBAction func presentGooglePlaces() {
        let controller = GMSAutocompleteViewController()
        controller.delegate = self
        controller.placeFields = GMSPlaceField(rawValue: UInt(GMSPlaceField.formattedAddress.rawValue))!
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        controller.autocompleteFilter = filter
        present(controller, animated: true, completion: nil)
    }
}

extension RegisterAddressViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension RegisterAddressViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        addressField.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        NSLog("ERROR: \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}

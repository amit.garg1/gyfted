//
//  RegisterBirthdayViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import AFDateHelper

class RegisterBirthdayViewController: BaseRegisterViewController {
    
    @IBOutlet weak var dobField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    lazy var dobPicker: UIDatePicker = {
        let picker = DarkDatePicker()
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
            picker.setValue(false, forKeyPath: "highlightsToday")
          }
        picker.datePickerMode = .date
        picker.date = Date().adjust(.year, offset: -18)
        picker.maximumDate = Date().adjust(.year, offset: -13)
        picker.addTarget(self, action: #selector(pickerValueDidChange), for: .valueChanged)
        return picker
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Information", comment: "").uppercased()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Skip", comment: ""), style: .plain, target: self, action: #selector(self.skip(_:)))
        dobField.inputView = dobPicker
        pickerValueDidChange()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dobField.becomeFirstResponder()
    }
    
    @IBAction func presentNextPage(_ sender: Any) {
        registrationProfile?.dateOfBirth = dobPicker.date
        performSegue(withIdentifier: "PresentGender", sender: nil)
    }
    
    @IBAction func pickerValueDidChange() {
        dobField.text = DateFormatterManager.shared.shortDateFormatter.string(from: dobPicker.date).replacingOccurrences(of: "-", with: "/").replacingOccurrences(of: "/", with: " / ")
    }
    
    @IBAction func skip(_ sender: Any) {
        registrationProfile?.dateOfBirth = nil
        performSegue(withIdentifier: "PresentGender", sender: nil)
    }
}

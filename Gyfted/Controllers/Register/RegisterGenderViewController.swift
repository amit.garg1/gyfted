//
//  RegisterGenderViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 22/05/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class RegisterGenderViewController: BaseRegisterViewController {
    
    enum GenderType: Int {
        case male
        case female
        case custom
    }

    @IBOutlet weak var genderField: UITextField!
    
    lazy var genderPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = .clear
        return picker
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Information", comment: "").uppercased()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Skip", comment: ""), style: .plain, target: self, action: #selector(self.skip(_:)))
        genderField.tintColor = .clear
        genderField.inputView = genderPicker
        genderField.text = "Male"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        genderField.becomeFirstResponder()
    }
    
    @IBAction func presentNextPage(_ sender: Any) {
        guard let gender = genderField.text, gender.isEmpty == false else {
            let controller = UIAlertController(title: NSLocalizedString("Gender", comment: ""), message: NSLocalizedString("Please enter your gender in order to continue.", comment: ""), preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(controller, animated: true, completion: nil)
            return
        }
        registrationProfile?.gender = gender
        performSegue(withIdentifier: "PresentAddress", sender: nil)
    }
    
    @IBAction func skip(_ sender: Any) {
        registrationProfile?.gender = ""
        performSegue(withIdentifier: "PresentAddress", sender: nil)
    }
}

extension RegisterGenderViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        presentNextPage(textField)
        return false
    }
}

extension RegisterGenderViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        guard let type = GenderType(rawValue: row) else { return nil }
        var title = ""
        switch type {
        case .male:
            title = "Male"
        case .female:
            title = "Female"
        case .custom:
            title = "Custom"
        }
        return NSAttributedString(string: title, attributes: [.font: UIFont.preferredFont(forTextStyle: .largeTitle), .foregroundColor: UIColor.white])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let type = GenderType(rawValue: row) else { return }
        switch type {
        case .male:
            genderField.text = "Male"
        case .female:
            genderField.text = "Female"
        case .custom:
            genderField.inputView = nil
            genderField.resignFirstResponder()
            genderField.text = ""
            genderField.tintColor = Config.colors.light
            genderField.becomeFirstResponder()
        }
    }
}

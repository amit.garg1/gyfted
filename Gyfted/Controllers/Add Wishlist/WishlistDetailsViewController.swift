//
//  WishlistDetailsViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 26/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import WSTagsField
import FirebaseAnalytics
import LUAutocompleteView
import Analytics

class WishlistDetailsViewController: BaseViewController {

    @IBOutlet weak var privacyStackView: UIStackView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var createWishlistButton: UIButton!
    @IBOutlet weak var keywordsContainer: UIView!
    @IBOutlet weak var keywordsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var fauxKeywordTextField: UITextField!
    
    var wishlist: Wishlist?
    
    var privacy = Privacy.everyone
    var type = WishlistType.general
    
    lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.date = Date()
        datePicker.addTarget(self, action: #selector(datePickerValueDidChange), for: .valueChanged)
        return datePicker
    }()
    
    lazy var keywordsField: WSTagsField = {
        let field = WSTagsField()
        field.autocapitalizationType = .allCharacters
        field.clipsToBounds = false
        field.layoutMargins = UIEdgeInsets(top: 4, left: 10, bottom: 4, right: 10)
        field.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        field.spaceBetweenLines = 4.0
        field.spaceBetweenTags = 4.0
        field.font = .systemFont(ofSize: 12.0)
        field.backgroundColor = .white
        field.tintColor = Config.colors.tint
        field.textColor = .white
        field.fieldTextColor = Config.colors.tint
        field.selectedColor = .red
        field.selectedTextColor = .white
        field.isDelimiterVisible = true
        field.returnKeyType = .done
        field.acceptTagOption = .comma
        field.placeholder = NSLocalizedString("Use a comma to separate keywords", comment: "")
        field.placeholderColor = UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
        field.onDidChangeHeightTo = { _, height in
            print("HeightTo", height)
            self.keywordsHeightConstraint.constant = max(30, height + 5)
        }
        field.onDidChangeText = { _, text in
            self.fauxKeywordTextField.text = text
            if text == "" {
                self.fauxKeywordTextField.sendActions(for: .editingDidEnd)
            } else {
                self.fauxKeywordTextField.sendActions(for: .editingChanged)
            }
        }
        return field
    }()
    
    lazy var autocompleteView: LUAutocompleteView = {
        let autocompleteView = LUAutocompleteView()
        autocompleteView.dataSource = self
        autocompleteView.delegate = self
        autocompleteView.throttleTime = 0
        autocompleteView.rowHeight = 30
        autocompleteView.maximumHeight = 150
        autocompleteView.autocompleteCell = CustomAutocompleteTableViewCell.self
        for subview in autocompleteView.subviews {
            if let tableView = subview as? UITableView {
                tableView.separatorInset = .zero
                tableView.layer.borderColor = Config.colors.tint.cgColor
                tableView.layer.borderWidth = 2
                tableView.layer.cornerRadius = 8
                tableView.separatorStyle = .none
                tableView.estimatedRowHeight = 30
            }
        }
        return autocompleteView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("Wishlist Details", comment: "").uppercased()
        
        fields = [nameField, dateField]
        _ = fields.map {
            $0.addDismissToolbar()
            $0.delegate = self
        }
        
        dateField.inputView = datePicker
        dateField.addClearButtonToToolbar()
        
        if let wishlist = wishlist {
            nameField.text = wishlist.name
            if let relevantDate = wishlist.relevantDate, let date = DateFormatterManager.shared.mysqlDateFormatter.date(from: relevantDate) {
                datePicker.date = date
                datePickerValueDidChange()
            }
            keywordsField.addTags(wishlist.keywords)
            privacy = wishlist.privacy
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
            createWishlistButton.setTitle(NSLocalizedString("Update Wishlist", comment: "").uppercased(), for: .normal)
        }
        
        updatePrivacyTicks()
        setupKeyboardNotifications()
        prefillForm()
        
        if wishlist == nil {
            nameField.becomeFirstResponder()
        }
        
        keywordsField.frame = keywordsContainer.bounds
        keywordsContainer.addSubview(keywordsField)
        keywordsField.textDelegate = self
        
        fauxKeywordTextField.superview?.addSubview(autocompleteView)
        autocompleteView.textField = fauxKeywordTextField
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func datePickerValueDidChange() {
        dateField.text = DateFormatterManager.shared.fullDateFormatter.string(from: datePicker.date)
    }
    
    // swiftlint:disable cyclomatic_complexity
    func prefillForm() {
        guard wishlist == nil, let data = Config.sharedDefaults().object(forKey: Config.userDefaults.profile) as? Data, let profile = try? JSONDecoder().decode(Profile.self, from: data) else { return }
        var name = profile.name
        if let firstName = profile.name.components(separatedBy: " ").first {
            name = firstName
        }
        
        switch type {
        case .general:
            return
        case .birthday:
            nameField.text = "\(name)'s Birthday"
            if let dateOfBirth = profile.dateOfBirth, let startDate = DateFormatterManager.shared.mysqlDateFormatter.date(from: dateOfBirth) {
                var date = startDate
                while date.compare(.isInThePast) {
                    date = date.adjust(.year, offset: 1)
                }
                datePicker.date = date
            } else {
                datePicker.date = Date().adjust(.month, offset: 1)
            }
            datePickerValueDidChange()
        case .wedding:
            nameField.text = "\(name)'s Wedding"
        case .anniversary:
            nameField.text = "\(name)'s Anniversary"
        case .babyShower:
            nameField.text = "Baby Shower"
        case .ring:
            nameField.text = "Ring Ideas"
        case .teacher:
            nameField.text = "Help-a-Teacher"
        case .holiday:
            nameField.text = "Holiday"
        }
        
        keywordsField.addTag(type.keyword)
    }
    
    func updatePrivacyTicks() {
        for view in privacyStackView.arrangedSubviews {
            let imageView = view.subviews.compactMap { $0 as? UIImageView }.first
            imageView?.isHidden = view.tag != privacy.rawValue
        }
    }

    @IBAction func privacySettingSelected(_ sender: UITapGestureRecognizer) {
        privacy = Privacy(rawValue: sender.view?.tag ?? 0) ?? .everyone
        updatePrivacyTicks()
    }
    
    private func updateHighlight(for field: UITextField, color: UIColor) {
        guard let subviews = field.superview?.subviews else { return }
        for subview in subviews where subview is UILabel {
            (subview as? UILabel)?.textColor = color
        }
    }
    
    // swiftlint:disable function_body_length
    @IBAction func createWishlist(_ sender: Any) {
        resignResponders()
        guard let name = nameField.text?.trimmingCharacters(in: .whitespacesAndNewlines), name.isEmpty == false else {
            present(NSLocalizedString("Your wishlist must have a name.", comment: ""))
            updateHighlight(for: nameField, color: .red)
            return
        }
        
        let count = DataManager.shared.wishlists.filter({$0.name.lowercased() == name.lowercased()}).count
        if count > 0 && wishlist?.name.lowercased() != name.lowercased() {
            present(NSLocalizedString("Your already have a wishlist with this name. Please choose a unique name.", comment: ""))
            updateHighlight(for: nameField, color: .red)
            return
        }
        
        createWishlistButton.isHidden = true
        _ = fields.map { $0.isUserInteractionEnabled = false }
        
        var data = [String: Any]()
        if let wishlist = wishlist {
            data["id"] = wishlist.id
        }
        data["name"] = name
        data["relevantDate"] = (dateField.text?.isEmpty ?? true) ? NSNull() : DateFormatterManager.shared.mysqlDateFormatter.string(from: datePicker.date)
        data["privacySetting"] = privacy.rawValue
        data["typeSetting"] = type.rawValue
        data["userId"] = Profile.userId
        data["keywordString"] = NSNull()
        if keywordsField.tags.count > 0 {
            data["keywordString"] = "," + keywordsField.tags.map({$0.text.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()}).joined(separator: ",") + ","
        }
        
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "wishlist/save"), parameters: data) { (handler) in
            switch handler {
            case .success:
                DataManager.shared.cacheWishlists {
                    if self.wishlist != nil {
                        self.dismissView()
                    } else {
                        Analytics.logEvent("create_wishlist", parameters: [
                            "name": name as NSObject,
                            "user_id": Profile.userId as NSObject
                        ])
                        
                        var properties = [String: String]()
                        properties["id"] = "\(DataManager.shared.wishlists.first?.id ?? 0)"
                        properties["name"] = name
                        if (self.dateField.text?.isEmpty ?? true) == false {
                            properties["relevantDate"] = DateFormatterManager.shared.mysqlDateFormatter.string(from: self.datePicker.date)
                        }
                        properties["privacy"] = self.privacy.name
                        properties["type"] = self.type.rawValue
                        SEGAnalytics.shared()?.track("Wishlist Created", properties: properties)
                        
                        guard let controller = UIStoryboard(name: "AddItem", bundle: nil).instantiateInitialViewController() as? AddItemViewController else { return }
                        controller.isFirstItem = true
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            case .failure:
                self.present(NSLocalizedString("An error occurred whilst saving your wishlist. Please try again ensuring you have a good network connection.", comment: ""))
                return
            }
        }
    }
}

extension WishlistDetailsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateHighlight(for: textField, color: Config.colors.dark)
        switch textField {
        case dateField:
            datePickerValueDidChange()
        default:
            break
        }
    }
}

extension WishlistDetailsViewController: LUAutocompleteViewDataSource {
    func autocompleteView(_ autocompleteView: LUAutocompleteView, elementsFor text: String, completion: @escaping ([String]) -> Void) {
        completion(DataManager.shared.fetchKeywords(for: text))
    }
}
extension WishlistDetailsViewController: LUAutocompleteViewDelegate {
    func autocompleteView(_ autocompleteView: LUAutocompleteView, didSelect text: String) {
        keywordsField.addTag(text)
    }
}

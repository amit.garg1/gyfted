//
//  WishlistTypeViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 09/05/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class WishlistTypeViewController: BaseViewController {

    @IBOutlet weak var onboardingLabel: UILabel!
    @IBOutlet weak var onboardingSkipButton: UIButton!
    
    var isOnboarding = false
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? WishlistDetailsViewController, let type = sender as? WishlistType {
            controller.type = type
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onboardingLabel.isHidden = !isOnboarding
        onboardingSkipButton.isHidden = !isOnboarding
        navigationController?.setNavigationBarHidden(isOnboarding, animated: false)
        title = NSLocalizedString("Wishlist Type", comment: "").uppercased()
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(isOnboarding, animated: animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let currentNavigationController = navigationController, let parent = currentNavigationController.presentingViewController else { return }
        if currentNavigationController.isKind(of: RegisterNavigationController.self) {
            parent.dismiss(animated: false) {
                let navigationController = BaseNavigationController(rootViewController: self)
                navigationController.setNavigationBarHidden(true, animated: false)
                parent.present(navigationController, animated: false, completion: nil)
            }
        }
    }

    @IBAction func typeSelected(_ sender: UITapGestureRecognizer) {
        let type = WishlistType.types[sender.view?.tag ?? 0]
        performSegue(withIdentifier: "PresentWishlistDetails", sender: type)
    }
}

//
//  ShareViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 21/08/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import Branch
import FirebaseAnalytics
import MessageUI
import PercentEncoder
import ContactsUI

class ShareViewController: BaseViewController {
    
    @IBOutlet weak var whatsappContainer: UIView!
    @IBOutlet weak var emailContainer: UIView!

    let shareText = "Hey! I am using Gyfted & think you'll like it too. It's a free universal wishlist that keeps all your wishlists in one place."
    var url: URL?
    var shareUrl: URL {
        return url ?? URL(string: Config.websites.website)!
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let data = Config.sharedDefaults().object(forKey: Config.userDefaults.profile) as? Data, let profile = try? JSONDecoder().decode(Profile.self, from: data) else { return }
    
        let buo = BranchUniversalObject(canonicalIdentifier: "profile/\(profile.id)")
        buo.title = "@" + profile.username + " on Gyfted"
        buo.contentDescription = shareText
        buo.contentMetadata.customMetadata["profile"] = profile.id
        buo.publiclyIndex = true
        buo.imageUrl = profile.avatarURL?.absoluteString
        buo.canonicalIdentifier = profile.id
        
        let lp = BranchLinkProperties()
        buo.getShortUrl(with: lp) { (url, _) in
            DispatchQueue.main.async {
                guard let url = url else { return }
                if let url = URL(string: url) {
                    self.url = url
                }
            }
        }
        
        if let url = URL(string: "whatsapp://"), UIApplication.shared.canOpenURL(url) == false {
            whatsappContainer.isHidden = true
        }
        
        if MFMailComposeViewController.canSendMail() == false {
            emailContainer.isHidden = true
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismissView()
    }
    
    @IBAction func presentContacts(_ sender: Any) {
        let controller = CNContactPickerViewController()
        controller.delegate = self
        controller.predicateForEnablingContact = NSPredicate(format: "phoneNumbers.@count > 0")
        controller.displayedPropertyKeys = [CNContactGivenNameKey, CNContactPhoneNumbersKey]
        present(controller, animated: true, completion: nil)
        logAnalytics("AddFriends-InviteFromContacts")
    }
    
    @IBAction func presentCopyLink(_ sender: Any) {
        UIPasteboard.general.url = url
        logAnalytics("AddFriends-CopyLink")
        let controller = UIAlertController(title: "Link Copied", message: nil, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func presentWhatsApp(_ sender: Any) {
        let text = (shareText + " " + shareUrl.absoluteString).ped_encodeURIComponent()
        guard let url = URL(string: "whatsapp://send?text="+text) else { return }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        logAnalytics("AddFriends-WhatsApp")
    }
    
    @IBAction func presentSendEmail(_ sender: Any) {
        let text = shareText + "\n\n" + shareUrl.absoluteString
        let controller = MFMailComposeViewController()
        controller.setSubject("Gyfted: The free universal wishlist")
        controller.setMessageBody(text, isHTML: false)
        controller.mailComposeDelegate = self
        present(controller, animated: true, completion: nil)
        
        logAnalytics("AddFriends-SendEmail")
    }
    
    @IBAction func presentShare(_ sender: Any) {
        let controller = UIActivityViewController(activityItems: [shareText, shareUrl], applicationActivities: nil)
        self.present(controller, animated: true, completion: nil)
        logAnalytics("AddFriends-Share")
    }
    
    func logAnalytics(_ type: String) {
        guard let data = Config.sharedDefaults().object(forKey: Config.userDefaults.profile) as? Data, let profile = try? JSONDecoder().decode(Profile.self, from: data) else { return }
        Analytics.logEvent(AnalyticsEventShare, parameters: [
            AnalyticsParameterContentType: type,
            AnalyticsParameterItemID: "\(profile.id)",
            AnalyticsParameterItemName: "\(profile.name)"
        ])
    }
}

extension ShareViewController: MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}

extension ShareViewController: CNContactPickerDelegate {
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        let phoneNumbers = contacts.compactMap({ $0.phoneNumbers.first?.value.stringValue })
        if phoneNumbers.count == 0 {
            return
        }
        let controller = MFMessageComposeViewController()
        controller.body = shareText + "\n" + shareUrl.absoluteString
        controller.recipients = phoneNumbers
        controller.messageComposeDelegate = self
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (_) in
            self.present(controller, animated: true, completion: nil)
        }
        
    }
}

extension ShareViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        dismiss(animated: true, completion: nil)
    }
}

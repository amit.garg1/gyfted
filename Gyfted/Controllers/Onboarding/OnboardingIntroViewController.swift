//
//  OnboardingIntroViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class OnboardingIntroViewController: BaseViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    var page: Int {
        return Int((scrollView.contentOffset.x / view.bounds.size.width).rounded()) + 1
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for page in 1...4 {
            let onboardingView = OnboardingView.instance()
            onboardingView.use(page)
            stackView.addArrangedSubview(onboardingView)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.viewControllers = [self]
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func presentNextSlide(_ sender: UIButton) {
        if page == 4 {
            skipOnboarding(sender)
            return
        }
        scrollView.setContentOffset(CGPoint(x: CGFloat(page) * view.bounds.size.width, y: 0), animated: true)
    }
    
    @IBAction func skipOnboarding(_ sender: UIButton) {
        dismissView()
    }
}

extension OnboardingIntroViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pager.currentPage = page - 1
        actionButton.setTitle((page == 4 ? NSLocalizedString("Get Started", comment: "") : NSLocalizedString("Got It", comment: "")).uppercased(), for: .normal)
        UIView.animate(withDuration: 0.1) {
            self.skipButton.alpha = self.page == 4 ? 0 : 1
        }
    }
    
}

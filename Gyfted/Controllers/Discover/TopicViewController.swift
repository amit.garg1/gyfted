//
//  TopicViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 18/07/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class TopicViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var topic: Topic?
    var items = [Item]()

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? ItemViewController, let item = sender as? Item {
            controller.item = item
            controller.showPurchaseStatus = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = topic?.name.uppercased()
        collectionView.register(UINib(nibName: "WishlistCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WishlistCollectionViewCell")
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @objc func reloadData() {
        collectionView.isHidden = true
        spinner.startAnimating()
        
        guard let topic = topic else { return }
        
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "topic/\(topic.id)/"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                if let data = data, let topic = try? JSONDecoder().decode(Topic.self, from: data) {
                    self.topic = topic
                    self.items = topic.items
                }
            case .failure(let error):
                NSLog("Error: \(error)")
                self.items = []
            }
            
            self.collectionView.reloadData()
            self.collectionView.setContentOffset(.zero, animated: false)
            self.collectionView.isHidden = false
            self.spinner.stopAnimating()
        }
    }
    
}

extension TopicViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    // swiftlint:disable force_cast
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WishlistCollectionViewCell", for: indexPath) as! WishlistCollectionViewCell
        cell.use(items[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((UIScreen.main.bounds.size.width - 50 - 25) / 2.0).rounded(.down)
        let height = (width * (248.0/151.0)).rounded()
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "PresentItem", sender: items[indexPath.row])
    }
}

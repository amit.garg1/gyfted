//
//  DiscoverViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 11/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class DiscoverViewController: BaseViewController {

    enum DiscoverTab: Int {
        case windowShop
        case giftGuides
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noFollowLabel: UILabel!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var notificationsBadge: UILabel!
    
    var topics = [Topic]()
    var shops = [Shop]()
    
    var selectedTab: DiscoverTab = .windowShop
    var isLoading = false
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? TopicViewController {
            controller.topic = sender as? Topic
        }
        if let controller = segue.destination as? ItemViewController {
            controller.item = sender as? Item
            controller.showPurchaseStatus = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Gyfted", comment: "").uppercased()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "NavBarAddFriends"), style: .plain, target: self, action: #selector(presentShare))
        
        navigationItem.rightBarButtonItem?.customView?.widthAnchor.constraint(equalToConstant: 65).isActive = true
        navigationItem.rightBarButtonItem?.customView?.heightAnchor.constraint(equalToConstant: 44).isActive = true
        NotificationCenter.default.addObserver(self, selector: #selector(updateBadgeFromNotification), name: .notificationsDidChange, object: nil)
        updateBadge(UIApplication.shared.applicationIconBadgeNumber)
        
        collectionView.register(UINib(nibName: "ShopCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShopCollectionViewCell")
        
        tableView.register(UINib(nibName: "TopicTableViewCell", bundle: nil), forCellReuseIdentifier: "TopicTableViewCell")
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 20))
        
        reloadData()
    }
    
    @IBAction func didChangeSegment(_ sender: UISegmentedControl) {
        guard let tab = DiscoverTab(rawValue: sender.selectedSegmentIndex) else { return }
        selectedTab = tab
        renderView()
    }
    
    func renderView() {
        collectionView.isHidden = selectedTab == .giftGuides
        tableView.isHidden = selectedTab == .windowShop
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    func reloadData() {
        isLoading = true
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "shop"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                guard let data = data else { return }
                do {
                    self.shops = try JSONDecoder().decode([Shop].self, from: data)
                    self.collectionView.reloadData()
                } catch let error {
                    NSLog("ERROR: \(error)")
                }
                
                APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "topic/discover"), parameters: nil) { (handler) in
                    self.isLoading = false
                    switch handler {
                    case .success(let data):
                        guard let data = data else { return }
                        do {
                            self.topics = try JSONDecoder().decode([Topic].self, from: data)
                            self.renderView()
                        } catch let error {
                            NSLog("ERROR: \(error)")
                        }
                    case .failure(let error):
                        print(error)
                    }
                    self.loadingView.isHidden = true
                }
            case .failure(let error):
                print(error)
            }
        }
    }
        
    @IBAction func presentSearch(_ sender: Any) {
        guard let controller = UIStoryboard(name: "Search", bundle: nil).instantiateInitialViewController() as? SearchViewController else { return }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func presentNotifications(_ sender: Any) {
        guard let controller = UIStoryboard(name: "Notifications", bundle: nil).instantiateInitialViewController() as? NotificationsViewController else { return }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func updateBadgeFromNotification() {
        updateBadge()
    }
    
    func updateBadge(_ number: Int? = nil) {
        let count = number ?? DataManager.shared.unreadNotificationCount
        notificationsBadge.text = "\(count)"
        notificationsBadge.superview?.isHidden = count == 0
    }
}

extension DiscoverViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topics.count
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopicTableViewCell", for: indexPath) as! TopicTableViewCell
        cell.delegate = self
        cell.use(topics[indexPath.row], indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "PresentTopic", sender: topics[indexPath.row])
    }
}

extension DiscoverViewController: TopicTableViewCellDelegate {
    func toggleFollow(at indexPath: IndexPath) {
        let topic = topics[indexPath.row]
        if topic.isFollowed {
            DataManager.shared.unfollowTopic(with: topic.id)
        } else {
            DataManager.shared.followTopic(with: topic.id)
        }
        tableView.reloadData()
    }
    
    func didSelect(item: Item, in topic: Topic) {
        performSegue(withIdentifier: "PresentItem", sender: item)
    }
}

extension DiscoverViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shops.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopCollectionViewCell", for: indexPath) as? ShopCollectionViewCell else {
            fatalError("Couldn't load ShopCollectionViewCell")
        }
        cell.use(shops[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((UIScreen.main.bounds.size.width - (16 + 8 + 16)) / 2).rounded(.down)
        let height = (width / (7.0/3.0)).rounded()
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let controller = UIStoryboard(name: "AddItem", bundle: nil).instantiateViewController(withIdentifier: "AddItemBrowserViewController") as? AddItemBrowserViewController else { return }
        controller.shop = shops[indexPath.row]
        let navigationController = BaseNavigationController(rootViewController: controller)
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.modalPresentationStyle = .fullScreen
        present(navigationController, animated: true, completion: nil)
    }
}

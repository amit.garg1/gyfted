//
//  HomeViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 11/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseAuth

enum HomeTab: Int {
    case home
    case following
    case friends
}

class HomeViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var homeScrollView: UIScrollView!
    @IBOutlet weak var homeStackView: UIStackView!
    @IBOutlet weak var noPostsLabel: UILabel!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var notificationsBadge: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var shopCollectionView: UICollectionView!
    
    private let refreshControl = UIRefreshControl()
    
    var homeFeed: HomeFeedResponse?
    var posts = [FeedPost]()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? WishlistViewController, let post = sender as? FeedPost {
            controller.id = post.wishlistId
            controller.name = post.wishlistName
        }
        if let controller = segue.destination as? ItemViewController {
            if let post = sender as? FeedPost {
                controller.id = post.itemId
                controller.name = post.metadata
            }
            if let item = sender as? Item {
                controller.item = item
            }
        }
        if let controller = segue.destination as? WishlistsViewController, let id = sender as? String {
            controller.id = id
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("Gyfted", comment: "").uppercased()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "NavBarAddFriends"), style: .plain, target: self, action: #selector(presentShare))
        
        navigationItem.rightBarButtonItem?.customView?.widthAnchor.constraint(equalToConstant: 65).isActive = true
        navigationItem.rightBarButtonItem?.customView?.heightAnchor.constraint(equalToConstant: 44).isActive = true
        NotificationCenter.default.addObserver(self, selector: #selector(updateBadgeFromNotification), name: .notificationsDidChange, object: nil)
        updateBadge(UIApplication.shared.applicationIconBadgeNumber)
        
        tableView.register(UINib(nibName: "FeedTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedTableViewCell")
        tableView.tableFooterView = UIView()
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        refreshControl.tintColor = Config.colors.tint
        tableView.refreshControl = refreshControl
        
        shopCollectionView.register(UINib(nibName: "HomeShopCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeShopCollectionViewCell")
        
        view.bringSubviewToFront(loadingView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadData()
    }
    
    @objc func reloadData() {
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "fetchhomefeeds"), parameters: nil) { (handler) in
            self.refreshControl.endRefreshing()
            switch handler {
            case .success(let data):
                guard let data = data else { return }
                do {
                    let feed = try JSONDecoder().decode(HomeFeedResponse.self, from: data)
                    self.homeFeed = feed
                    self.updateFeed(scrollToTop: false)
                } catch let error {
                    print(error)
                    self.loadingView.isHidden = true
                }
            case .failure(let error):
                NSLog("ERROR: \(error)")
                if self.homeFeed == nil {
                    let controller = UIAlertController(title: "Unable to connect", message: "Please try again ensuring you have a good connection to the internet.", preferredStyle: .alert)
                    controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                        self.reloadData()
                    }))
                    self.present(controller, animated: true, completion: nil)
                    return
                }
            }
            self.loadingView.isHidden = true
        }
    }
    
    @IBAction func segmentedControlDidChange() {
        updateFeed()
    }
    
    func updateFeed(scrollToTop: Bool = true) {
        guard let feed = homeFeed else { return }
        let tab = HomeTab(rawValue: segmentedControl.selectedSegmentIndex) ?? .friends
        
        tableView.isHidden = true
        homeScrollView.isHidden = true
        switch tab {
        case .home:
            posts = []
            homeScrollView.isHidden = false
            shopCollectionView.reloadData()
            
            while homeStackView.arrangedSubviews.count > 1 {
                if let view = homeStackView.arrangedSubviews.last {
                    homeStackView.removeArrangedSubview(view)
                    view.removeFromSuperview()
                }
            }
            
            for collection in feed.collections where collection.items.count > 0 {
                let carousel = HomeCollectionCarouselView.instance()
                carousel.delegate = self
                homeStackView.addArrangedSubview(carousel)
                carousel.use(collection)
            }
            
        case .following:
            posts = feed.following
            tableView.isHidden = false
        case .friends:
            posts = feed.friends
            tableView.isHidden = false
        }
        
        if scrollToTop {
            tableView.setContentOffset(.zero, animated: false)
            tableView.reloadData()
            tableView.layoutIfNeeded()
            tableView.setContentOffset(.zero, animated: false)
            
            homeScrollView.contentOffset = .zero
        } else {
            tableView.reloadData()
        }

        switch tab {
        case .home:
            break
        case .following, .friends:
            tableView.isHidden = posts.count == 0
            noPostsLabel.isHidden = !tableView.isHidden
        }
    }
    
    @IBAction func presentNotifications(_ sender: Any) {
        guard let controller = UIStoryboard(name: "Notifications", bundle: nil).instantiateInitialViewController() as? NotificationsViewController else { return }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func presentSearch(_ sender: Any) {
        guard let controller = UIStoryboard(name: "Search", bundle: nil).instantiateInitialViewController() as? SearchViewController else { return }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func updateBadgeFromNotification() {
        updateBadge()
    }
    
    func updateBadge(_ number: Int? = nil) {
        let count = number ?? DataManager.shared.unreadNotificationCount
        notificationsBadge.text = "\(count)"
        notificationsBadge.superview?.isHidden = count == 0
    }
    
    @IBAction func presentStores(_ sender: Any) {
        (navigationController?.parent as? MainViewController)?.select(tab: .discover)
    }
    
    func presentStore(_ shop: Shop) {
        guard let controller = UIStoryboard(name: "AddItem", bundle: nil).instantiateViewController(withIdentifier: "AddItemBrowserViewController") as? AddItemBrowserViewController else { return }
        controller.shop = shop
        let navigationController = BaseNavigationController(rootViewController: controller)
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.modalPresentationStyle = .fullScreen
        present(navigationController, animated: true, completion: nil)
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell", for: indexPath) as! FeedTableViewCell
        cell.use(posts[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = posts[indexPath.row]
        switch post.type {
        case .addedItem, .addedWishlist:
            performSegue(withIdentifier: "PresentWishlist", sender: post)
        case .purchasedItem:
            performSegue(withIdentifier: "PresentItem", sender: post)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let post = posts[indexPath.row]
        return post.userId == Profile.userId
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let post = posts[indexPath.row]
        if let feed = homeFeed {
            var index: Int
 
            index = 0
            for object in feed.following {
                if object.id == post.id {
                    feed.following.remove(at: index)
                    break
                }
                index += 1
            }
            
            index = 0
            for object in feed.friends {
                if object.id == post.id {
                    feed.friends.remove(at: index)
                    break
                }
                index += 1
            }
        }
        
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "fullfatfeed/\(post.id)/remove"), parameters: nil, onCompletion: nil)
        
        posts.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
}

extension HomeViewController: FeedTableViewCellDelegate {
    func presentProfile(with id: String) {
        performSegue(withIdentifier: "PresentProfile", sender: id)
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == shopCollectionView {
            return homeFeed?.shops.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == shopCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeShopCollectionViewCell", for: indexPath) as! HomeShopCollectionViewCell
            if let shop = homeFeed?.shops[indexPath.row] {
                cell.use(shop)
            }
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == shopCollectionView {
            if let shop = homeFeed?.shops[indexPath.row] {
                presentStore(shop)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == shopCollectionView {
            return CGSize(width: 74, height: 92)
        } else {
            return .zero
        }
    }
}

extension HomeViewController: HomeCollectionCarouselViewDelegate {
    func carouselViewPresentItem(_ item: Item) {
        performSegue(withIdentifier: "PresentItem", sender: item)
    }
}

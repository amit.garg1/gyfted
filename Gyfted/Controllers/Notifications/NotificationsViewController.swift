//
//  NotificationsViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 11/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import OneSignal
import FirebaseAnalytics
import Analytics

class NotificationsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noNotificationsView: UIView!
    
    var notifications = [GyftedNotification]()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? WishlistViewController, let id = sender as? Int {
            controller.id = id
        }
        if let controller = segue.destination as? WishlistsViewController, let id = sender as? String {
            controller.id = id
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Notifications", comment: "").uppercased()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "NavBarMore"), style: .plain, target: self, action: #selector(presentActions))
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .notificationsDidChange, object: nil)
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.tableFooterView = UIView()
        reloadData()
    }
    
    @objc func reloadData() {
        notifications = DataManager.shared.notifications
        tableView.reloadData()
        tableView.isHidden = notifications.count == 0
        noNotificationsView.isHidden = !tableView.isHidden
    }
    
    @IBAction func presentActions() {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: NSLocalizedString("Mark all as read", comment: ""), style: .default, handler: { (_) in
            self.markAllAsRead()
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    func delete(_ notification: GyftedNotification) {
        if let index = DataManager.shared.notifications.firstIndex(of: notification) {
            DataManager.shared.notifications.remove(at: index)
        }
        NotificationCenter.default.post(name: .notificationsDidChange, object: nil)
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "notifications/\(notification.id)/delete"), parameters: nil, onCompletion: nil)
    }
    
    func markAsRead(_ notification: GyftedNotification) {
        if notification.isRead {
            return
        }
        _ = DataManager.shared.notifications.filter({$0.id == notification.id}).map({$0.isRead = true})
        NotificationCenter.default.post(name: .notificationsDidChange, object: nil)
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "notifications/\(notification.id)/markAsRead"), parameters: nil, onCompletion: nil)
    }
    
    func markAllAsRead() {
        _ = DataManager.shared.notifications.map({$0.isRead = true})
        NotificationCenter.default.post(name: .notificationsDidChange, object: nil)
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "notifications/markAllAsRead"), parameters: nil, onCompletion: nil)
    }
    
    @IBAction func presentInvite(_ sender: Any) {
        guard let main = navigationController?.parent as? MainViewController else { return }
        main.presentShare()
    }
}

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        cell.use(notifications[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notification = notifications[indexPath.row]
        switch notification.notificationType {
        case .addressRequest,
             .friendRequest:
            performSegue(withIdentifier: "PresentProfile", sender: notification.profileId)
            return
        case .myEvent,
             .friendEvent:
            markAsRead(notification)
            performSegue(withIdentifier: "PresentWishlist", sender: notification.wishlistId)
        case .purchase,
             .addressShared:
            markAsRead(notification)
            performSegue(withIdentifier: "PresentProfile", sender: notification.profileId)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        delete(notifications[indexPath.row])
    }
}

extension NotificationsViewController: NotificationTableViewCellDelegate {
    func requestAccepted(_ notification: GyftedNotification) {
        
        if notification.notificationType == .friendRequest {
            let controller = UIAlertController(title: "Send Friend Request", message: nil, preferredStyle: .actionSheet)
            controller.addAction(UIAlertAction(title: "Friend", style: .default, handler: { (_) in
                self.acceptFriendRequest(notification, isClose: false)
            }))
            controller.addAction(UIAlertAction(title: "Close Friend", style: .default, handler: { (_) in
                self.acceptFriendRequest(notification, isClose: true)
            }))
            controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(controller, animated: true, completion: nil)
            return
        }
        
        _ = DataManager.shared.notifications.filter({$0.id == notification.id}).map({
            $0.requestStatus = NotificationRequestStatus.accepted.rawValue
            $0.isRead = true
        })
        NotificationCenter.default.post(name: .notificationsDidChange, object: nil)
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "request/\(notification.id)/accept"), parameters: nil, onCompletion: nil)
        
        if notification.notificationType == .addressRequest {
            Analytics.logEvent("accepted_address_request", parameters: [
                "user_id": Profile.userId as NSObject,
                "friend_id": notification.profileId as NSObject
            ])
        }   
    }
    
    private func acceptFriendRequest(_ notification: GyftedNotification, isClose: Bool) {
        
        _ = DataManager.shared.notifications.filter({$0.id == notification.id}).map({
            $0.requestStatus = NotificationRequestStatus.accepted.rawValue
            $0.isRead = true
        })
        
        let parameters: [String: Any] = ["close": isClose ? 1 : 0]
        NotificationCenter.default.post(name: .notificationsDidChange, object: nil)
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "request/\(notification.id)/accept"), parameters: parameters, onCompletion: nil)
        
        Analytics.logEvent("accepted_friend_request", parameters: [
            "user_id": Profile.userId as NSObject,
            "friend_id": notification.profileId as NSObject
        ])
        SEGAnalytics.shared()?.track("Added Friend", properties: ["id": "\(notification.profileId)"])
    }
    
    func requestRejected(_ notification: GyftedNotification) {
        if let index = DataManager.shared.notifications.firstIndex(of: notification) {
            DataManager.shared.notifications.remove(at: index)
        }
        NotificationCenter.default.post(name: .notificationsDidChange, object: nil)
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "request/\(notification.id)/reject"), parameters: nil, onCompletion: nil)
    }
    
    func presentProfile(with id: String) {
        performSegue(withIdentifier: "PresentProfile", sender: id)
    }
}

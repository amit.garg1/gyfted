//
//  SearchViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/08/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class SearchViewController: BaseViewController {
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noResultsLabel: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var searchTimer: Timer?
    var isSearching = false
    
    var profiles = [SearchProfile]()
    var items = [Item]()
    var term = ""
    
    weak var initialInteractivePopGestureRecognizerDelegate: UIGestureRecognizerDelegate?

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? ItemViewController, let item = sender as? Item {
            controller.item = item
            controller.showPurchaseStatus = false
        }
        if let controller = segue.destination as? WishlistsViewController, let id = sender as? String {
            controller.id = id
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialInteractivePopGestureRecognizerDelegate = navigationController?.interactivePopGestureRecognizer?.delegate
        tableView.register(UINib(nibName: "SearchProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchProfileTableViewCell")
        tableView.tableFooterView = UIView()
        collectionView.register(UINib(nibName: "WishlistCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WishlistCollectionViewCell")
        loadingView.isHidden = true
        noResultsLabel.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if (searchField.text ?? "").isEmpty {
            searchField.becomeFirstResponder()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = initialInteractivePopGestureRecognizerDelegate
        searchField.resignFirstResponder()
    }
    
    func performSearch(_ text: String) {
        
        if text == "" {
            items = []
            profiles = []
            term = ""
            segmentedControlDidChange()
            return
        }
        
        tableView.isHidden = true
        noResultsLabel.isHidden = true
        loadingView.isHidden = false
        
        term = text
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "search"), parameters: ["query": text]) { (handler) in
            if self.term != text {
                return
            }
            switch handler {
            case .success(let data):
                if let data = data, let response = try? JSONDecoder().decode(SearchResponse.self, from: data) {
                    self.profiles = response.profiles
                    self.items = response.items
                }
            case .failure:
                break
            }
            self.isSearching = false
            self.segmentedControlDidChange()
        }
    }
    
    @IBAction func segmentedControlDidChange() {
        if isSearching {
            return
        }
        
        loadingView.isHidden = true
        
        if segmentedControl.selectedSegmentIndex == 0 {
            tableView.isHidden = true
            collectionView.isHidden = items.count == 0
            noResultsLabel.isHidden = items.count > 0
            collectionView.reloadData()
            collectionView.setContentOffset(.zero, animated: false)
        }
        
        if segmentedControl.selectedSegmentIndex == 1 {
            collectionView.isHidden = true
            tableView.isHidden = profiles.count == 0
            noResultsLabel.isHidden = profiles.count > 0
            tableView.reloadData()
        }
        
        if term == "" {
            noResultsLabel.isHidden = true
        }
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profiles.count
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchProfileTableViewCell", for: indexPath) as! SearchProfileTableViewCell
        cell.use(profiles[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profile = profiles[indexPath.row]
        performSegue(withIdentifier: "PresentProfile", sender: profile.id)
    }
}

extension SearchViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    // swiftlint:disable force_cast
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WishlistCollectionViewCell", for: indexPath) as! WishlistCollectionViewCell
        cell.use(items[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((UIScreen.main.bounds.size.width - 50 - 25) / 2.0).rounded(.down)
        let height = (width * (248.0/151.0)).rounded()
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "PresentItem", sender: items[indexPath.row])
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        searchTimer?.invalidate()
        searchTimer = Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false, block: { (_) in
            self.performSearch(text)
        })
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchTimer?.invalidate()
        if let text = textField.text, term != text {
            self.performSearch(text)
        }
        return true
    }
}

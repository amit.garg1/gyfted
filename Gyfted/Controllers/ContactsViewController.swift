//
//  ContactsViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 15/11/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import SafariServices
import Contacts
import MessageUI
import Branch
import FirebaseAnalytics
import Analytics

// swiftlint:disable file_length
class ContactsViewController: BaseViewController {
    
    @IBOutlet weak var notificationsBadge: UILabel!
    @IBOutlet weak var promptView: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingSpinner: UIActivityIndicatorView!
    
    var isLoading = false
    
    var allContacts = [Contact]()
    var gyftedContacts = [Contact]()
    var otherContacts = [Contact]()
    var profiles = [ContactProfile]()
    
    let shareText = "Hey! I am using Gyfted & think you'll like it too. It's a free universal wishlist that keeps all your wishlists in one place."
    var shareUrl: URL {
        return url ?? URL(string: Config.websites.website)!
    }
    var url: URL?
    
    enum ContactShareMethod {
        case messages
        case mail
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? WishlistsViewController, let id = sender as? String {
            controller.id = id
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactTableViewCell")
        tableView.register(UINib(nibName: "TextTableViewCell", bundle: nil), forCellReuseIdentifier: "TextTableViewCell")
        
        guard let data = Config.sharedDefaults().object(forKey: Config.userDefaults.profile) as? Data, let profile = try? JSONDecoder().decode(Profile.self, from: data) else { return }
        
        let buo = BranchUniversalObject(canonicalIdentifier: "profile/\(profile.id)")
        buo.title = "@" + profile.username + " on Gyfted"
        buo.contentDescription = shareText
        buo.contentMetadata.customMetadata["profile"] = profile.id
        buo.publiclyIndex = true
        buo.imageUrl = profile.avatarURL?.absoluteString
        buo.canonicalIdentifier = profile.id
        
        let lp = BranchLinkProperties()
        buo.getShortUrl(with: lp) { (url, _) in
            DispatchQueue.main.async {
                guard let url = url else { return }
                if let url = URL(string: url) {
                    self.url = url
                }
            }
        }
        
        title = NSLocalizedString("Gyfted", comment: "").uppercased()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "NavBarAddFriends"), style: .plain, target: self, action: #selector(presentShare))
        
        navigationItem.rightBarButtonItem?.customView?.widthAnchor.constraint(equalToConstant: 65).isActive = true
        navigationItem.rightBarButtonItem?.customView?.heightAnchor.constraint(equalToConstant: 44).isActive = true
        NotificationCenter.default.addObserver(self, selector: #selector(updateBadgeFromNotification), name: .notificationsDidChange, object: nil)
        updateBadge(UIApplication.shared.applicationIconBadgeNumber)
        
        
        
        reloadData(withLoadingIndicator: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadData(withLoadingIndicator: false)
    }
    
    @IBAction func presentNotifications(_ sender: Any) {
        guard let controller = UIStoryboard(name: "Notifications", bundle: nil).instantiateInitialViewController() as? NotificationsViewController else { return }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func presentSearch(_ sender: Any) {
        guard let controller = UIStoryboard(name: "Search", bundle: nil).instantiateInitialViewController() as? SearchViewController else { return }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func updateBadgeFromNotification() {
        updateBadge()
    }
    
    func updateBadge(_ number: Int? = nil) {
        let count = number ?? DataManager.shared.unreadNotificationCount
        notificationsBadge.text = "\(count)"
        notificationsBadge.superview?.isHidden = count == 0
    }
    
    @IBAction func presentPrivacyPolicy(_ sender: Any) {
        guard let url = URL(string: Config.websites.privacy) else { return }
        let controller = SFSafariViewController(url: url)
        present(controller, animated: true, completion: nil)
    }
    
    fileprivate func reloadData(withLoadingIndicator showLoadingIndicator: Bool = false) {

        if showLoadingIndicator {
            loadingSpinner.startAnimating()
            promptView.isHidden = true
            tableView.isHidden = true
        }
        
        let authorisationState = CNContactStore.authorizationStatus(for: .contacts)
        if authorisationState != .authorized {
            renderUI()
            return
        }
        
        if isLoading {
            return
        }
        isLoading = true
        
        allContacts = fetchContacts()
        profiles = []
        
        var terms = [String: String]()
        for contact in allContacts {
            terms[contact.id] = (contact.phoneNumbers.map({$0.filter("0123456789".contains)}) + contact.emailAddresses).joined(separator: ",")
        }

        var string = ""
        if let termsData = try? JSONEncoder().encode(terms) {
            string = String(data: termsData, encoding: .utf8) ?? ""
        }
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "contacts/fetch"), parameters: ["terms": string]) { (handler) in
            switch handler {
            case .success(let data):
                if let data = data {
                    self.profiles = (try? JSONDecoder().decode([ContactProfile].self, from: data)) ?? []
                }
            case .failure(let error):
                print(error)
            }
            
            self.filterContacts()
            self.renderUI()
            self.isLoading = false
        }
    }
    
    fileprivate func renderUI() {
        loadingSpinner.stopAnimating()
        promptView.isHidden = true
        tableView.isHidden = true
        
        let authorisationState = CNContactStore.authorizationStatus(for: .contacts)
        if authorisationState != .authorized {
            promptView.isHidden = false
            return
        }
        
        tableView.isHidden = false
        tableView.reloadData()
    }
    
    fileprivate func filterContacts() {
        
        gyftedContacts = []
        otherContacts = []
        
        var keyedProfiles = [String: ContactProfile]()
        _ = profiles.map({ keyedProfiles[$0.localId] = $0 })
        
        for contact in allContacts {
            if let profile = keyedProfiles[contact.id] {
                contact.profile = profile
                gyftedContacts.append(contact)
            } else {
                otherContacts.append(contact)
            }
        }
        
    }
    
    fileprivate func fetchContacts() -> [Contact] {
        let store = CNContactStore()
        let containers: [CNContainer] = (try? store.containers(matching: nil)) ?? []
        let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName)] + ([CNContactPhoneNumbersKey, CNContactEmailAddressesKey, CNContactThumbnailImageDataKey] as [CNKeyDescriptor])
        
        var localContacts = [CNContact]()
        for container in containers {
            let predicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            do {
                let results = try store.unifiedContacts(matching: predicate, keysToFetch: keys)
                localContacts.append(contentsOf: results)
            } catch {
                print("Error fetching results for container")
            }
        }
        
        var uniqueKeys = [String]()
        
        var contacts = [Contact]()
        for localContact in localContacts {
            guard let name = CNContactFormatter.string(from: localContact, style: .fullName) else { continue }
            
            let contact = Contact()
            contact.id = localContact.identifier
            contact.name = name
            contact.imageData = localContact.thumbnailImageData
            contact.emailAddresses = localContact.emailAddresses.map { $0.value as String }
            contact.phoneNumbers = localContact.phoneNumbers.map { $0.value.stringValue }
            
            if contact.emailAddresses.count == 0 && contact.phoneNumbers.count == 0 {
                continue
            }
            
            let uniqueKey = contact.uniqueKey
            if uniqueKeys.contains(uniqueKey) {
                continue
            }
            uniqueKeys.append(uniqueKey)
            contacts.append(contact)
        }
        
        contacts.sort(by: { $0.name < $1.name })
        return contacts
    }
    
    @IBAction func requestContactPermissions(_ sender: Any) {
        
        CNContactStore().requestAccess(for: .contacts) { (access, _) in
            DispatchQueue.main.async {
                if access == false {
                    let controller = UIAlertController(title: NSLocalizedString("Contacts Access", comment: ""), message: NSLocalizedString("Gyfted needs access to your contacts in order to find your friends and family. Please enable access to your contacts within Settings to continue.", comment: ""), preferredStyle: .alert)
                    controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
                    controller.addAction(UIAlertAction(title: NSLocalizedString("Open Settings", comment: ""), style: .default, handler: { (_) in
                        guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }))
                    self.present(controller, animated: true, completion: nil)
                    return
                }
                
                self.reloadData(withLoadingIndicator: true)
            }
        }
    }
    
    func presentActionSheet(for contact: Contact) {
        if contact.emailAddresses.count == 1 && contact.phoneNumbers.count == 0 {
            share(contact.emailAddresses[0], by: .mail)
            return
        }
        
        if contact.phoneNumbers.count == 1 && contact.emailAddresses.count == 0 {
            share(contact.phoneNumbers[0], by: .messages)
            return
        }
        
        let controller = UIAlertController(title: "Invite to Gyfted", message: nil, preferredStyle: .actionSheet)
        for string in contact.phoneNumbers {
            controller.addAction(UIAlertAction(title: string, style: .default, handler: { (_) in
                self.share(string, by: .messages)
            }))
        }
        for string in contact.emailAddresses {
            controller.addAction(UIAlertAction(title: string, style: .default, handler: { (_) in
                self.share(string, by: .mail)
            }))
        }
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(controller, animated: true)
    }
    
    func share(_ value: String, by method: ContactShareMethod) {
        switch method {
        case .messages:
            let controller = MFMessageComposeViewController()
            controller.body = shareText + "\n" + shareUrl.absoluteString
            controller.recipients = [value]
            controller.messageComposeDelegate = self
            present(controller, animated: true, completion: nil)
            logAnalytics("Contact-SendMessage")
        case .mail:
            let text = shareText + "\n\n" + shareUrl.absoluteString
            let controller = MFMailComposeViewController()
            controller.setToRecipients([value])
            controller.setSubject("Gyfted: The free universal wishlist")
            controller.setMessageBody(text, isHTML: false)
            controller.mailComposeDelegate = self
            present(controller, animated: true, completion: nil)
            logAnalytics("Contact-SendEmail")
        }
    }
    
    func logAnalytics(_ type: String) {
        guard let data = Config.sharedDefaults().object(forKey: Config.userDefaults.profile) as? Data, let profile = try? JSONDecoder().decode(Profile.self, from: data) else { return }
        Analytics.logEvent(AnalyticsEventShare, parameters: [
            AnalyticsParameterContentType: type,
            AnalyticsParameterItemID: "\(profile.id)",
            AnalyticsParameterItemName: "\(profile.name)"
        ])
    }
    
}

extension ContactsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return otherContacts.count > 0 ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 && gyftedContacts.count == 0 {
            return 1
        }
        return contacts(for: section).count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return NSLocalizedString("Friends on Gyfted", comment: "")
        } else {
            return NSLocalizedString("Invite to Gyfted", comment: "")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && gyftedContacts.count == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "TextTableViewCell", for: indexPath)
        }
        return tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let cell = cell as? TextTableViewCell {
            cell.use("None of your contacts are on Gyfted yet but you can invite them below. You're likely to get 1 gift from your wishlist purchased for every 5 of your contacts on the app, according to our research.")
            return
        }
        
        guard let cell = cell as? ContactTableViewCell else { return }
        cell.delegate = self
        cell.use(contacts(for: indexPath.section)[indexPath.row], indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && gyftedContacts.count == 0 {
            return
        }
        DispatchQueue.main.async {
            let contact = self.contacts(for: indexPath.section)[indexPath.row]
            if let profile = contact.profile {
                self.performSegue(withIdentifier: "PresentProfile", sender: profile.id)
            } else {
                self.presentActionSheet(for: contact)
            }
        }
    }
    
    func contacts(for section: Int) -> [Contact] {
        return section == 0 ? gyftedContacts : otherContacts
    }
 
}

extension ContactsViewController: ContactTableViewCellDelegate {
    func contactCellButtonWasTapped(indexPath: IndexPath) {
        let contact = contacts(for: indexPath.section)[indexPath.row]
        guard let profile = contact.profile, profile.isFriend == false, profile.isRequestSent == false else {
            tableView(tableView, didSelectRowAt: indexPath)
            return
        }
        
        let controller = UIAlertController(title: "Send Friend Request", message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "Friend", style: .default, handler: { (_) in
            self.performSendFriendRequest(indexPath: indexPath, isClose: false)
        }))
        controller.addAction(UIAlertAction(title: "Close Friend", style: .default, handler: { (_) in
            self.performSendFriendRequest(indexPath: indexPath, isClose: true)
        }))
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    private func performSendFriendRequest(indexPath: IndexPath, isClose: Bool) {
        let contact = contacts(for: indexPath.section)[indexPath.row]
        guard let profile = contact.profile, profile.isFriend == false, profile.isRequestSent == false else { return }
        
        let parameters: [String: Any] = ["close": isClose ? 1 : 0]
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/\(profile.id)/sendFriendRequest"), parameters: parameters, onCompletion: nil)
        contact.profile?.isRequestSent = true
        tableView.reloadData()
        Analytics.logEvent("send_friend_request", parameters: [
            "user_id": Profile.userId as NSObject,
            "friend_id": profile.id as NSObject
        ])
        SEGAnalytics.shared()?.track("Added Friend", properties: ["id": "\(profile.id)"])
    }
}

extension ContactsViewController: MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}

extension ContactsViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        dismiss(animated: true, completion: nil)
    }
}

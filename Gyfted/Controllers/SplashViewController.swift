//
//  SplashViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 03/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseAuth

class SplashViewController: BaseViewController {
    
    var useDarkStatusBar = false
    var initialLoadComplete = false
    var presentedFromOnboarding = false
    
    deinit {
        NSLog("DEINIT")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return useDarkStatusBar ? .default : .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if initialLoadComplete {
            followPath()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if initialLoadComplete == false {
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (_) in
                self.followPath()
            }
        }
    }
    
    func followPath() {
        // If facebook user is signed in but no cached profile, then sign them out as didn't go through registration flow
        if let provider = Auth.auth().currentUser?.providerData.first?.providerID, provider == FacebookAuthProviderID, Config.sharedDefaults().object(forKey: Config.userDefaults.profile) == nil, !presentedFromOnboarding {
            try? Auth.auth().signOut()
        }
        
        // If apple user is signed in but no cached profile, then sign them out as didn't go through registration flow
        if let provider = Auth.auth().currentUser?.providerData.first?.providerID, provider == "apple.com", Config.sharedDefaults().object(forKey: Config.userDefaults.profile) == nil, !presentedFromOnboarding {
            try? Auth.auth().signOut()
        }
        
        if Profile.isLoggedIn == false {
            performSegue(withIdentifier: "PresentRegister", sender: nil)
            initialLoadComplete = true
            presentedFromOnboarding = true
            return
        }
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController, let window = UIApplication.shared.keyWindow else { return }
        
        controller.presentedFromOnboarding = presentedFromOnboarding

        initialLoadComplete = true
        presentedFromOnboarding = false
        
        useDarkStatusBar = true
        setNeedsStatusBarAppearanceUpdate()
        window.rootViewController = controller
    }

}

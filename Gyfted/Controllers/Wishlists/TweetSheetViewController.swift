//
//  TweetSheetViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 03/02/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit
import Swifter

class TweetSheetViewController: BaseViewController {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var charactersLabel: UILabel!

    var text: String?
    var url: URL?
    var imageUrl: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Tweet Wishlist"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .plain, target: self, action: #selector(sendTweet))
        
        textView.text = text
        updateCharactersLabel()
        imageView.sd_setImage(with: imageUrl, completed: nil)
        imageView.layer.borderColor = Config.colors.light.cgColor
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 4
        
        textView.becomeFirstResponder()
    }
    
    @objc func sendTweet() {
        
        guard let token = Config.sharedDefaults().string(forKey: Config.userDefaults.twitterToken), let secret = Config.sharedDefaults().string(forKey: Config.userDefaults.twitterSecret) else { return }
        let text = textView.text ?? ""
        guard text.count <= 280 else { return }
        
        var components = [String]()
        if text.isEmpty == false {
            components.append(text)
        }
        if let url = url {
            components.append(url.absoluteString)
        }
        
        let swifter = Swifter(consumerKey: Config.credentials.twitterConsumerKey, consumerSecret: Config.credentials.twitterConsumerSecret, oauthToken: token, oauthTokenSecret: secret)
        swifter.postTweet(status: components.joined(separator: " "))

        dismissView()
    }
    
    func updateCharactersLabel() {
        let text = textView.text ?? ""
        charactersLabel.text = "\(text.count) / 280"
        charactersLabel.textColor = text.count > 280 ? UIColor.red : UIColor.lightGray
        charactersLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 10, weight: .medium)
    }
    
}

extension TweetSheetViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        updateCharactersLabel()
    }
    
}

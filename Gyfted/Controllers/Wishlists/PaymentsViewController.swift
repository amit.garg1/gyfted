//
//  PaymentsViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 04/02/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit

class PaymentsViewController: BaseViewController {

    @IBOutlet weak var paypalField: UITextField!
    @IBOutlet weak var venmoField: UITextField!
    
    var profile: Profile?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fields = [paypalField, venmoField]
        _ = fields.map {
            $0.addDismissToolbar()
            $0.delegate = self
        }
        setupKeyboardNotifications()
        
        if let profile = profile {
            if profile.paypal.isEmpty == false {
                paypalField.text = "paypal.me/" + profile.paypal
            }
            if profile.venmo.isEmpty == false {
                venmoField.text = "@" + profile.venmo
            }
        }
    }

    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any) {
        let paypal = paypalField.text?.replacingOccurrences(of: paypalField.placeholder ?? "", with: "") ?? ""
        let venmo = venmoField.text?.replacingOccurrences(of: venmoField.placeholder ?? "", with: "") ?? ""
        
        var data = [String: Any]()
        data["id"] = Profile.userId
        data["paypal"] = paypal
        data["venmo"] = venmo
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/save"), parameters: data) { (_) in
            self.dismissView(self)
        }
        
    }
}

extension PaymentsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.isEmpty ?? true {
            textField.text = (textField.placeholder ?? "") + string
            return false
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.text?.isEmpty ?? true {
            textField.text = textField.placeholder
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.count ?? 0) == (textField.placeholder?.count ?? 0) {
            textField.text = nil
        }
    }
}

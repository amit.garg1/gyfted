//
//  ItemViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 09/05/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import Branch
import FirebaseAnalytics
import Analytics

extension Notification.Name {
    static let itemWasUpdated = Notification.Name("ItemWasUpdatedNotification")
}

class ItemViewController: BaseViewController {
    
    var profile: Profile?
    var wishlist: Wishlist?
    var item: Item?
    var id: Int?
    var name: String?
    var showPurchaseStatus = true
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if wishlist?.userId == Profile.userId {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "NavBarMore"), style: .plain, target: self, action: #selector(presentActions))
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(presentShareItem))
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(itemWasUpdated), name: .itemWasUpdated, object: nil)
        reloadData()
    }
    
    @objc func itemWasUpdated(notification: Notification) {
        let item = notification.userInfo?["item"] as? Item
        if item?.id == self.item?.id {
            self.item = item
        }
        reloadData()
    }
    
    // swiftlint:disable cyclomatic_complexity function_body_length
    func reloadData() {
        _ = contentView.subviews.map { $0.removeFromSuperview() }
        
        guard let item = item else {
            guard let id = id else { return }
            APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "item/\(id)"), parameters: nil) { (handler) in
                switch handler {
                case .success(let data):
                    guard let data = data, let item = try? JSONDecoder().decode(Item.self, from: data) else { return }
                    self.item = item
                    self.reloadData()
                case .failure(let error):
                    NSLog("ERROR: \(error)")
                }
            }
            return
        }
        
        guard let wishlist = wishlist else {
            APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "wishlist/\(item.wishlistId)"), parameters: nil) { (handler) in
                switch handler {
                case .success(let data):
                    guard let data = data, let wishlist = try? JSONDecoder().decode(Wishlist.self, from: data) else { return }
                    self.wishlist = wishlist
                    self.reloadData()
                case .failure(let error):
                    NSLog("ERROR: \(error)")
                }
            }
            return
        }
        
        guard let profile = profile else {
            APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/\(wishlist.userId)"), parameters: nil) { (handler) in
                switch handler {
                case .success(let data):
                    guard let data = data, let profile = try? JSONDecoder().decode(Profile.self, from: data) else { return }
                    self.profile = profile
                    self.reloadData()
                case .failure(let error):
                    NSLog("Error: \(error)")
                }
            }
            return
        }
        
        let itemView = ItemDetailView.instance()
        contentView.addSubview(itemView)
        itemView.translatesAutoresizingMaskIntoConstraints = false
        itemView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        itemView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        itemView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        itemView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        view.layoutIfNeeded()
        itemView.showPurchaseStatus = showPurchaseStatus
        itemView.use(item, and: profile)
        if let controller = navigationController?.parent as? MainViewController {
            itemView.delegate = controller
        }
        itemView.itemViewController = self
    }
    
    @IBAction func presentAddItem(_ sender: UIButton) {
        guard let item = item, let controller = navigationController?.parent as? MainViewController else { return }
        controller.presentAddItemView(for: item)
    }
    
    @IBAction func presentActions() {
        guard let wishlist = wishlist, let item = item else { return }
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: NSLocalizedString("Share Item", comment: ""), style: .default, handler: { (_) in
            self.presentShareItem()
        }))
        if wishlist.userId == Profile.userId {
            controller.addAction(UIAlertAction(title: NSLocalizedString("Edit Item", comment: ""), style: .default, handler: { (_) in
                if let controller = UIStoryboard(name: "AddItem", bundle: nil).instantiateViewController(withIdentifier: "AddItemDetailsViewController") as? AddItemDetailsViewController {
                    controller.wishlist = wishlist
                    controller.item = item
                    let navController = BaseNavigationController(rootViewController: controller)
                    self.navigationController?.parent?.present(navController, animated: true, completion: nil)
                }
            }))
            controller.addAction(UIAlertAction(title: NSLocalizedString("Delete Item", comment: ""), style: .destructive, handler: { (_) in
                let controller = UIAlertController(title: NSLocalizedString("Delete Item", comment: ""), message: NSLocalizedString("Are you sure you want to delete this item?", comment: ""), preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: { (_) in
                    APIClient.shared.performRequest(method: .delete, url: APIClient.url(action: "item/\(item.id)"), parameters: nil, onCompletion: { (_) in
                        NotificationCenter.default.post(name: .cachedWishlistsDidChange, object: nil)
                    })
                    self.navigationController?.popViewController(animated: true)
                }))
                controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
                self.present(controller, animated: true, completion: nil)
            }))
        }
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func presentShareItem() {
        guard let wishlist = wishlist, let item = item else { return }
        
        let isMine = wishlist.userId == Profile.userId
        let text = isMine ? "Check out what I just added to my wishlist on @Gyftedapp! Feel free to spend some $$ & buy this for me :)" : "Check out this item I found on Gyfted!"
        
        let buo = BranchUniversalObject(canonicalIdentifier: "item/\(item.id)")
        buo.title = item.name
        buo.contentDescription = text
        buo.contentMetadata.customMetadata["item"] = item.id
        buo.publiclyIndex = true
        buo.imageUrl = item.imageUrls.first?.absoluteString
        buo.canonicalIdentifier = "item-\(item.id)"
        
        let lp = BranchLinkProperties()
        buo.getShortUrl(with: lp) { (url, _) in
            DispatchQueue.main.async {
                guard let url = url else { return }
                if let url = URL(string: url) {
                    var items = [Any]()
                    items.append(text)
                    items.append(url)
                    let controller = UIActivityViewController(activityItems: items, applicationActivities: nil)
                    self.present(controller, animated: true, completion: nil)
                    
                    Analytics.logEvent(AnalyticsEventShare, parameters: [
                        AnalyticsParameterContentType: "product",
                        AnalyticsParameterItemID: "\(item.id)",
                        AnalyticsParameterItemName: "\(item.name)"
                    ])
                    
                    SEGAnalytics.shared()?.track("Shared Item", properties: ["id": "\(item.id)"])
                }
            }
        }
    }
    
}

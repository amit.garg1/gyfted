//
//  ItemsViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 04/08/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

enum ItemsViewControllerMode {
    case likes
    case tag
}

class ItemsViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var items = [Item]()
    
    var profile: Profile?
    var tag: String?
    var mode = ItemsViewControllerMode.likes
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? ItemViewController, let item = sender as? Item {
            controller.item = item
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch mode {
        case .likes:
            title = NSLocalizedString("Likes", comment: "")
        case .tag:
            title = tag ?? ""
        }
        
        collectionView.register(UINib(nibName: "WishlistCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WishlistCollectionViewCell")
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .cachedWishlistsDidChange, object: nil)
        reloadData()
    }
    
    @objc func reloadData() {
        collectionView.isHidden = true
        spinner.startAnimating()
        
        switch mode {
        case .likes:
            guard let id = profile?.id else { return }
            APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/\(id)/likes"), parameters: nil) { (handler) in
                switch handler {
                case .success(let data):
                    if let data = data, let items = try? JSONDecoder().decode([Item].self, from: data) {
                        self.items = items
                    }
                case .failure(let error):
                    NSLog("Error: \(error)")
                    self.items = []
                }
                
                self.collectionView.reloadData()
                self.collectionView.setContentOffset(.zero, animated: false)
                self.collectionView.isHidden = false
                self.spinner.stopAnimating()
            }
        case .tag:
            guard let tag = tag else { return }
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "tag"), parameters: ["tag": tag]) { (handler) in
                switch handler {
                case .success(let data):
                    if let data = data, let items = try? JSONDecoder().decode([Item].self, from: data) {
                        self.items = items
                    }
                case .failure(let error):
                    NSLog("Error: \(error)")
                    self.items = []
                }
                
                self.collectionView.reloadData()
                self.collectionView.setContentOffset(.zero, animated: false)
                self.collectionView.isHidden = false
                self.spinner.stopAnimating()
            }
        }
    }
}

extension ItemsViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    // swiftlint:disable force_cast
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WishlistCollectionViewCell", for: indexPath) as! WishlistCollectionViewCell
        cell.use(items[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((UIScreen.main.bounds.size.width - 50 - 25) / 2.0).rounded(.down)
        let height = (width * (248.0/151.0)).rounded()
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "PresentItem", sender: items[indexPath.row])
    }
}

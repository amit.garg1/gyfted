//
//  WishlistsViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 11/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import SDWebImage
import Branch
import PhotosUI
import FirebaseStorage
import FirebaseAnalytics
import Analytics

// swiftlint:disable type_body_length file_length
class WishlistsViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var handleContainer: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var statusBarMask: UIView!
    @IBOutlet weak var phoneDetailsContainer: UIView!
    @IBOutlet weak var addressDetailsContainer: UIView!
    @IBOutlet weak var birthdayDetailsContainer: UIView!
    @IBOutlet weak var addFriendButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var friendsButton: UIButton!
    @IBOutlet weak var likesButton: UIButton!
    @IBOutlet weak var editAddressButton: UIButton!
    @IBOutlet weak var editPhoneButton: UIButton!
    @IBOutlet weak var editBirthdayButton: UIButton!
    @IBOutlet weak var editNameButton: UIButton!
    @IBOutlet weak var avatarButton: UIButton!
    @IBOutlet weak var shareUsernameButton: UIButton!
    @IBOutlet weak var profileStackView: UIStackView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var setupPaymentPrompt: UIView!
    @IBOutlet weak var setupPaymentButton: UIButton!
    @IBOutlet weak var presentPaymentsButton: UIButton!
    @IBOutlet weak var paymentsLabel: UILabel!
    @IBOutlet weak var paymentsContainer: UIView!
    @IBOutlet weak var wishlistsLabel: UILabel!
    
    var wishlists = [Wishlist]()
    var tappableButtons = [UIControl]()
    
    var id: String?
    var profile: Profile?
    var image: UIImage?
    
    lazy var dobPicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.date = Date().adjust(.year, offset: -18)
        picker.maximumDate = Date().adjust(.year, offset: -13)
        picker.addTarget(self, action: #selector(pickerValueDidChange), for: .valueChanged)
        return picker
    }()
    
    weak var dobField: UITextField?
    
    weak var initialInteractivePopGestureRecognizerDelegate: UIGestureRecognizerDelegate?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let wishlist = sender as? Wishlist, let controller = segue.destination as? WishlistViewController {
            controller.wishlist = wishlist
        }
        if let controller = segue.destination as? ItemsViewController {
            controller.profile = profile
        }
        if let controller = segue.destination as? FriendsViewController {
            controller.profile = profile
        }
        if let navController = segue.destination as? UINavigationController, let controller = navController.viewControllers.first as? SettingsViewController {
            controller.profile = profile
        }
        if let controller = segue.destination as? PaymentsViewController {
            controller.profile = profile
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shareButton.setImage(UIImage(named: "NavBarAddFriends")?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        initialInteractivePopGestureRecognizerDelegate = navigationController?.interactivePopGestureRecognizer?.delegate

        NotificationCenter.default.addObserver(self, selector: #selector(renderInterface), name: .authenticatedUserDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(wishlistsDidChange), name: .cachedWishlistsDidChange, object: nil)
        avatarView.layer.cornerRadius = avatarView.bounds.size.width / 2.0
        avatarView.layer.borderColor = UIColor.white.cgColor
        avatarView.layer.borderWidth = 2.0
        avatarView.layer.applySketchShadow(color: .black, alpha: 0.3, x: 9, y: 9, blur: 1, spread: 0)
        avatarView.backgroundColor = Config.colors.light
        handleContainer.layer.cornerRadius = 4
        handleContainer.layer.applySketchShadow(color: UIColor(displayP3Red: 196/255.0, green: 196/255.0, blue: 196/255.0, alpha: 1), alpha: 0.5, x: 0, y: -1, blur: 8, spread: 0)
        
        backButton.isHidden = (navigationController?.viewControllers.count ?? 1) == 1
        view.bringSubviewToFront(scrollView)
        view.bringSubviewToFront(loadingView)
        view.bringSubviewToFront(statusBarMask)
        
        collectionView.register(UINib(nibName: "WishlistCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WishlistCollectionViewCell")
        collectionView.register(UINib(nibName: "AddButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AddButtonCollectionViewCell")
        
        tappableButtons = [backButton, shareButton, friendsButton, likesButton, moreButton, addFriendButton, addressButton, editAddressButton, phoneButton, editPhoneButton, editBirthdayButton, editNameButton, avatarButton, shareUsernameButton, setupPaymentButton, presentPaymentsButton]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadData()
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = initialInteractivePopGestureRecognizerDelegate
    }
    
    func reloadData() {
        if id == nil, let data = Config.sharedDefaults().object(forKey: Config.userDefaults.profile) as? Data, let profile = try? JSONDecoder().decode(Profile.self, from: data) {
            id = profile.id
        }
        
        if let id = id {
            APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/\(id)"), parameters: nil) { (handler) in
                switch handler {
                case .success(let data):
                    do {
                        if let data = data {
                            let profile = try JSONDecoder().decode(Profile.self, from: data)
                            self.profile = profile
                            self.renderInterface()
                        }
                    } catch let error {
                        NSLog("Error: \(error)")
                    }
                    
                case .failure(let error):
                    let controller = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
                    controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (_) in
                        _ = self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(controller, animated: true, completion: nil)
                }
            }
        } else {
            Profile.signOut()
            guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController(), let window = UIApplication.shared.keyWindow else { return }
            window.rootViewController = controller
        }
    }
    
    // swiftlint:disable function_body_length cyclomatic_complexity
    @objc func renderInterface() {
        guard let profile = profile else { return }
        
        if let image = image {
            avatarView.image = image
        } else {
            avatarView.sd_setImage(with: profile.avatarURL, placeholderImage: nil)
        }
        
        nameLabel.text = profile.name.uppercased()
        nameLabel.kern(0.68)
        if let attributed = nameLabel.attributedText, profile.isCreator {
            let string = NSMutableAttributedString(attributedString: attributed)
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "CreatorTick")?.withRenderingMode(.alwaysTemplate)
            attachment.bounds = CGRect(x: 0, y: -3, width: 18, height: 18)
            let attachmentString = NSMutableAttributedString(attachment: attachment)
            attachmentString.insert(NSAttributedString(string: " ", attributes: [.font: UIFont.systemFont(ofSize: 18)]), at: 0)
            attachmentString.addAttribute(.foregroundColor, value: Config.colors.tint, range: NSMakeRange(0, attachmentString.length))
            string.append(attachmentString)
            nameLabel.attributedText = string
        }
        
        usernameLabel.text = "@" + profile.username
        
        shareButton.superview?.isHidden = backButton.isHidden == false || profile.id != Profile.userId
        
        if let dateOfBirth = profile.dateOfBirth, let date = DateFormatterManager.shared.mysqlDateFormatter.date(from: dateOfBirth) {
            birthdayLabel.text = DateFormatterManager.shared.shortDateFormatter.string(from: date)
        }
        
        titleLabel.text = profile.id == Profile.userId ? NSLocalizedString("My Wishlists", comment: "").uppercased() : NSLocalizedString("Wishlists", comment: "").uppercased()
        titleLabel.kern(0.68)
        
        let friendsTitle = profile.friendsCount == 1 ? "1 friend" : "\(profile.friendsCount) friends"
        let likesTitle = profile.likesCount == 1 ? "1 like" : "\(profile.likesCount) likes"
        let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 16, weight: .medium), .foregroundColor: Config.colors.text, .underlineStyle: NSNumber(value: NSUnderlineStyle.single.rawValue)]
        friendsButton.setAttributedTitle(NSAttributedString(string: friendsTitle, attributes: attributes), for: .normal)
        likesButton.setAttributedTitle(NSAttributedString(string: likesTitle, attributes: attributes), for: .normal)
        
        var showPayments = false
        
        if profile.id == Profile.userId {
            
            addFriendButton.superview?.isHidden = true
            
            addressDetailsContainer.isHidden = true
            addressButton.superview?.isHidden = true
            if profile.address.isEmpty == false {
                addressLabel.text = profile.address
                addressDetailsContainer.isHidden = false
            }
            
            phoneDetailsContainer.isHidden = true
            phoneButton.superview?.isHidden = true
            if profile.phone.isEmpty == false {
                phoneLabel.text = profile.phone
                phoneDetailsContainer.isHidden = false
            }
            
            setupPaymentPrompt.isHidden = true
            if profile.paymentsEnabled == false {
                setupPaymentPrompt.isHidden = false
            } else {
                showPayments = true
            }
            
        } else {
            phoneDetailsContainer.isHidden = true
            phoneButton.superview?.isHidden = true
            
            setupPaymentPrompt.isHidden = true
            
            if profile.isFriend {
                addFriendButton.superview?.isHidden = true
                birthdayDetailsContainer.isHidden = false
                addressDetailsContainer.isHidden = true
                addressButton.superview?.isHidden = true
                if profile.isAddressShared {
                    addressLabel.text = profile.address.isEmpty ? "No address provided" : profile.address
                    addressDetailsContainer.isHidden = false
                }
                if profile.paymentsEnabled {
                    showPayments = true
                }
            } else {
                birthdayDetailsContainer.isHidden = true
                addressDetailsContainer.isHidden = true
                addressButton.superview?.isHidden = true
                addFriendButton.isHidden = profile.isRequestSent
                
                if DataManager.shared.notifications.filter({$0.profileId == profile.id && $0.requestStatus == NotificationRequestStatus.pending.rawValue }).first != nil {
                    addFriendButton.setTitle(NSLocalizedString("Accept Friend Request", comment: ""), for: .normal)
                }
            }
        }
        
        if profile.dateOfBirth == nil {
            birthdayDetailsContainer.isHidden = true
        }
        
        paymentsContainer.isHidden = !showPayments
        if showPayments {
            var strings = [String]()
            if profile.paypal.isEmpty == false {
                strings.append("paypal.me/" + profile.paypal)
            }
            if profile.venmo.isEmpty == false {
                strings.append("@" + profile.venmo)
            }
            let attributed = NSAttributedString(string: strings.joined(separator: "\n"), attributes: [.foregroundColor: Config.colors.text, .font: UIFont.systemFont(ofSize: 16, weight: .medium), .underlineStyle: NSUnderlineStyle.single.rawValue])
            paymentsLabel.attributedText = attributed
        }

        view.layoutIfNeeded()
        reloadWishlists()
    }
    
    @objc func wishlistsDidChange(notification: Notification) {
        reloadWishlists()
    }
    
    func reloadWishlists() {
        guard let profile = profile else { return }
        let id = self.id ?? Profile.userId
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/\(id)/wishlists"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                if let data = data, let wishlists = try? JSONDecoder().decode([Wishlist].self, from: data) {
                    self.wishlists = wishlists
                    
                    self.wishlistsLabel.isHidden = true
                    self.titleLabel.text = profile.id == Profile.userId ? NSLocalizedString("My Wishlists", comment: "").uppercased() : NSLocalizedString("Wishlists", comment: "").uppercased()
                    if wishlists.count == 0 {
                        self.wishlistsLabel.isHidden = false
                        if profile.id == Profile.userId {
                            self.wishlistsLabel.isHidden = true
                        } else {
                            if profile.isFriend == false && profile.wishlistsCount > 0 && wishlists.count == 0 {
                                self.wishlistsLabel.text = "Send a friend request to see their wishlists"
                                self.titleLabel.text = "Private Account".uppercased()
                            } else {
                                self.wishlistsLabel.text = "\(profile.name) does not have any wishlists."
                            }
                        }
                    }
                    self.titleLabel.kern(0.68)
                    
                    self.collectionView.reloadData()
                    self.loadingView.isHidden = true
                }
            case .failure(let error):
                let controller = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (_) in
                    _ = self.navigationController?.popViewController(animated: true)
                }))
            }
        }
    }
    
    @IBAction func scrollViewWasTapped(_ sender: UITapGestureRecognizer) {
        let offset = view.convert(handleContainer.frame, from: scrollView).origin.y
        for button in tappableButtons where button.isHidden == false && (button.superview?.isHidden ?? true) == false {
            let frame = view.convert(button.frame, from: button.superview ?? button)
            if frame.origin.y > offset {
                continue
            }
            if frame.contains(sender.location(in: view)) {
                button.sendActions(for: .touchUpInside)
            }
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendFriendRequest(_ sender: Any) {
        let controller = UIAlertController(title: "Send Friend Request", message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "Friend", style: .default, handler: { (_) in
            self.performSendFriendRequest(isClose: false)
        }))
        controller.addAction(UIAlertAction(title: "Close Friend", style: .default, handler: { (_) in
            self.performSendFriendRequest(isClose: true)
        }))
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    private func performSendFriendRequest(isClose: Bool) {
        guard let profile = profile else { return }

        let parameters: [String: Any] = ["close": isClose ? 1 : 0]
        if let notification = DataManager.shared.notifications.filter({$0.profileId == profile.id && $0.requestStatus == NotificationRequestStatus.pending.rawValue }).first {
            _ = DataManager.shared.notifications.filter({$0.id == notification.id}).map({
                $0.requestStatus = NotificationRequestStatus.accepted.rawValue
                $0.isRead = true
            })
            NotificationCenter.default.post(name: .notificationsDidChange, object: nil)
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "request/\(notification.id)/accept"), parameters: parameters) { (_) in
                Analytics.logEvent("accepted_friend_request", parameters: [
                    "user_id": Profile.userId as NSObject,
                    "friend_id": profile.id as NSObject
                ])
                self.reloadData()
            }
        } else {
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/\(profile.id)/sendFriendRequest"), parameters: parameters, onCompletion: nil)
            addFriendButton.isHidden = true
            Analytics.logEvent("send_friend_request", parameters: [
                "user_id": Profile.userId as NSObject,
                "friend_id": profile.id as NSObject
            ])
        }
        
        SEGAnalytics.shared()?.track("Added Friend", properties: ["id": "\(profile.id)"])
    }
    
    @IBAction func handleAddressButton(_ sender: Any) {
        guard let profile = profile else { return }
        if profile.id == Profile.userId {
            presentAddAddress()
        } else if addressButton.isHidden == false {
            sendAddressRequest()
        }
    }
    
    func presentAddAddress() {
        guard let profile = profile, let controller = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "RegisterAddressViewController") as? RegisterAddressViewController else { return }
        controller.profile = profile
        controller.mode = .edit
        let navController = RegisterNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        present(navController, animated: true, completion: nil)
    }
    
    func sendAddressRequest() {
        guard let profile = profile else { return }
        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/\(profile.id)/sendAddressRequest"), parameters: nil, onCompletion: nil)
        addressButton.isHidden = true
        Analytics.logEvent("send_address_request", parameters: [
            "user_id": Profile.userId as NSObject,
            "friend_id": profile.id as NSObject
        ])
    }
    
    @IBAction func presentAddPhone(_ sender: Any) {
        guard let profile = profile, profile.id == Profile.userId, let controller = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "RegisterPhoneViewController") as? RegisterPhoneViewController else { return }
        controller.profile = profile
        controller.mode = .edit
        let navController = RegisterNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        present(navController, animated: true, completion: nil)
    }
    
    @IBAction func presentShare(_ sender: Any) {
        guard let main = navigationController?.parent as? MainViewController else { return }
        main.presentShare()
    }
    
    @IBAction func presentPaymentLinks(_ sender: Any) {
        guard let profile = profile else { return }
        if profile.id == Profile.userId {
            presentPayments()
            return
        }
        guard let main = navigationController?.parent as? MainViewController else { return }
        main.presentPaymentLinksView(profile)
    }
    
    @IBAction func presentShareProfile(_ sender: Any) {
        guard let profile = profile else { return }
        if backButton.isHidden == false || profile.id != Profile.userId { return }
        
        let text = "Hey! I just created my wishlist on Gyfted. It’s a free universal wishlist app & I think you’ll like it too. Check out my wishlist & create yours too!"
        let buo = BranchUniversalObject(canonicalIdentifier: "profile/\(profile.id)")
        buo.title = "@" + profile.username + " on Gyfted"
        buo.contentDescription = text
        buo.contentMetadata.customMetadata["profile"] = profile.id
        buo.publiclyIndex = true
        buo.imageUrl = profile.avatarURL?.absoluteString
        buo.canonicalIdentifier = profile.id
        
        let lp = BranchLinkProperties()
        buo.getShortUrl(with: lp) { (url, _) in
            DispatchQueue.main.async {
                guard let url = url else { return }
                if let url = URL(string: url) {
                    var items = [Any]()
                    items.append(text)
                    items.append(url)
                    let controller = UIActivityViewController(activityItems: items, applicationActivities: nil)
                    self.present(controller, animated: true, completion: nil)
                    
                    Analytics.logEvent(AnalyticsEventShare, parameters: [
                        AnalyticsParameterContentType: "profile",
                        AnalyticsParameterItemID: "\(profile.id)",
                        AnalyticsParameterItemName: "\(profile.name)"
                    ])
                    
                    SEGAnalytics.shared()?.track("Shared Profile")
                }
            }
        }
    }
    
    @IBAction func presentPayments() {
        performSegue(withIdentifier: "PresentPayments", sender: nil)
    }
    
    @IBAction func presentMoreActions(_ sender: Any) {
        guard let profile = profile else { return }
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if profile.id == Profile.userId {
            controller.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default, handler: { (_) in
                self.performSegue(withIdentifier: "PresentSettings", sender: nil)
            }))
            controller.addAction(UIAlertAction(title: NSLocalizedString("Edit Profile", comment: ""), style: .default, handler: { (_) in
                self.presentEditActions(sender)
            }))
            controller.addAction(UIAlertAction(title: NSLocalizedString("Share Profile", comment: ""), style: .default, handler: { (_) in
                self.presentShareProfile(sender)
            }))
        } else {
            
            if profile.isFriend {
                controller.addAction(UIAlertAction(title: NSLocalizedString("Remove Friend", comment: ""), style: .destructive, handler: { (_) in
                    APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/\(profile.id)/unfriend"), parameters: nil) { (_) in
                        self.reloadData()
                    }
                }))
                
                if profile.isClose {
                    controller.addAction(UIAlertAction(title: NSLocalizedString("Move to Friend", comment: ""), style: .default, handler: { (_) in
                        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/\(profile.id)/downgrade"), parameters: nil) { (_) in
                            self.reloadData()
                        }
                    }))
                } else {
                    controller.addAction(UIAlertAction(title: NSLocalizedString("Promote to Close Friend", comment: ""), style: .default, handler: { (_) in
                        APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/\(profile.id)/upgrade"), parameters: nil) { (_) in
                            self.reloadData()
                        }
                    }))
                }
                
                if profile.isAddressShared == false {
                    controller.addAction(UIAlertAction(title: NSLocalizedString("Request Address", comment: ""), style: .default, handler: { (_) in
                        self.sendAddressRequest()
                    }))
                }
            } else {
                controller.addAction(UIAlertAction(title: NSLocalizedString("Send Friend Request", comment: ""), style: .default, handler: { (_) in
                    self.sendFriendRequest(self)
                }))
            }
        }
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func presentEditActions(_ sender: Any) {
        let controller = UIAlertController(title: NSLocalizedString("Edit Profile", comment: ""), message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: NSLocalizedString("Profile Photo", comment: ""), style: .default, handler: { (_) in
            self.presentChangeAvatar()
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Name", comment: ""), style: .default, handler: { (_) in
            self.updateName()
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Birthday", comment: ""), style: .default, handler: { (_) in
            self.updateBirthday()
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Mailing Address", comment: ""), style: .default, handler: { (_) in
            self.presentAddAddress()
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Phone Number", comment: ""), style: .default, handler: { (_) in
            self.presentAddPhone(self)
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Gyft Money", comment: ""), style: .default, handler: { (_) in
            self.presentPayments()
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func presentLikes(_ sender: Any) {
        guard let profile = profile else { return }
        if profile.likesCount > 0 {
            performSegue(withIdentifier: "PresentLikes", sender: nil)
        }
    }
    
    @IBAction func presentFriends(_ sender: Any) {
        guard let profile = profile else { return }
        if profile.friendsCount > 0 {
            performSegue(withIdentifier: "PresentFriends", sender: nil)
        }
    }
    
    @IBAction func updateBirthday() {
        guard let profile = profile, profile.id == Profile.userId else { return }
        let controller = UIAlertController(title: NSLocalizedString("Birthday", comment: ""), message: nil, preferredStyle: .alert)
        controller.addTextField { (textField) in
            textField.inputView = self.dobPicker
            textField.tintColor = .clear
            textField.textAlignment = .center
            if let date = self.profile?.dob {
                self.dobPicker.date = date
            }
            self.dobField = textField
            self.pickerValueDidChange()
        }
        controller.addAction(UIAlertAction(title: NSLocalizedString("Update", comment: ""), style: .default, handler: { (_) in
            var data = [String: Any]()
            data["id"] = profile.id
            data["dateOfBirth"] = DateFormatterManager.shared.mysqlDateFormatter.string(from: self.dobPicker.date)
            self.profile?.dateOfBirth = DateFormatterManager.shared.mysqlDateFormatter.string(from: self.dobPicker.date)
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/save"), parameters: data, onCompletion: nil)
            self.birthdayLabel.text = DateFormatterManager.shared.shortDateFormatter.string(from: self.dobPicker.date)
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func updateName() {
        guard let profile = profile, profile.id == Profile.userId else { return }
        let controller = UIAlertController(title: NSLocalizedString("Name", comment: ""), message: nil, preferredStyle: .alert)
        controller.addTextField { (textField) in
            textField.tintColor = Config.colors.tint
            textField.placeholder = NSLocalizedString("Name", comment: "")
            textField.text = profile.name
        }
        controller.addAction(UIAlertAction(title: NSLocalizedString("Update", comment: ""), style: .default, handler: { (_) in
            guard let text = controller.textFields?.first?.text, text.isEmpty == false else { return }
            var data = [String: Any]()
            data["id"] = profile.id
            data["name"] = text
            APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/save"), parameters: data, onCompletion: nil)
            self.nameLabel.text = text.uppercased()
            self.nameLabel.kern(0.68)
            self.profile?.name = text
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @objc func pickerValueDidChange() {
        dobField?.text = DateFormatterManager.shared.shortDateFormatter.string(from: dobPicker.date).replacingOccurrences(of: "-", with: "/").replacingOccurrences(of: "/", with: " / ")
    }
    
    @IBAction func presentChangeAvatar() {
        guard let profile = profile, profile.id == Profile.userId else { return }
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: NSLocalizedString("Choose Photo", comment: ""), style: .default, handler: { (_) -> Void in
            self.presentPhotoPicker(.photoLibrary)
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Take Photo", comment: ""), style: .default, handler: { (_) -> Void in
            self.presentPhotoPicker(.camera)
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
}

extension WishlistsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private func presentPhotoPicker(_ sourceType: UIImagePickerController.SourceType) {
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = sourceType
        controller.allowsEditing = true
        present(controller, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let profile = profile, let image = info[.editedImage] as? UIImage, let resizedImage = resize(image, maxDimension: 400), let data = resizedImage.jpegData(compressionQuality: 0.7) {
            self.image = resizedImage
            let storage = Storage.storage()
            let storageRef = storage.reference()
            let filename = Profile.userId + "-" + NSUUID().uuidString + ".jpg"
            let imageRef = storageRef.child("avatars/" + filename)
            _ = imageRef.putData(data, metadata: nil) { (_, _) in
                imageRef.downloadURL(completion: { (url, _) in
                    guard let url = url else { return }
                    var data = [String: Any]()
                    data["id"] = profile.id
                    data["avatarUrlString"] = url.absoluteString
                    APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "profile/save"), parameters: data, onCompletion: { (_) in
                        self.image = nil
                    })
                })
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    private func resize(_ image: UIImage, maxDimension: CGFloat) -> UIImage? {
        let rect = AVMakeRect(aspectRatio: image.size, insideRect: CGRect(x: 0, y: 0, width: maxDimension, height: maxDimension))
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 1.0)
        image.draw(in: CGRect(origin: .zero, size: rect.size))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage
    }
}

extension WishlistsViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let profile = profile else { return 0 }
        if profile.id == Profile.userId {
            return wishlists.count + 1
        }
        return wishlists.count
    }
    
    // swiftlint:disable force_cast
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row < wishlists.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WishlistCollectionViewCell", for: indexPath) as! WishlistCollectionViewCell
            cell.use(wishlists[indexPath.row])
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddButtonCollectionViewCell", for: indexPath) as! AddButtonCollectionViewCell
        cell.use(NSLocalizedString("Add a new wishlist", comment: ""), heightPercentage: 198/248)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((UIScreen.main.bounds.size.width - 50 - 25) / 2.0).rounded(.down)
        let height = (width * (248.0/151.0)).rounded()
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < wishlists.count {
            performSegue(withIdentifier: "PresentWishlist", sender: wishlists[indexPath.row])
            return
        }
        
        if let controller = navigationController?.parent as? MainViewController {
            controller.presentCreateWishlist()
        }
    }
}

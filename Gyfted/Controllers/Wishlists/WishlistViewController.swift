//
//  WishlistViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 26/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import Branch
import FirebaseAnalytics
import FBSDKShareKit
import Swifter
import Analytics

// swiftlint:disable type_body_length file_length
class WishlistViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var wishlist: Wishlist?
    var profile: Profile?
    var items = [Item]()
    
    var id: Int?
    var name: String?
    var showPrice = false
    
    var tweetUrl: URL?
    var tweetText: String?
    var ignoreWishlistsDidChange = false
    
    enum WishlistSortOrder: CaseIterable {
        case dateAdded
        case alphabetical
        case price
        
        var name: String {
            switch self {
            case .dateAdded:
                return NSLocalizedString("Date Added", comment: "")
            case .alphabetical:
                return NSLocalizedString("Name", comment: "")
            case .price:
                return NSLocalizedString("Price", comment: "")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? ItemViewController, let item = sender as? Item {
            controller.item = item
            controller.wishlist = wishlist
        }
        
        if let navigationController = segue.destination as? UINavigationController, let controller = navigationController.viewControllers.first as? TweetSheetViewController {
            controller.text = tweetText
            controller.url = tweetUrl
            controller.imageUrl = wishlist?.imageUrl
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var buttons = [UIBarButtonItem]()
        if wishlist?.userId == Profile.userId {
            buttons.append(UIBarButtonItem(image: UIImage(named: "NavBarMore"), style: .plain, target: self, action: #selector(presentActions)))
            let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressDetected))
            collectionView.addGestureRecognizer(recognizer)
        } else {
            buttons.append(UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(presentShareWishlist)))
        }
        buttons.append(UIBarButtonItem(image: UIImage(named: "NavBarSort"), style: .plain, target: self, action: #selector(presentSortOptions)))
        navigationItem.rightBarButtonItems = buttons
        collectionView.register(UINib(nibName: "WishlistCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WishlistCollectionViewCell")
        collectionView.register(UINib(nibName: "AddButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AddButtonCollectionViewCell")
        collectionView.register(UINib(nibName: "WishlistHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "WishlistHeaderCollectionReusableView")
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .cachedWishlistsDidChange, object: nil)
        reloadData()
    }
        
    // swiftlint:disable cyclomatic_complexity function_body_length
    @objc func reloadData() {
        
        if ignoreWishlistsDidChange {
            ignoreWishlistsDidChange = false
            return
        }
        
        collectionView.isHidden = true
        spinner.startAnimating()
        
        if let id = wishlist?.id, let wishlist = DataManager.shared.wishlists.filter({ return $0.id == id }).first { // ensures we have latest version if it got edited whilst on this page!
            self.wishlist = wishlist
        }
        
        guard let wishlist = wishlist else {
            guard let id = id else { return }
            APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "wishlist/\(id)"), parameters: nil) { (handler) in
                switch handler {
                case .success(let data):
                    do {
                        if let data = data {
                            let wishlist = try JSONDecoder().decode(Wishlist.self, from: data)
                            self.wishlist = wishlist
                            self.reloadData()
                        }
                    } catch let error {
                        print(error)
                    }
                case .failure(let error):
                    NSLog("Error: \(error)")
                }
            }
            return
        }
        
        if profile == nil {
            APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/\(wishlist.userId)"), parameters: nil) { (handler) in
                switch handler {
                case .success(let data):
                    guard let data = data, let profile = try? JSONDecoder().decode(Profile.self, from: data) else { return }
                    self.profile = profile
                    self.reloadData()
                case .failure(let error):
                    NSLog("Error: \(error)")
                }
            }
            return
        }
        
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "wishlist/\(wishlist.id)/items"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                if let data = data, let items = try? JSONDecoder().decode([Item].self, from: data) {
                    self.items = items
                }
            case .failure(let error):
                NSLog("Error: \(error)")
                self.items = []
            }
            
            self.collectionView.reloadData()
            self.collectionView.setContentOffset(.zero, animated: false)
            self.collectionView.isHidden = false
            self.spinner.stopAnimating()
        }
    }
    
    @IBAction func presentActions() {
        guard let wishlist = wishlist else { return }
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: NSLocalizedString("Share Wishlist", comment: ""), style: .default, handler: { (_) in
            self.presentShareWishlist()
        }))
        if wishlist.userId == Profile.userId {
            controller.addAction(UIAlertAction(title: NSLocalizedString("Edit Wishlist", comment: ""), style: .default, handler: { (_) in
                if let controller = UIStoryboard(name: "AddWishlist", bundle: nil).instantiateViewController(withIdentifier: "WishlistDetailsViewController") as? WishlistDetailsViewController {
                    controller.wishlist = wishlist
                    let navController = BaseNavigationController(rootViewController: controller)
                    self.navigationController?.parent?.present(navController, animated: true, completion: nil)
                }
            }))
            controller.addAction(UIAlertAction(title: NSLocalizedString("Delete Wishlist", comment: ""), style: .destructive, handler: { (_) in
                let controller = UIAlertController(title: NSLocalizedString("Delete Wishlist", comment: ""), message: NSLocalizedString("Are you sure you want to delete this wishlist?", comment: ""), preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: { (_) in
                    APIClient.shared.performRequest(method: .delete, url: APIClient.url(action: "wishlist/\(wishlist.id)"), parameters: nil, onCompletion: { (_) in
                        DataManager.shared.cacheWishlists {
                        }
                    })
                    self.navigationController?.popViewController(animated: true)
                }))
                controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
                self.present(controller, animated: true, completion: nil)
            }))
        }
        
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @objc func presentSortOptions() {
        let controller = UIAlertController(title: NSLocalizedString("Sort Wishlist", comment: ""), message: nil, preferredStyle: .actionSheet)
        for order in WishlistSortOrder.allCases {
            controller.addAction(UIAlertAction(title: order.name, style: .default, handler: { (_) in
                self.sortItems(order)
            }))
        }
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    func sortItems(_ order: WishlistSortOrder) {
        showPrice = false
        switch order {
        case .dateAdded:
            items.sort { $0.id > $1.id }
        case .alphabetical:
            items.sort { $0.name.lowercased() < $1.name.lowercased() }
        case .price:
            items.sort {
                var a = $0.price
                var b = $1.price
                a.remove(at: a.startIndex)
                b.remove(at: b.startIndex)
                return Double(a) ?? 0 < Double(b) ?? 0
            }
            showPrice = true
        }
        collectionView.reloadData()
    }
    
    @IBAction func presentShareWishlist() {
        guard let wishlist = wishlist else { return }
        
        let isMine = wishlist.userId == Profile.userId
        let text = isMine ? "Check out my wishlist on @Gyftedapp! Start here if you’re going to buy me gifts anytime soon :)" : "Check out this wishlist I found on Gyfted!"
        
        let buo = BranchUniversalObject(canonicalIdentifier: "wishlist/\(wishlist.id)")
        buo.title = wishlist.name
        buo.contentDescription = text
        buo.contentMetadata.customMetadata["wishlist"] = wishlist.id
        buo.publiclyIndex = true
        buo.imageUrl = wishlist.imageUrl?.absoluteString
        buo.canonicalIdentifier = "wishlist-\(wishlist.id)"
        
        let lp = BranchLinkProperties()
        buo.getShortUrl(with: lp) { (urlString, _) in
            DispatchQueue.main.async {
                guard let urlString = urlString, let url = URL(string: urlString) else { return }
                
                let controller = UIAlertController(title: "Share Wishlist", message: nil, preferredStyle: .actionSheet)
                controller.addAction(UIAlertAction(title: NSLocalizedString("Facebook", comment: ""), style: .default, handler: { (_) in
                    self.presentFacebookShare(url: url)
                }))
                controller.addAction(UIAlertAction(title: NSLocalizedString("Twitter", comment: ""), style: .default, handler: { (_) in
                    self.presentTwitterShare(url: url, text: text)
                }))
                controller.addAction(UIAlertAction(title: NSLocalizedString("More", comment: ""), style: .default, handler: { (_) in
                    self.presentNativeShareSheet(url: url, text: text)
                }))
                controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
                self.present(controller, animated: true, completion: nil)

                if isMine {
                    APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "wishlist/save"), parameters: ["id": wishlist.id, "hasBeenShared": 1], onCompletion: nil)
                }
            }
        }
    }
    
    private func presentFacebookShare(url: URL) {
        let content = ShareLinkContent()
        content.contentURL = url
        let dialog = ShareDialog(fromViewController: self, content: content, delegate: nil)
        dialog.show()
        SEGAnalytics.shared()?.track("Shared Wishlist", properties: ["id": "\(self.wishlist?.id ?? 0)", "method": "Facebook"])
    }
    
    private func presentTwitterShare(url: URL, text: String) {
        
        if Config.isTwitterEnabled {
            tweetUrl = url
            tweetText = text
            performSegue(withIdentifier: "PresentTweetSheet", sender: nil)
            return
        }
        
        guard let callbackUrl = URL(string: "gyfted://") else { return }
        let swifter = Swifter(consumerKey: Config.credentials.twitterConsumerKey, consumerSecret: Config.credentials.twitterConsumerSecret)
        swifter.authorize(withCallback: callbackUrl, presentingFrom: self, success: { (token, _) in
            guard let token = token else { return }
            DispatchQueue.main.async {
                Config.sharedDefaults().set(token.key, forKey: Config.userDefaults.twitterToken)
                Config.sharedDefaults().set(token.secret, forKey: Config.userDefaults.twitterSecret)
                Config.sharedDefaults().synchronize()
                self.presentTwitterShare(url: url, text: text)
                
                SEGAnalytics.shared()?.track("Shared Wishlist", properties: ["id": "\(self.wishlist?.id ?? 0)", "method": "Twitter"])
            }
        })
    
    }
    
    private func presentNativeShareSheet(url: URL, text: String) {
        guard let wishlist = wishlist else { return }
        
        var items = [Any]()
        items.append(text)
        items.append(url)
        let controller = UIActivityViewController(activityItems: items, applicationActivities: nil)
        self.present(controller, animated: true, completion: nil)
        
        Analytics.logEvent(AnalyticsEventShare, parameters: [
            AnalyticsParameterContentType: "wishlist",
            AnalyticsParameterItemID: "\(wishlist.id)",
            AnalyticsParameterItemName: "\(wishlist.name)"
        ])
        
        SEGAnalytics.shared()?.track("Shared Wishlist", properties: ["id": "\(wishlist.id)", "method": "Share Sheet"])
    }
    
    @objc func longPressDetected(recognizer: UILongPressGestureRecognizer) {
        guard recognizer.state == .began else { return }
        let point = recognizer.location(in: collectionView)
        guard let indexPath = collectionView.indexPathForItem(at: point), indexPath.row < items.count else { return }
        presentLongPressActions(for: indexPath)
    }
    
    @IBAction func presentLongPressActions(for indexPath: IndexPath) {
        let item = items[indexPath.row]
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if DataManager.shared.wishlists.count > 1 {
            controller.addAction(UIAlertAction(title: NSLocalizedString("Move Item…", comment: ""), style: .default, handler: { (_) in
                self.moveItem(at: indexPath)
            }))
            controller.addAction(UIAlertAction(title: NSLocalizedString("Duplicate Item…", comment: ""), style: .default, handler: { (_) in
                self.duplicateItem(at: indexPath)
            }))
        }
        controller.addAction(UIAlertAction(title: NSLocalizedString("Delete Item", comment: ""), style: .destructive, handler: { (_) in
            APIClient.shared.performRequest(method: .delete, url: APIClient.url(action: "item/\(item.id)"), parameters: nil, onCompletion: { (_) in
                self.ignoreWishlistsDidChange = true
                NotificationCenter.default.post(name: .cachedWishlistsDidChange, object: nil)
            })
            self.items.remove(at: indexPath.row)
            self.collectionView.reloadData()
        }))
        controller.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func moveItem(at indexPath: IndexPath) {
        let item = items[indexPath.row]
        let controller = UIAlertController(title: nil, message: "Move this item to:", preferredStyle: .actionSheet)
        for wishlist in DataManager.shared.wishlists.filter({$0.id != self.wishlist?.id}) {
            controller.addAction(UIAlertAction(title: wishlist.name, style: .default, handler: { (_) in
                var data = [String: Any]()
                data["id"] = item.id
                data["wishlistId"] = wishlist.id
                APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "item/save"), parameters: data) { (_) in
                    self.ignoreWishlistsDidChange = true
                    NotificationCenter.default.post(name: .cachedWishlistsDidChange, object: nil)
                }
                self.items.remove(at: indexPath.row)
                self.collectionView.reloadData()
            }))
        }
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func duplicateItem(at indexPath: IndexPath) {
        let item = items[indexPath.row]
        let controller = UIAlertController(title: nil, message: "Duplicate item to:", preferredStyle: .actionSheet)
        for wishlist in DataManager.shared.wishlists.filter({$0.id != self.wishlist?.id}) {
            controller.addAction(UIAlertAction(title: wishlist.name, style: .default, handler: { (_) in
                var data = [String: Any]()
                data["name"] = item.name
                data["price"] = item.price
                data["text"] = item.text
                data["size"] = item.size
                data["color"] = item.color
                data["url"] = item.url
                data["storeName"] = item.storeName
                data["storeAddress"] = item.storeAddress
                if let lat = item.storeLat, let lng = item.storeLng {
                    data["storeLat"] = lat
                    data["storeLng"] = lng
                }
                data["imageUrlsString"] = item.imageUrlsString
                data["wishlistId"] = wishlist.id
                data["keywordString"] = item.keywordString
                APIClient.shared.performRequest(method: .post, url: APIClient.url(action: "item/save"), parameters: data) { (_) in
                    self.ignoreWishlistsDidChange = true
                    NotificationCenter.default.post(name: .cachedWishlistsDidChange, object: nil)
                }
            }))
        }
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }

}

extension WishlistViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if wishlist?.userId == Profile.userId {
            return items.count + 1
        }
        return items.count   
    }
    
    // swiftlint:disable force_cast
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row < items.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WishlistCollectionViewCell", for: indexPath) as! WishlistCollectionViewCell
            cell.use(items[indexPath.row], showPrice: showPrice, showPurchaseStatus: true)
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddButtonCollectionViewCell", for: indexPath) as! AddButtonCollectionViewCell
        cell.use(NSLocalizedString("Add a new item", comment: ""), heightPercentage: 198/248)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "WishlistHeaderCollectionReusableView", for: indexPath) as! WishlistHeaderCollectionReusableView
        if let wishlist = wishlist {
            header.use(wishlist, and: profile)
            if let controller = navigationController?.parent as? MainViewController {
                header.delegate = controller
            }
        }
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((UIScreen.main.bounds.size.width - 50 - 25) / 2.0).rounded(.down)
        let height = (width * (248.0/151.0)).rounded()
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let indexPath = IndexPath(row: 0, section: section)
        let headerView = self.collectionView(collectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: indexPath)
        return headerView.systemLayoutSizeFitting(CGSize(width: collectionView.frame.width, height: UIView.layoutFittingExpandedSize.height), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < items.count {
            performSegue(withIdentifier: "PresentItem", sender: items[indexPath.row])
            return
        }
        
        if let controller = navigationController?.parent as? MainViewController {
            controller.presentAddNewItem(with: wishlist)
        }
    }
}

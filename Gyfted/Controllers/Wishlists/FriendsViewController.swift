//
//  FriendsViewController.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/08/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class FriendsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
    
    var profile: Profile?
    var profiles = [SearchProfile]()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? WishlistsViewController, let id = sender as? String {
            controller.id = id
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("Friends", comment: "").uppercased()
        
        tableView.register(UINib(nibName: "SearchProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchProfileTableViewCell")
        tableView.tableFooterView = UIView()
        
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadData()
    }
    
    @objc func reloadData() {
        guard let profile = profile else { return }
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/\(profile.id)/friends"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                guard let data = data, let profiles = try? JSONDecoder().decode([SearchProfile].self, from: data) else { return }
                self.profiles = profiles
                self.tableView.reloadData()
            case .failure(let error):
                NSLog("ERROR: \(error)")
            }
            self.loadingView.isHidden = true
        }
    }
    
}

extension FriendsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profiles.count
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchProfileTableViewCell", for: indexPath) as! SearchProfileTableViewCell
        cell.use(profiles[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profile = profiles[indexPath.row]
        performSegue(withIdentifier: "PresentProfile", sender: profile.id)
    }
    
}

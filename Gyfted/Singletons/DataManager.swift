//
//  DataManager.swift
//  Gyfted
//
//  Created by Ben Dodson on 02/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import RealmSwift

private let _DataManagerSharedInstance = DataManager()

class DataManager: NSObject {
    
    class var shared: DataManager {
        return _DataManagerSharedInstance
    }
    
    // swiftlint:disable force_try
    let realm = try! Realm()
    
    var followedTopics = [Int]()
    var notifications = [GyftedNotification]()
    
    var unreadNotificationCount: Int {
        return notifications.filter({$0.isRead == false}).count
    }
    
    var wishlists: [Wishlist] {
        guard let data = Config.sharedDefaults().data(forKey: Config.userDefaults.wishlists) else { return [] }
        do {
            return try JSONDecoder().decode([Wishlist].self, from: data)
        } catch let error {
            NSLog("failed to decode wishlists: \(error)")
        }
        return []
    }
    
    override init() {
        followedTopics = Config.sharedDefaults().object(forKey: Config.userDefaults.followedTopics) as? [Int] ?? []
        super.init()
    }
    
    func cacheWishlists(onCompletion completionHandler: @escaping () -> Void) {
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "wishlist/fetchMine"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                Config.sharedDefaults().set(data, forKey: Config.userDefaults.wishlists)
                #if !TARGET_IS_EXTENSION
                    NotificationCenter.default.post(name: .cachedWishlistsDidChange, object: nil)
                #endif
            case .failure(let error):
                NSLog("ERROR: \(error)")
            }
            completionHandler()
        }
    }
    
    func cacheProfile(onCompletion completionHandler: @escaping () -> Void) {
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/\(Profile.userId)/"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                Config.sharedDefaults().set(data, forKey: Config.userDefaults.profile)
                #if !TARGET_IS_EXTENSION
                    NotificationCenter.default.post(name: .authenticatedUserDidChange, object: nil)
                #endif
            case .failure(let error):
                NSLog("Error: \(error)")
            }
            completionHandler()
        }
    }
    
    func cacheFollowedTopics(onCompletion completionHandler: @escaping () -> Void) {
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "topic/followed"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                guard let data = data, let followedTopics = try? JSONDecoder().decode([Int].self, from: data) else { return }
                self.followedTopics = followedTopics
                Config.sharedDefaults().set(followedTopics, forKey: Config.userDefaults.followedTopics)
            case .failure(let error):
                NSLog("Error: \(error)")
            }
            completionHandler()
        }
    }
    
    func cacheNotifications(onCompletion completionHandler: @escaping () -> Void) {
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "notifications"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                guard let data = data, let notifications = try? JSONDecoder().decode([GyftedNotification].self, from: data) else { return }
                self.notifications = notifications
                #if !TARGET_IS_EXTENSION
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .notificationsDidChange, object: nil)
                    }
                #endif
            case .failure(let error):
                NSLog("Error: \(error)")
            }
            completionHandler()
        }
    }
    
    func followTopic(with id: Int) {
        let url = APIClient.url(action: "topic/\(id)/follow")
        APIClient.shared.performRequest(method: .post, url: url, parameters: nil, onCompletion: nil)
        if followedTopics.contains(id) == false {
            followedTopics.append(id)
            Config.sharedDefaults().set(followedTopics, forKey: Config.userDefaults.followedTopics)
        }
    }
    
    func unfollowTopic(with id: Int) {
        let url = APIClient.url(action: "topic/\(id)/unfollow")
        APIClient.shared.performRequest(method: .post, url: url, parameters: nil, onCompletion: nil)
        if let index = followedTopics.firstIndex(of: id) {
            followedTopics.remove(at: index)
            Config.sharedDefaults().set(followedTopics, forKey: Config.userDefaults.followedTopics)
        }
    }
    
    func isTopicFollowed(with id: Int) -> Bool {
        return followedTopics.contains(id)
    }
    
    // MARK: Cached Keywords
    
    func downloadCachedKeywords() {
        APIClient.shared.performRequest(method: .get, url: APIClient.url(action: "profile/keywords"), parameters: nil) { (handler) in
            switch handler {
            case .success(let data):
                guard let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: .init(rawValue: 0)) as? [String] else {
                    return
                }
                
                var keywords = [CachedKeyword]()
                for string in json {
                    let keyword = CachedKeyword()
                    keyword.string = string
                    keywords.append(keyword)
                }
                
                try? self.realm.write {
                    self.realm.deleteAll()
                    self.realm.add(keywords)
                }
            case .failure(let error):
                NSLog("Error: \(error)")
            }
        }
    }
    
    func fetchKeywords(for term: String) -> [String] {
        return realm.objects(CachedKeyword.self).filter("string BEGINSWITH[cd] %@", term).sorted(byKeyPath: "string").map({$0.string})
    }
}

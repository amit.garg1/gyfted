//
//  APIClient.swift
//  Gyfted
//
//  Created by Ben Dodson on 11/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import PercentEncoder

enum APIError: Error {
    case unknownError
    case unexpectedStatusCode
    case jsonError(String)
    case serverError
    case requestError
    case invalidURL
    case custom(String)
}

private let _APIClientSharedInstance = APIClient()

typealias APIHandler = (Result<Data?, APIError>) -> Void

class APIClient: NSObject {
    
    enum APIClientMethod: String {
        case get = "GET"
        case post = "POST"
        case delete = "DELETE"
        case put = "PUT"
        case patch = "PATCH"
    }
    
    class var shared: APIClient {
        return _APIClientSharedInstance
    }
    
    class func url(action: String) -> URL? {
        return APIClient.url(action: action, with: [:])
    }
    
    class func url(action: String, with parameters: [String: Any]) -> URL? {
        var string = Config.API.endpoint + action + "/"
        if parameters.count > 0 {
            var components = [String]()
            for (key, value) in parameters {
                var string = value as? String
                if string == nil {
                    string = "\(value)"
                }
                if let string = string {
                    components.append("\(key)=\(PercentEncoding.encodeURIComponent.evaluate(string: string))")
                }
            }
            string += "?" + components.joined(separator: "&")
        }
        return URL(string: string)
    }
    
    // swiftlint:disable cyclomatic_complexity function_body_length
    func performRequest(method: APIClientMethod, url: URL?, parameters: [String: Any]?, onCompletion completionHandler: APIHandler?) {
        
        guard let url = url else {
            DispatchQueue.main.async(execute: {
                completionHandler?(.failure(.invalidURL))
            })
            return
        }
        
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        let session = URLSession(configuration: config)
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.setValue(Profile.userId, forHTTPHeaderField: "Authorization")
        
        if let parameters = parameters, method != .get {
            var components = [String]()
            for (key, value) in parameters {
                var string = value as? String
                if string == nil {
                    string = "\(value)"
                }
                if let string = string {
                    components.append("\(key)=\(PercentEncoding.encodeURIComponent.evaluate(string: string))")
                }
            }
            if let data = components.joined(separator: "&").data(using: .utf8) {
                request.httpBody = data
                request.setValue("\((data as NSData).length)", forHTTPHeaderField: "Content-Length")
            }
            writeLog("POST \(url.absoluteString)")
            writeLog(parameters.description)
        } else {
            writeLog("GET \(url.absoluteString)")
        }
        
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            
            DispatchQueue.main.async(execute: {
                if let error = error {
                    NSLog("Error: \(error)")
                    completionHandler?(.failure(.unknownError))
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    completionHandler?(.failure(.unknownError))
                    return
                }
                
                switch httpResponse.statusCode {
                case 200:
                    guard let data = data else {
                        completionHandler?(.failure(.unknownError))
                        return
                    }
                    
                    let dictionary = try? JSONSerialization.jsonObject(with: data, options: .init(rawValue: 0)) as? NSDictionary
                    let array = try? JSONSerialization.jsonObject(with: data, options: .init(rawValue: 0)) as? NSArray
                    
                    if dictionary == nil && array == nil {
                        let output = String(data: data, encoding: .utf8) ?? "Can't decode server response from JSON"
                        completionHandler?(.failure(.jsonError(output)))
                        return
                    }
                    
                    if let dictionary = dictionary, let message = dictionary["error"] as? String, message.count > 0 {
                        completionHandler?(.failure(.custom(message)))
                        return
                    }
                    
                    completionHandler?(.success(data))
                case 201, 204:
                    completionHandler?(.success(nil))
                case 400...499:
                    completionHandler?(.failure(.requestError))
                case 500...599:
                    completionHandler?(.failure(.serverError))
                default:
                    completionHandler?(.failure(.unexpectedStatusCode))
                }
            })
        })
        task.resume()
    }
}

//
//  Config.swift
//  Gyfted
//
//  Created by Ben Dodson on 03/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

private let _logManager = LogManager()

class LogManager: NSObject {
    class var shared: LogManager {
        return _logManager
    }
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "H:mm:ss.SSS"
        return formatter
    }()
    class func timestamp() -> String {
        return LogManager.shared.dateFormatter.string(from: Date())
    }
}

func writeLog(_ error: Error, file: String = #file, function: String = #function, line: Int = #line) {
    writeLog("\(error)", file: file, function: function, line: line)
}

func writeLog(_ message: String? = nil, file: String = #file, function: String = #function, line: Int = #line) {
    let logEmojis = ["😀","😎","😱","😈","👺","👽","👾","🤖","🎃","👍","👁","🧠","🎒","🧤","🐶","🐱","🐭","🐹","🦊","🐻","🐨","🐵","🦄","🦋","🌈","🔥","💥","⭐️","🍉","🥝","🌽","🍔","🍿","🎹","🎁","❤️","🧡","💛","💚","💙","💜","🔔"]
    let logEmoji = logEmojis[abs(file.hashValue % logEmojis.count)]
    if let message = message {
        print("DodsonLog \(LogManager.timestamp()): \(logEmoji) \((file as NSString).lastPathComponent):\(line) \(function): \(message)")
    } else {
        print("DodsonLog \(LogManager.timestamp()): \(logEmoji) \((file as NSString).lastPathComponent):\(line) \(function)")
    }
}

// swiftlint:disable type_name
class Config: NSObject {
    
    struct API {
        static let endpoint = "https://gyfted.dodoapps.io/api/"
        //static let endpoint = "http://localhost:4000/api/"
    }
    
    struct colors {
        static let dark = UIColor(named: "Dark")!
        static let light = UIColor(named: "Light")!
        static let tint = UIColor(named: "Tint")!
        static let text = UIColor(named: "Text")!
        static let blue = UIColor(named: "Blue")!
    }
    
    struct userDefaults {
        static let profile = "CachedProfile"
        static let wishlists = "CachedWishlists"
        static let followedTopics = "CachedFollowedTopics"
        static let shownReviewPrompt = "ShownReviewPrompt"
        static let appOpens = "AppOpens"
        static let engagementNotificationsScheduled = "EngagementNotificationsScheduled"
        static let inviteFriendsPromptDisplayed = "InviteFriendsPromptDisplayed"
        static let twitterToken = "TwitterToken"
        static let twitterSecret = "TwitterSecret"
        static let autoTweetEnabled = "AutoTweetEnabled"
    }
    
    struct credentials {
        static let googlePlaces = "AIzaSyBG4tpW-k5UMRQHKFSiCbNbj2hElnm1Q3c"
        static let firebaseKeychain = "GV9EJCZ6NW.it.gyfted.ios.firebase"
        static let twitterConsumerKey = "XLygRc4yI0pX1RrNjwhafQf0l"
        static let twitterConsumerSecret = "MV4QWNVwiyf6Fvu2hYnREZaQndSvFeRjcQf3AMjkhnFLevnuNU"
    }
    
    struct websites {
        static let website = "https://gyfted.it"
        static let faqs = "https://gyfted.it"
        static let help = "https://gyfted.it"
        static let privacy = "https://gyfted.it/privacy-policy/"
        static let terms = "https://gyfted.it/terms/"
    }
    
    struct emails {
        static let feedback = "feedback@gyfted.it"
    }
    
    static func keepingUpAppearances() {
        UINavigationBar.appearance().shadowImage = UIImage()
        
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: Config.colors.dark, .font: UIFont.systemFont(ofSize: 18, weight: .bold)]
        UINavigationBar.appearance(whenContainedInInstancesOf: [RegisterNavigationController.self]).titleTextAttributes = [.foregroundColor: UIColor.white, .font: UIFont.systemFont(ofSize: 18, weight: .bold)]
        
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance(whenContainedInInstancesOf: [RegisterNavigationController.self]).barTintColor = Config.colors.dark
        
        UINavigationBar.appearance().tintColor = Config.colors.tint
        
        UINavigationBar.appearance(whenContainedInInstancesOf: [BaseNavigationController.self]).backIndicatorImage = UIImage()
        UINavigationBar.appearance(whenContainedInInstancesOf: [BaseNavigationController.self]).backIndicatorTransitionMaskImage = UIImage()
        
        let states: [UIControl.State] = [.normal, .selected, .highlighted]
        for state in states {
            UIBarButtonItem.appearance().setTitleTextAttributes([.foregroundColor: Config.colors.tint, .font: Config.compactFont(ofSize: 16)], for: state)
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [RegisterNavigationController.self]).setTitleTextAttributes([.foregroundColor: UIColor.white, .font: UIFont.systemFont(ofSize: 15, weight: .medium)], for: state)
        }
    }
    
    static func compactFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "SFCompactDisplay-Regular", size: size)!
    }
    
    static func sharedDefaults() -> UserDefaults {
        guard let defaults = UserDefaults(suiteName: "group.it.gyfted.ios") else {
            fatalError("Couldn't create shared defaults")
        }
        return defaults
    }
    
    static var isTwitterEnabled: Bool {
        return Config.sharedDefaults().string(forKey: Config.userDefaults.twitterToken) != nil && Config.sharedDefaults().string(forKey: Config.userDefaults.twitterSecret) != nil
    }

}

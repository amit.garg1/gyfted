//
//  Item.swift
//  Gyfted
//
//  Created by Ben Dodson on 26/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import PercentEncoder

class Like: NSObject, Codable {
    var userId: String
    var itemId: String
}

class Item: NSObject, Codable {
    var id: Int
    var name: String
    var price: String
    var text: String
    var size: String
    var color: String
    var url: String
    var storeName: String
    var storeAddress: String
    var storeLat: Double?
    var storeLng: Double?
    var imageUrlsString: String
    var wishlistId: String
    var keywordString: String
    var purchasedBy: String?
    
    var likeKey: String {
        return Profile.userId + "-\(id)"
    }
    
    var isLiked: Bool {
        return Config.sharedDefaults().bool(forKey: likeKey)
    }
    
    var keywords: [String] {
        return keywordString.split(separator: ",").map({"\($0)"}).filter({ return $0.isEmpty == false })
    }
    
    var imageUrls: [URL] {
        return imageUrlsString.split(separator: ",").compactMap({ URL(string: "\($0)") })
    }
    
    var formattedStoreAddress: String {
        var components = storeAddress.components(separatedBy: ",")
        components.insert(storeName, at: 0)
        return components.map({ $0.trimmingCharacters(in: .whitespacesAndNewlines) }).joined(separator: "\n")
    }
    
    var skimlinksUrl: URL? {
        guard url.isEmpty == false else { return nil }
        let string = "http://go.skimresources.com/?id=147247X1612209&url=" + PercentEncoding.encodeURI.evaluate(string: url)
        return URL(string: string)
    }

}

//
//  Wishlist.swift
//  Gyfted
//
//  Created by Ben Dodson on 24/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

enum Privacy: Int {
    case everyone
    case friends
    case me
    
    var name: String {
        switch self {
        case .everyone:
            return "everyone"
        case .friends:
            return "friends"
        case .me:
            return "me"
        }
    }
}

enum WishlistType: String {
    case general
    case anniversary
    case babyShower
    case birthday
    case holiday
    case wedding
    case teacher
    case ring
}

extension WishlistType {
    static var types: [WishlistType] {
        return [.holiday, .birthday, .wedding, .anniversary, .teacher, .babyShower, .ring, .general]
    }
    
    var keyword: String {
        switch self {
        case .general:
            return ""
        case .babyShower:
            return "Baby Shower".uppercased()
        case .ring:
            return "Ring Ideas".uppercased()
        case .teacher:
            return "Help-a-Teacher".uppercased()
        default:
            return rawValue.uppercased()
        }
    }
}

class Wishlist: NSObject, Codable {
    var id: Int
    var name: String
    var relevantDate: String?
    var imageUrl: URL?
    var privacySetting: Int
    var typeSetting: String
    var keywordString: String?
    var userId: String
    
    var privacy: Privacy {
        return Privacy(rawValue: privacySetting) ?? .everyone
    }
    
    var type: WishlistType {
        return WishlistType(rawValue: typeSetting) ?? .general
    }
    
    var keywords: [String] {
        return keywordString?.split(separator: ",").map({"\($0)"}).filter({ return $0.isEmpty == false }) ?? []
    }
    
    var date: String? {
        guard let relevantDate = relevantDate else { return nil }
        if let date = DateFormatterManager.shared.mysqlDateFormatter.date(from: relevantDate) {
            return DateFormatterManager.shared.fullDateFormatter.string(from: date)
        }
        return nil
    }
}

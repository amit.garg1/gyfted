//
//  User.swift
//  Gyfted
//
//  Created by Ben Dodson on 02/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import OneSignal
import Analytics

class RegistrationProfile: NSObject {
    var email = ""
    var password = ""
    var username = ""
    var name = ""
    var dateOfBirth: Date?
    var gender = ""
    var address = ""
    var phone = ""
    var avatarUrlString: String?
    
    var data: [String: Any] {
        var data = [String: Any]()
        data["id"] = Profile.userId
        data["email"] = email
        data["name"] = name
        data["username"] = username
        if let date = dateOfBirth {
            data["dateOfBirth"] = DateFormatterManager.shared.mysqlDateFormatter.string(from: date)
        }
        data["gender"] = gender
        data["address"] = address
        data["phone"] = phone
        data["avatarUrlString"] = avatarUrlString ?? "https://www.gravatar.com/avatar/\(email.hashed(.md5) ?? "")?s=200&default=identicon"
        return data
    }
}

class SearchProfile: NSObject, Codable {
    var id: String
    var name: String
    var avatarUrlString: String
    var wishlistsCount = 0
    var imageUrlsString: String
    var isCreator: Bool
    
    var avatarURL: URL? {
        return URL(string: avatarUrlString)
    }
    
    var imageUrls: [URL] {
        return imageUrlsString.split(separator: ",").compactMap({ URL(string: "\($0)") })
    }
}

class Profile: NSObject, Codable {
    
    var id: String
    var email: String
    var username: String
    var name: String
    var dateOfBirth: String?
    var gender: String
    var address: String
    var phone: String
    var avatarUrlString: String
    var isFriend: Bool
    var isClose: Bool
    var isAddressShared: Bool
    var isRequestSent: Bool
    var friendsCount = 0
    var likesCount = 0
    var wishlistsCount = 0
    var isCreator: Bool
    var paypal: String
    var venmo: String
    
    static var isLoggedIn: Bool {
        return Auth.auth().currentUser != nil
    }
    
    static var userId: String {
        //return "yTvZt4FfGdOmvmNVqCxWQqsCaX92"
        return Auth.auth().currentUser?.uid ?? ""
    }
    
    var avatarURL: URL? {
        return URL(string: avatarUrlString)
    }
    
    var paymentsEnabled: Bool {
        return paypal.isEmpty == false || venmo.isEmpty == false
    }
    
    var dob: Date? {
        guard let dateOfBirth = dateOfBirth else { return nil }
        return DateFormatterManager.shared.mysqlDateFormatter.date(from: dateOfBirth)
    }
    
    var analyticsTraits: [String: String] {
        var traits = [String: String]()
        traits["name"] = name
        traits["email"] = email
        traits["avatar"] = avatarUrlString
        if let dateOfBirth = dateOfBirth {
            traits["birthday"] = dateOfBirth
        }
        traits["gender"] = gender
        traits["username"] = username
        traits["phone"] = phone
        traits["address"] = address
        return traits
    }
    
    static func signOut() {
        try? Auth.auth().signOut()
        Config.sharedDefaults().removeObject(forKey: Config.userDefaults.profile)
        Config.sharedDefaults().removeObject(forKey: Config.userDefaults.wishlists)
        Config.sharedDefaults().removeObject(forKey: Config.userDefaults.followedTopics)
        Config.sharedDefaults().removeObject(forKey: Config.userDefaults.shownReviewPrompt)
        Config.sharedDefaults().removeObject(forKey: Config.userDefaults.appOpens)
        Config.sharedDefaults().removeObject(forKey: Config.userDefaults.engagementNotificationsScheduled)
        Config.sharedDefaults().removeObject(forKey: Config.userDefaults.inviteFriendsPromptDisplayed)
        DataManager.shared.notifications = []
        OneSignal.deleteTag("user")
        SEGAnalytics.shared()?.reset()
    }
    
}

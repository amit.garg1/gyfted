//
//  Topic.swift
//  Gyfted
//
//  Created by Ben Dodson on 17/07/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class Topic: NSObject, Codable {

    var id: Int
    var name: String
    var items: [Item]
    var imageUrlString: String
    
    var isFollowed: Bool {
        return DataManager.shared.isTopicFollowed(with: id)
    }
}

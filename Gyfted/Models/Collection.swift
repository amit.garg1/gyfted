//
//  Collection.swift
//  Gyfted
//
//  Created by Ben Dodson on 21/08/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit

class Collection: NSObject, Codable {

    var id: Int
    var name: String
    var items: [Item]
}

//
//  Notification.swift
//  Gyfted
//
//  Created by Ben Dodson on 02/08/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

enum NotificationType: String {
    case purchase
    case friendRequest
    case addressRequest
    case friendEvent
    case myEvent
    case addressShared
}

enum NotificationRequestStatus: String {
    case notApplicable
    case pending
    case accepted
}

class GyftedNotification: NSObject, Codable {
    
    var id: Int
    var userId: String
    var text: String
    var highlight: String
    var type: String
    var profileId: String
    var itemId: Int
    var wishlistId: Int
    var requestStatus: String
    var createdAt: String
    var isRead: Bool
    var imageUrlString: String
    
    var imageUrl: URL? {
        return URL(string: imageUrlString)
    }
    
    var sentDate: Date {
        return DateFormatterManager.shared.mysqlDateTimeFormatter.date(from: createdAt) ?? Date()
    }
    
    var notificationType: NotificationType {
        return NotificationType(rawValue: type) ?? .purchase
    }
    
    var notificationRequestStatus: NotificationRequestStatus {
        return NotificationRequestStatus(rawValue: requestStatus) ?? .notApplicable
    }
}

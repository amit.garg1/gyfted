//
//  CachedKeyword.swift
//  Gyfted
//
//  Created by Ben Dodson on 14/09/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import RealmSwift
import LUAutocompleteView

class CachedKeyword: Object {
    @objc dynamic var string = ""
}

final class CustomAutocompleteTableViewCell: LUAutocompleteTableViewCell {

    override func set(text: String) {
        textLabel?.text = text
        textLabel?.textColor = Config.colors.tint
        textLabel?.font = Config.compactFont(ofSize: 13)
    }
}

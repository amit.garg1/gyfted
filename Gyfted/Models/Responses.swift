//
//  Responses.swift
//  Gyfted
//
//  Created by Ben Dodson on 05/06/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class ItemLikedResponse: NSObject, Codable {
    var isLiked: Bool
}

class SearchResponse: NSObject, Codable {
    var items: [Item]
    var profiles: [SearchProfile]
}

class HomeFeedResponse: NSObject, Codable {
    var shops: [Shop]
    var collections: [Collection]
    var following: [FeedPost]
    var friends: [FeedPost]
}

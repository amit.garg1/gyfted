//
//  FeedPost.swift
//  Gyfted
//
//  Created by Ben Dodson on 22/07/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

enum FeedPostType: String {
    case addedItem
    case addedWishlist
    case purchasedItem
}

class FeedPost: NSObject, Codable {
    
    var id: Int
    var userId: String
    var userName: String
    var avatarUrlString: String
    var action: String
    var wishlistId: Int
    var itemId: Int
    var metadata: String
    var imageUrlsString: String
    var createdAt: String
    var wishlistName: String
    
    var avatarUrl: URL? {
        return URL(string: avatarUrlString)
    }
    
    var imageUrls: [URL] {
        return imageUrlsString.split(separator: ",").compactMap({ URL(string: "\($0)") })
    }
    
    var postDate: Date {
        return DateFormatterManager.shared.mysqlDateTimeFormatter.date(from: createdAt) ?? Date()
    }
    
    var type: FeedPostType {
        return FeedPostType(rawValue: action) ?? .addedWishlist
    }

}

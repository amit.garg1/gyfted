//
//  Shop.swift
//  Gyfted
//
//  Created by Ben Dodson on 12/05/2020.
//  Copyright © 2020 Kopcrate LLC. All rights reserved.
//

import UIKit

class Shop: NSObject, Codable {

    var id: Int?
    var name: String?
    var url: String
    var imageUrlString: String
    var iconUrlString: String?
    
    var imageUrl: URL? {
        return URL(string: imageUrlString)
    }
    
    var iconUrl: URL? {
        guard let string = iconUrlString else {
            return nil
        }
        return URL(string: string)
    }
}

//
//  Contact.swift
//  Gyfted
//
//  Created by Ben Dodson on 18/11/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

class Contact: NSObject {
    var id = ""
    var name = ""
    var phoneNumbers = [String]()
    var emailAddresses = [String]()
    var imageData: Data?
    var profile: ContactProfile?
    
    override var description: String {
        return name
    }
    
    var metaText: String {
        return (phoneNumbers + emailAddresses).joined(separator: "\n")
    }
    
    var uniqueKey: String {
        return name + phoneNumbers.joined(separator: ",") + emailAddresses.joined(separator: ",")
    }
}

class ContactProfile: NSObject, Codable {
    var id: String
    var name: String
    var username: String
    var avatarUrlString: String
    var localId: String
    var isFriend: Bool
    var isRequestSent: Bool
    
    var avatarURL: URL? {
        return URL(string: avatarUrlString)
    }
}

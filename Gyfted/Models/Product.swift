//
//  Product.swift
//  Gyfted
//
//  Created by Ben Dodson on 11/04/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit

struct PotentialProduct {
    var title = ""
    var description = ""
    var url: URL?
    var images = [String]()
    
    var imageUrls: [URL] {
        return images.compactMap({ URL(string: $0) })
    }
}

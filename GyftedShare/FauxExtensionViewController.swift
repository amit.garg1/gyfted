//
//  FauxExtensionViewController.swift
//  GyftedShare
//
//  Created by Ben Dodson on 21/08/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import MobileCoreServices

class FauxExtensionViewController: UIViewController {

    var itemProvider: NSItemProvider?
    var context: NSExtensionContext?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navController = segue.destination as? UINavigationController, let controller = navController.viewControllers.first as? LoadingViewController {
            controller.itemProvider = itemProvider
            controller.context = context
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let items = extensionContext?.inputItems as? [NSExtensionItem] {
            for item in items {
                if let attachments = item.attachments {
                    for attachment in attachments {
                        if attachment.hasItemConformingToTypeIdentifier(kUTTypePropertyList as String) {
                            self.itemProvider = attachment
                            self.context = extensionContext
                            return
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if itemProvider == nil {
            NSLog("NO ITEM")
            extensionContext?.cancelRequest(withError: NSError(domain: "extension", code: 400, userInfo: nil))
            return
        }
        performSegue(withIdentifier: "PresentExtension", sender: nil)
    }
}

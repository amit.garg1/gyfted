var MyExtensionJavaScriptClass = function() {};
MyExtensionJavaScriptClass.prototype = {
    getTitle: function() {
        if (document.querySelectorAll('meta[property="og:title"]').length > 0) {
            return document.querySelectorAll('meta[property="og:title"]')[0].content;
        }
        if (document.querySelectorAll('title').length > 0) {
            return document.querySelectorAll('title')[0].text;
        }
        return "";
    },
    getDescription: function() {
        if (document.querySelectorAll('meta[property="og:description"]').length > 0) {
            return document.querySelectorAll('meta[property="og:description"]')[0].content;
        }
        if (document.querySelectorAll('meta[name="description"]').length > 0) {
            return document.querySelectorAll('meta[name="description"]')[0].content;
        }
        return "";
    },
    getImages: function() {
        var images = [];
        if (document.querySelectorAll('meta[property="og:image:secure_url"]').length > 0) {
            images.push(document.querySelectorAll('meta[property="og:image:secure_url"]')[0].content);
        } else if (document.querySelectorAll('meta[property="og:image"]').length > 0) {
            images.push(document.querySelectorAll('meta[property="og:image"]')[0].content);
        }
        
        let otherImages = Array.prototype.filter.call(document.images, function(image) {
            return image.src.substring(0,4) == 'http' && image.width >= 250 && image.height >= 250 && image.complete
        }).map(function(image) {
            return image.src
        })
        
        images = images.concat(otherImages);
        images = [...new Set(images)]; 
        return images;
    },
    run: function(arguments) {
        arguments.completionFunction({"url": document.baseURI, "title": this.getTitle(), "description": this.getDescription(), "images": this.getImages()});
    },
    finalize: function(arguments) {
    }
};

var ExtensionPreprocessingJS = new MyExtensionJavaScriptClass;

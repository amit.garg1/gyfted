//
//  LoadingViewController.swift
//  GyftedShare
//
//  Created by Ben Dodson on 21/08/2019.
//  Copyright © 2019 Kopcrate LLC. All rights reserved.
//

import UIKit
import Firebase
import MobileCoreServices

class LoadingViewController: BaseViewController {
    
    var itemProvider: NSItemProvider?
    @IBOutlet weak var errorLabel: UILabel!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? AddItemDetailsViewController {
            controller.product = sender as? PotentialProduct
            controller.wishlist = DataManager.shared.wishlists.first
            #if TARGET_IS_EXTENSION
                controller.context = context
            #endif
        }
        
        if let controller = segue.destination as? AddItemImagesViewController {
            controller.product = sender as? PotentialProduct
            controller.wishlist = DataManager.shared.wishlists.first
            #if TARGET_IS_EXTENSION
                controller.context = context
            #endif
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        errorLabel.isHidden = true
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        
        do {
            try Auth.auth().useUserAccessGroup(Config.credentials.firebaseKeychain)
            NSLog("AUTH REGISTERED SUCCESSFULLY")
        } catch let error as NSError {
            print("Error changing user access group: %@", error)
        }
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
        (navigationController as? BaseNavigationController)?.applyAppearances()
        
        if Auth.auth().currentUser == nil {
            errorLabel.text = NSLocalizedString("You must be signed into the Gyfted app in order to use this extension.", comment: "")
            errorLabel.isHidden = false
            return
        }
        
        if DataManager.shared.wishlists.count == 0 {
            errorLabel.text = NSLocalizedString("You need to create a wishlist within the Gyfted app before you can use this extension.", comment: "")
            errorLabel.isHidden = false
            return
        }
        
        guard let itemProvider = itemProvider else {
            NSLog("NO INPUT")
            self.dismissView()
            return
        }
    
        itemProvider.loadItem(forTypeIdentifier: kUTTypePropertyList as String, options: nil) { (result, _) in
            guard let result = result as? NSDictionary, let dictionary = result[NSExtensionJavaScriptPreprocessingResultsKey] as? NSDictionary else {
                NSLog("NO DICTIONARY")
                self.dismissView()
                return
            }
            
            let title = dictionary["title"] as? String ?? ""
            let description = dictionary["description"] as? String ?? ""
            let url = dictionary["url"] as? String ?? ""
            let images = dictionary["images"] as? [String] ?? []

            DispatchQueue.main.async {
                let product = PotentialProduct(title: title, description: description, url: URL(string: url), images: images)
                if images.count > 1 {
                    self.performSegue(withIdentifier: "PresentImages", sender: product)
                } else {
                    self.performSegue(withIdentifier: "PresentItemDetails", sender: product)
                }
            }
        }
    }
}
